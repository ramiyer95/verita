﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardUI : MonoBehaviour {

	private PlayerController thisPlayerController;

	public Board board;

	public void Start() {

		// reference to the parent. The parent will be responsible for sending messages to the server.

		thisPlayerController = this.transform.GetComponentInParent<PlayerController>();

		board = GameObject.Find("Board").GetComponent<Board>();

	}


	void OnMouseDown() {

		if (thisPlayerController.interactingWithAbilityUI) {

        	return;

        }

        AudioManager.instance.PlaySound("tap_audio");

		Vector2 clickedPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

		clickedPos = GlobalFunctions.WorldToGridCoordinates(clickedPos);

        thisPlayerController.photonView.RPC("PRPC_SendInputsToServer", PhotonTargets.MasterClient, clickedPos);

	}

    
}
