﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class Board : Photon.MonoBehaviour {


    // Need to place all the pieces at their correct positions when the game starts

    //TODO: Write code to spawn the VeritaMen from code. When the VeritaMen are spawned, they should be added automatically to the VeritaMen array.
    //TODO: VeritaMen must inherit from VeritaMen class. After scripts have been added to each VeritaMan, then prefab them. 

    public Team Team;

    public VeritaMen[,] veritaMenOnBoard = new VeritaMen[7,7];

    public VeritaMen selectedVeritaMan;

    public Team? currentTurn;
    private Team currenTurnTemp;  // To implement a 'NEUTRAL' state in the switch turn function

    public BoardHighlighting boardHighlighter;

    public WallManager walls;

    public Hashtable fortifiedSqaures = new Hashtable();

    public bool isDoubleMove = false;

    [SerializeField]
    private bool firstStepOverDM = false;

    public TurnManager turnManager;

    public RespawnManager respawnManager;

    public Tracker tracker;

    public Hashtable stonePiecesOfTeam = new Hashtable();

    public Team teamOne;
    public Team teamTwo;



    //NETWORKING VARIABLES
    //this is to identify the PhotonView of the two controllers
    public PhotonView playerOnePhotonView;
    public PhotonView playerTwoPhotonView;

    // I dont think we will need this anymore, but lets move forward and find out
    public PlayerController playerOneController;
    public PlayerController playerTwoController;

    public Hashtable photonViewID_clientTeam = new Hashtable();

    //deprecated?
    public Hashtable netId_Controller = new Hashtable();
    public Hashtable netId_netIdObject = new Hashtable();
    public Hashtable clientTeam_netIdObject = new Hashtable();
    
    public Hashtable clientTeam_photonVeiw = new Hashtable();


    
    //since these two variables should be strictly accessed together, it is neccessary to create a wrapper function
    //  around them.
    
    public bool hasGameEnded = false;

    public Team winnerOfGame;

    public void Start() {

    	this.enabled = true;

    }

    //Will only go through completely, when both players connect. So this wont go through when the masterclient
    //  instantiates his playerController. This will only go through when the 2nd player calls it on the masterClient.
    //  At this point, both players will be connected.
    public void WhenAllPlayersConnect () {

    	if (! (PhotonNetwork.playerList.Length == 2)) {

    		return; 

    	}


		//Setting the default values of fortified squares to (-1,-1). 
    	//This represents that no square is fortified.
    	fortifiedSqaures.Add(Team.RED, new Vector2(-1,-1));
    	fortifiedSqaures.Add(Team.BLUE, new Vector2(-1,-1));

    	stonePiecesOfTeam.Add(Team.BLUE, false);
    	stonePiecesOfTeam.Add(Team.RED, false);

    	respawnManager = this.gameObject.GetComponent<RespawnManager>();

    	turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();

    	tracker = GameObject.FindObjectOfType<Tracker>();

    	walls = this.transform.Find("Walls").gameObject.GetComponent<WallManager>();

    	currentTurn = Team.BLUE;

        SpawnAllRedPieces();
        SpawnAllBluePieces();

        //Instantiate the walls and initialize the walls arrays
        walls.StartOnBoardLoad();

        tracker.StartOnBoardLoad();

    	float a = Random.Range(0f, 1f);


		bool isPlayerOneIsBlue;
    	bool isPlayerTwoIsBlue;



    	if (a > 0.5) {

    		isPlayerOneIsBlue = true;
    		isPlayerTwoIsBlue = false;

    		teamOne = Team.BLUE;
    		teamTwo = Team.RED;

    	} else {

			isPlayerOneIsBlue = false;
			isPlayerTwoIsBlue = true;

			teamOne = Team.RED;
    		teamTwo = Team.BLUE;
    		
    	}

        playerOnePhotonView.RPC("PRPC_ResolveTeam", PhotonTargets.All, isPlayerOneIsBlue);
        playerTwoPhotonView.RPC("PRPC_ResolveTeam", PhotonTargets.All, isPlayerTwoIsBlue);

        playerOnePhotonView.RPC("PRPC_AssignTimerColor", PhotonTargets.All, isPlayerOneIsBlue);
        playerTwoPhotonView.RPC("PRPC_AssignTimerColor", PhotonTargets.All, isPlayerTwoIsBlue);


        //this mapping is required to inform ability scripts which team the ability execution request is coming from
        //also used in the selectVeritaMan function to verify the team moving pieces has the current turn
        photonViewID_clientTeam.Add(playerOnePhotonView.viewID, teamOne);
    	photonViewID_clientTeam.Add(playerTwoPhotonView.viewID, teamTwo);

        //deprecated

    	clientTeam_netIdObject.Add(teamOne, playerOnePhotonView); 
    	clientTeam_netIdObject.Add(teamTwo, playerTwoPhotonView);

    	netId_Controller.Add(playerOnePhotonView, playerOneController);
    	netId_Controller.Add(playerTwoPhotonView, playerTwoController);

    	netId_netIdObject.Add(playerOnePhotonView, playerOnePhotonView);
    	netId_netIdObject.Add(playerTwoPhotonView, playerTwoPhotonView);



        //important
    	clientTeam_photonVeiw.Add(teamOne, playerOnePhotonView);
    	clientTeam_photonVeiw.Add(teamTwo, playerTwoPhotonView);



    	// Can get away with calling this on only one player controller because ClientRpc runs the function on all client machines.
    	// (Thats what I thought earlier but turns out you CANNOT do that.)
    	// (This is because the same playerController does not have AUTHORITY on both the client and the server. Hence it can execute shit
    	//    on one of the two ONLY.)
    	// Therefore, have to call both. 
        // ---DEPRECATED----- (NOW RUNNING THIS FUNCTION IN THE START FUNCTION OF THE PLAYERCONTROLLERS)
        // IDIOT! YOU SHOULD HAVE TRUSTED YOUR OLDER SELF AND NOT DONE THIS. 

    	//playerOneController.TargetRunStartFuncOfUtilities(NetworkServer.objects[playerOnePhotonView].connectionToClient);
    	//playerTwoController.TargetRunStartFuncOfUtilities(NetworkServer.objects[playerTwoPhotonView].connectionToClient);

        playerOnePhotonView.RPC("PRPC_RunStartFuncOfUtilities", PhotonTargets.All);
        playerTwoPhotonView.RPC("PRPC_RunStartFuncOfUtilities", PhotonTargets.All);

        
        //Show the indicator for the first time since the game has started
        //Game always starts with the BLUE team.

        PhotonView pv = (PhotonView) clientTeam_photonVeiw[Team.BLUE];

        pv.RPC("PRPC_ShowTurnIndicator", PhotonTargets.AllViaServer);

        Invoke("CallRpcDeactivateTheTurnChangeIndicator", 1.3f);

        playerOnePhotonView.RPC("PRPC_ResolveGameState", PhotonTargets.All, GlobalFunctions.ResolveGameStatetoID(GameState.PLAY));

        playerTwoPhotonView.RPC("PRPC_ResolveGameState", PhotonTargets.All, GlobalFunctions.ResolveGameStatetoID(GameState.PLAY));


        //Updating sprites on game start
        foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
        {

            if (v.team == Team.BLUE)
            {
                if (!v.tempState.Contains(VeritaManState.TURNGLOW) && v.veritamenTypeOfThis != VeritaMenType.BULLDOZER)
                {

                    v.tempState.Add(VeritaManState.TURNGLOW);
                }
            }

            v.gameObject.GetComponent<SpriteManager>().CallPRPCUpdateSprites();

        }




        //Lastly, to make sure that the screen never sleeps, calling the Global Function here.
        // (if screen sleeps, the timer coroutine goes out of sync so its really important that the screen does NOT sleep
        GlobalFunctions.ScreenNeverSleep();
	}
	

    public void SelectVeritaMan(Vector2 mousePos, int viewIDOfPlayerController )
    {

        // The netID of the playerController that sends the command is required to make sure that players can only command
        //   to do relevant stuff when it is their turn.

        PhotonView photonViewOfController = PhotonView.Find(viewIDOfPlayerController);

        if (veritaMenOnBoard[(int)mousePos.x, (int)mousePos.y])
        {

			if ((veritaMenOnBoard[(int)mousePos.x, (int)mousePos.y].team == (Team)photonViewID_clientTeam[viewIDOfPlayerController]) && (veritaMenOnBoard[(int)mousePos.x, (int)mousePos.y].team == currentTurn))
            {

				if (isDoubleMove) {

					if (! veritaMenOnBoard[(int)mousePos.x, (int)mousePos.y].GetComponent<Superpawn>()) {

						return;

					}

            	}

            	//exit function if it is being attempted to select a bulldozer
            	if (veritaMenOnBoard[(int) mousePos.x, (int) mousePos.y].GetComponent<Bulldozer>()) {

            		return;

            	}


                selectedVeritaMan = veritaMenOnBoard[(int)mousePos.x, (int)mousePos.y];

				byte[] serializedMovesArray = GlobalFunctions.SerializeToByteArray<bool[,]>(selectedVeritaMan.PossibleMoves());

                //Hide all highlights before making new highlights
                photonViewOfController.RPC("PRPC_HideHighlight", PhotonTargets.AllViaServer);

                // Refer documentation to get clarification on this following line.
                photonViewOfController.RPC("PRPC_Highlight", PhotonTargets.AllViaServer, serializedMovesArray);



            } else
            {

                if (selectedVeritaMan)
                { 

                    if (selectedVeritaMan.MoveVeritaMan(mousePos))
                    {

                        veritaMenOnBoard[(int)selectedVeritaMan.pos.x, (int)selectedVeritaMan.pos.y] = null;

                        veritaMenOnBoard[(int)mousePos.x, (int)mousePos.y] = selectedVeritaMan;

                        selectedVeritaMan.pos = new Vector2(mousePos.x, mousePos.y);


                        SwitchTurn(false);
                        
                    }

                    photonViewOfController.RPC("PRPC_HideHighlight", PhotonTargets.AllViaServer);

                    selectedVeritaMan = null;


                }

            }

        }
        else
        {

            if (selectedVeritaMan)
            {

                if (selectedVeritaMan.MoveVeritaMan(mousePos))
                {

                    veritaMenOnBoard[(int)selectedVeritaMan.pos.x, (int)selectedVeritaMan.pos.y] = null;

                    veritaMenOnBoard[(int)mousePos.x, (int)mousePos.y] = selectedVeritaMan;

                    selectedVeritaMan.pos = new Vector2(mousePos.x, mousePos.y);

					// CHECKING WIN CONDITION AND CALLING RELEVANT FUNCTIONS IF IT IS SATISFIED

                        if (selectedVeritaMan.team == Team.BLUE) {

                        	// FIRST WIN CONDITION
							//Blue has successfully reached the red base

                        	if ( ( (int) mousePos.x == 0) && ((int) mousePos.y == 0 ) ) {
                        	                        		

                        		photonView.RPC("PRPC_GameEndedWithWinner", PhotonTargets.AllViaServer, Team.BLUE);

                        		PhotonView pvBlue = (PhotonView) clientTeam_photonVeiw[Team.BLUE];

                        		PhotonView pvRed = (PhotonView) clientTeam_photonVeiw[Team.RED];

                        		pvBlue.RPC("PRPC_OnWinConditionSatisfied", PhotonTargets.AllViaServer, true);
                            
                                pvRed.RPC("PRPC_OnWinConditionSatisfied", PhotonTargets.AllViaServer, false);

                        }




                        } else {

                        	if ( ((int) mousePos.x == 6) && ((int) mousePos.y == 6)) {

                        		//Red has successfully reached the blue base


                        		photonView.RPC("PRPC_GameEndedWithWinner", PhotonTargets.AllViaServer,Team.RED);

								PhotonView pvBlue = (PhotonView) clientTeam_photonVeiw[Team.BLUE];

                        		PhotonView pvRed = (PhotonView) clientTeam_photonVeiw[Team.RED];

                        		pvBlue.RPC("PRPC_OnWinConditionSatisfied", PhotonTargets.AllViaServer, false);

                                pvRed.RPC("PRPC_OnWinConditionSatisfied", PhotonTargets.AllViaServer, true);


                        }

						
                        }


                    SwitchTurn(false);

                }

                photonViewOfController.RPC("PRPC_HideHighlight", PhotonTargets.AllViaServer);

                selectedVeritaMan = null;

            }


        }
    }



    // ************************************
    //Piece spawning functions
    void SpawnAllRedPieces ()
    {

        SpawnQueen(Team.RED, 1, 1);
        SpawnRook(Team.RED, 1, 0);
        SpawnBishop(Team.RED, 0, 1);
        SpawnSuperpawn(Team.RED, 1, 2);
        SpawnSuperpawn(Team.RED, 2, 1);
        SpawnBulldozer(Team.RED, 2, 0);
          
    }


    void SpawnAllBluePieces ()
    {

        SpawnQueen(Team.BLUE, 5, 5);
        SpawnRook(Team.BLUE, 5, 6);
        SpawnBishop(Team.BLUE, 6, 5);
        SpawnSuperpawn(Team.BLUE, 4, 5);
        SpawnSuperpawn(Team.BLUE, 5, 4);
        SpawnBulldozer(Team.BLUE, 4, 6);

    }


    //Need to write a function to spawn each VeritaMan

    public VeritaMen SpawnQueen (Team team, int x, int y) {

    	if (!PhotonNetwork.isMasterClient) {

    		return  null;

    	}

        VeritaMen newVeritaMan;
       
        if (team == Team.BLUE)
        {

			GameObject n1ewVeritaMan = PhotonNetwork.Instantiate("b_queen", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0 );

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("B").transform);
            
            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();

        } else
        {

            GameObject n1ewVeritaMan = PhotonNetwork.Instantiate("r_queen", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0 );

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("R").transform);

            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();
        }

         
        newVeritaMan.pos = new Vector2(x, y);

        veritaMenOnBoard[x, y] = newVeritaMan;


        return newVeritaMan;
    }

    public VeritaMen SpawnRook(Team team, int x, int y)
    {

		if (!PhotonNetwork.isMasterClient) {

    		return null;

    	}

        VeritaMen newVeritaMan;
       
        if (team == Team.BLUE)
        {

            GameObject n1ewVeritaMan = (GameObject)PhotonNetwork.Instantiate("b_rook", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0);

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("B").transform);

            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();

        } else
        {

            GameObject n1ewVeritaMan = (GameObject)PhotonNetwork.Instantiate("r_rook", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0);

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("R").transform);

            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();
        }


        newVeritaMan.pos = new Vector2(x, y);

        veritaMenOnBoard[x, y] = newVeritaMan;

        return newVeritaMan;

    }


    public VeritaMen SpawnBishop(Team team, int x, int y)
    {

		if (!PhotonNetwork.isMasterClient) {

    		return null;

    	}

        VeritaMen newVeritaMan;
       
        if (team == Team.BLUE)
        {

            GameObject n1ewVeritaMan = (GameObject)PhotonNetwork.Instantiate("b_bishop", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0);

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("B").transform);

            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();

        } else
        {

            GameObject n1ewVeritaMan = (GameObject)PhotonNetwork.Instantiate("r_bishop", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0);

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("R").transform);

            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();
        }


        newVeritaMan.pos = new Vector2(x, y);

        veritaMenOnBoard[x, y] = newVeritaMan;

        return newVeritaMan;

    }


    public VeritaMen SpawnSuperpawn(Team team, int x, int y)
    {

		if (!PhotonNetwork.isMasterClient) {

    		return null;

    	}

        VeritaMen newVeritaMan;
       
        if (team == Team.BLUE)
        {

            GameObject n1ewVeritaMan = (GameObject)PhotonNetwork.Instantiate("b_superpawn 1", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0);

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("B").transform);

            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();

        } else
        {

            GameObject n1ewVeritaMan = (GameObject)PhotonNetwork.Instantiate("r_superpawn 1", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0);

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("R").transform);

            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();
        }


        newVeritaMan.pos = new Vector2(x, y);

        veritaMenOnBoard[x, y] = newVeritaMan;

        return newVeritaMan;

    }



    public VeritaMen SpawnBulldozer(Team team, int x, int y)
    {

		if (!PhotonNetwork.isMasterClient) {

    		return null;

    	}

        VeritaMen newVeritaMan;

        if (team == Team.BLUE)
        {

            GameObject n1ewVeritaMan = (GameObject)PhotonNetwork.Instantiate("b_bulldozer", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0);

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("B").transform);
            
            //calling this function to assign the movePath array based on which the piece will move.
            n1ewVeritaMan.GetComponent<Bulldozer>().AssignMovePathAndDeathSquares();

            //Both bulldozers start at the 0th position.
            //indexing the currentPos from 0 so that it can be directly used as array indexes
            n1ewVeritaMan.GetComponent<Bulldozer>().currentPos = 0;

            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();

            walls.theBlueBd = newVeritaMan;

        }
        else
        {

            GameObject n1ewVeritaMan = (GameObject)PhotonNetwork.Instantiate("r_bulldozer", GlobalFunctions.GridToWorldCoordinates(new Vector2(x, y)), Quaternion.identity, 0);

            n1ewVeritaMan.transform.SetParent(this.transform.Find("Pieces").transform.Find("R").transform);

            //calling this function to assign the movePath array based on which the piece will move.
            n1ewVeritaMan.GetComponent<Bulldozer>().AssignMovePathAndDeathSquares();

            //Both bulldozers start at the 0th position.
            //indexing the currentPos from 0 so that it can be directly used as array indexes
            n1ewVeritaMan.GetComponent<Bulldozer>().currentPos = 0;

            newVeritaMan = n1ewVeritaMan.GetComponent<VeritaMen>();

            walls.theRedBd = newVeritaMan;
        }

        newVeritaMan.pos = new Vector2(x, y);

        veritaMenOnBoard[x, y] = newVeritaMan;

        return newVeritaMan;

    }
    


	void SwitchTeamMoveBulldozerRepsawnAndCheck2ndWinCondition ()
	{
		//this looks cramped because it was refactored into a new method from the SwitchTurn() method.

		//take care that this method is called through Invoke so if it is modified in a hard way then the Invokes probably
		//  need to be updated.

		if (currenTurnTemp == Team.BLUE) {

            if (this.transform.Find ("Pieces/R").gameObject.transform.GetComponentInChildren<Bulldozer> ()) {

                this.transform.Find ("Pieces/R").gameObject.transform.GetComponentInChildren<Bulldozer> ().SpecialMoveBulldozer ();
			}

            turnManager.TurnEnded (Team.BLUE);

            ResetCoolDownAndAbilities (Team.RED);

            currentTurn = Team.RED;

            this.gameObject.GetComponent<RespawnManager> ().RespawnNextPieceOf (Team.RED);

            //updating veritaman states and sprites
            foreach(VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
            {
                
                if (v.team == Team.BLUE)
                {

                    if(v.tempState.Contains(VeritaManState.TURNGLOW))
                    {

                        v.tempState.Remove(VeritaManState.TURNGLOW);

                    }

                } else
                {
                    if (!v.tempState.Contains(VeritaManState.TURNGLOW) && v.veritamenTypeOfThis != VeritaMenType.BULLDOZER)
                    {

                        v.tempState.Add(VeritaManState.TURNGLOW);

                    }
                }

                v.gameObject.GetComponent<SpriteManager>().CallPRPCUpdateSprites();

            }



		}
		else {

            if (this.transform.Find ("Pieces/B").gameObject.transform.GetComponentInChildren<Bulldozer> ()) {

                this.transform.Find ("Pieces/B").gameObject.transform.GetComponentInChildren<Bulldozer> ().SpecialMoveBulldozer ();
			}

            turnManager.TurnEnded (Team.RED);

            ResetCoolDownAndAbilities (Team.BLUE);

            currentTurn = Team.BLUE;

            this.gameObject.GetComponent<RespawnManager> ().RespawnNextPieceOf (Team.BLUE);

            //updating veritaman states and sprites
            foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
            {

                if (v.team == Team.BLUE)
                {
                    if (!v.tempState.Contains(VeritaManState.TURNGLOW) && v.veritamenTypeOfThis != VeritaMenType.BULLDOZER)
                    {

                        v.tempState.Add(VeritaManState.TURNGLOW);
                    }
                }
                else
                {
                    if (v.tempState.Contains(VeritaManState.TURNGLOW))
                    {

                        v.tempState.Remove(VeritaManState.TURNGLOW);

                    }

                }

                v.gameObject.GetComponent<SpriteManager>().CallPRPCUpdateSprites();

            }
        }

		UpdateRespawnUI();

        PhotonView pv = (PhotonView)clientTeam_photonVeiw[currentTurn];

        List<int> cdArray = currentTurn == Team.BLUE ? tracker.blueAbilitiesCooldown : tracker.redAbilitiesCooldown;

        byte[] serializedCDArray = GlobalFunctions.SerializeToByteArray<List<int>>(cdArray);

        pv.RPC("PRPC_ShowAbilityReadyEffect", PhotonTargets.AllViaServer, serializedCDArray);

        //need to check win condition of the team before switch turn because if the current team's pieces are dead, that is
        //  the win condition of the opposite team (earlier team).

        Check2ndWinConditionOf(currenTurnTemp);

	}


	public void Check2ndWinConditionOf (Team team) {

		//SECOND WIN CONDITION
    	// if no piece other than the Bulldozer survives on the board
    	if (team == Team.BLUE) {

	    	int numberOfAliveVeritaMen = 0;
	    	bool isBDAlive = false;

	    	for (int i = 0; i<7; i++) {

	    		for (int j = 0; j<7; j++) {

	    			if ( (veritaMenOnBoard[i,j] != null) && (veritaMenOnBoard[i,j].team == Team.RED) ) {

	    				numberOfAliveVeritaMen++;

	    				if (veritaMenOnBoard[i,j].gameObject.GetComponent<Bulldozer>()) {

	    					isBDAlive = true;

	    				}

	    			}

	    		}

	    	}


	    	// If no VeritaMen is alive OR only one VeritMan is alive and that is the Bulldozer, game over
	    	if ((numberOfAliveVeritaMen == 0) || ( (isBDAlive) && (numberOfAliveVeritaMen == 1) ) ) {

				photonView.RPC("PRPC_GameEndedWithWinner", PhotonTargets.AllViaServer, Team.BLUE);

	    		PhotonView pvBlue = (PhotonView) clientTeam_photonVeiw[Team.BLUE];

	    		PhotonView pvRed = (PhotonView) clientTeam_photonVeiw[Team.RED];

	    		pvBlue.RPC("PRPC_OnWinConditionSatisfied", PhotonTargets.AllViaServer, true);

                pvRed.RPC("PRPC_OnWinConditionSatisfied", PhotonTargets.AllViaServer, false);

            }

    	} else {

			int numberOfAliveVeritaMen = 0;
	    	bool isBDAlive = false;

	    	for (int i = 0; i<7; i++) {

	    		for (int j = 0; j<7; j++) {

	    			if ( (veritaMenOnBoard[i,j] != null) && (veritaMenOnBoard[i,j].team == Team.BLUE) ) {

	    				numberOfAliveVeritaMen++;

	    				if (veritaMenOnBoard[i,j].gameObject.GetComponent<Bulldozer>()) {

	    					isBDAlive = true;

	    				}

	    			}

	    		}

	    	}


	    	// If no VeritaMen is alive OR only one VeritMan is alive and that is the Bulldozer, game over
	    	if ((numberOfAliveVeritaMen == 0) || ( (isBDAlive) && (numberOfAliveVeritaMen == 1) ) ) {

				photonView.RPC("PRPC_GameEndedWithWinner", PhotonTargets.AllViaServer, Team.RED);

	    		PhotonView pvBlue = (PhotonView) clientTeam_photonVeiw[Team.BLUE];

	    		PhotonView pvRed = (PhotonView) clientTeam_photonVeiw[Team.RED];

	    		pvBlue.RPC("PRPC_OnWinConditionSatisfied", PhotonTargets.AllViaServer, false);

                pvRed.RPC("PRPC_OnWinConditionSatisfied", PhotonTargets.AllViaServer, true);

            }


    	}
	}



	//this needs to happen on both clients, thus ClientRpc
	[PunRPC]
	public void PRPC_DeactivateTheTurnChangeIndicator() {

		//function is called two times ONLY in this script and using Invoke.

		this.transform.Find("TurnChange").transform.Find("your_turn").gameObject.SetActive(false);
		this.transform.Find("TurnChange").transform.Find("enemy_turn").gameObject.SetActive(false);
		this.transform.Find("TurnChange").transform.Find("turnChangeGlow_PS").gameObject.SetActive(false);

	}


	//for some reason, invoke is not working with ClientRpc so routing it through another method
	public void CallRpcDeactivateTheTurnChangeIndicator() {

        photonView.RPC("PRPC_DeactivateTheTurnChangeIndicator", PhotonTargets.AllViaServer);

        this.gameObject.GetComponent<AudioNetworking>().CallPRPCInterrupt();

        this.GetComponent<AudioNetworking>().CallPRPCPlaySound("turnchange_audio");

    }

    public void SwitchTurn(bool hasTurnTimedout)
    {
        respawnManager.gameObject.GetComponent<PhotonView>().RPC("PRPC_DeactivateRespawnAuras", PhotonTargets.AllViaServer);

    	photonView.RPC("PRPC_HideAllHighlightsEverywhere", PhotonTargets.AllViaServer);
                

        DoubleMove doubleMoveAbility = GameObject.FindObjectOfType<DoubleMove>();

        //If the turn is timedOut, then the interrupts must be called immediately.

        if (hasTurnTimedout)
        {

            photonView.RPC("PRPC_InterruptReset", PhotonTargets.AllViaServer);

        }
        else
        {
            if (isDoubleMove)
            {

                if (firstStepOverDM)
                {
                    isDoubleMove = false;

                    firstStepOverDM = false;

                    doubleMoveAbility.CallUpdateDMIndicator(currentTurn == Team.BLUE ? true : false, 2);

                }
                else
                {
                    firstStepOverDM = true;

                    doubleMoveAbility.CallUpdateDMIndicator(currentTurn == Team.BLUE ? true : false, 1);

                    return;
                }

            }


            photonView.RPC("PRPC_InterruptReset", PhotonTargets.AllViaServer);

        }




        //using the temporary currenTurn var to store the state of currentTurn before we make it null.
        //currentTurn is used everywhere to check whether it is allowed to execute certain functions.
        currenTurnTemp = (Team) currentTurn;

 	 	currentTurn = null;

 	 	//For 1.5 sec now, we need the board to be in a "neutral" state in which it is no one's turn. 

 	 	if (currenTurnTemp == Team.BLUE) {

 	 		PhotonView pv = (PhotonView) clientTeam_photonVeiw[Team.RED];

 	 		pv.RPC("PRPC_ShowTurnIndicator", PhotonTargets.AllViaServer);

 	 	} else {

			PhotonView pv = (PhotonView) clientTeam_photonVeiw[Team.BLUE];

            pv.RPC("PRPC_ShowTurnIndicator", PhotonTargets.AllViaServer);

        }
         	 	
		Invoke("CallRpcDeactivateTheTurnChangeIndicator", 1.3f);

		Invoke("SwitchTeamMoveBulldozerRepsawnAndCheck2ndWinCondition", 2.3f);

        //calling the update respawn visuals after the respawn has actually happened

    }


	void ResetStonePieces (Team team)
	{
		// reseting the stonepieces to normal when its time is up
		StonePieces theStonePiecesAbility = GameObject.FindObjectOfType<StonePieces> ();
		//finding the stonePieces abilities object that is relevant

		if (!theStonePiecesAbility) {
			return;
		}

		if (team == Team.BLUE) {
			if (tracker.blueBackToNormal_SP == turnManager.blueTurn) {

				theStonePiecesAbility.RevertAppearanceToNormal(true);

				tracker.blueBackToNormal_SP = -1;

				stonePiecesOfTeam[Team.BLUE] = false;
			}
		}
		else {
			if (tracker.redBackToNormal_SP == turnManager.redTurn) {

                theStonePiecesAbility.RevertAppearanceToNormal(false);

				tracker.redBackToNormal_SP = -1;

				stonePiecesOfTeam[Team.RED] = false;
			}
		}


	

	}


	private void KillCorruptedVeritaMen(Team team) {


		//loop through all pieces to see if there are any corrupted pieces that need to die

		for (int i =0; i<7; i++) {

			for (int j = 0; j<7; j++) {

				if (veritaMenOnBoard[i, j] && (veritaMenOnBoard[i,j].team == team ) ) {

					if (team == Team.BLUE) {

						if (veritaMenOnBoard[i,j].killThisAtTurn == turnManager.blueTurn) {

							PhotonNetwork.Destroy(veritaMenOnBoard[i,j].gameObject);

							veritaMenOnBoard[i,j] = null;

						} else if (veritaMenOnBoard[i, j].killThisAtTurn == (turnManager.blueTurn + 1))
                        {
                            //veritaMan to die because of corruption in the next turn.
                            
                            veritaMenOnBoard[i, j].GetComponent<PhotonView>().RPC("PRPC_ActivateSkullEffect", PhotonTargets.AllViaServer);


                        }

                    } else {

						if (veritaMenOnBoard[i,j].killThisAtTurn == turnManager.redTurn) {

							PhotonNetwork.Destroy(veritaMenOnBoard[i,j].gameObject);

							veritaMenOnBoard[i,j] = null;

						} else if (veritaMenOnBoard[i, j].killThisAtTurn == (turnManager.redTurn + 1))
                        {
                            //veritaMan to die because of corruption in the next turn.

                            veritaMenOnBoard[i, j].GetComponent<PhotonView>().RPC("PRPC_ActivateSkullEffect", PhotonTargets.AllViaServer);    


                        }

                    }

				}

			}

		}


	}



    public void ResetCoolDownAndAbilities(Team team) {

		ResetStonePieces (team);

		KillCorruptedVeritaMen(team);

		if (team == Team.BLUE) {

			for (int i = 0; i<6; i++) {

				if (tracker.blueAbilitiesCooldown[i] == turnManager.blueTurn) {

					tracker.blueAbilitiesCooldown[i] = -1;

				} 

			}
				

		} else {

			for (int i = 0; i<6; i++) {

				if(tracker.redAbilitiesCooldown[i] == turnManager.redTurn) {

					tracker.redAbilitiesCooldown[i] = -1;

				}

			}

		}

		tracker.UpdateAbilityVisualsOfTeam(team);

    }


    [PunRPC]

    public void PRPC_InterruptReset() {

    	selectedVeritaMan = null;

    	boardHighlighter.HideHighlights();

    	GameObject specialAbilitiesManager = GameObject.Find("SpecialAbilitiesManager");

    	specialAbilitiesManager.GetComponent<RookBishopSwap>().InterruptReset();

    	specialAbilitiesManager.GetComponent<Fortify>().InterruptReset();

    	specialAbilitiesManager.GetComponent<DoubleMove>().InterruptReset();

    	specialAbilitiesManager.GetComponent<Sacrifice>().InterruptReset();

    }



	public void InterruptReset(NetworkConnection connection) {

    	selectedVeritaMan = null;

    	boardHighlighter.HideHighlights();

    	GameObject specialAbilitiesManager = GameObject.Find("SpecialAbilitiesManager");

    	specialAbilitiesManager.GetComponent<RookBishopSwap>().InterruptReset();

    	specialAbilitiesManager.GetComponent<Fortify>().InterruptReset();

    	specialAbilitiesManager.GetComponent<DoubleMove>().InterruptReset();

    	specialAbilitiesManager.GetComponent<Sacrifice>().InterruptReset();

    }


    [PunRPC]

    void PRPC_HideAllHighlightsEverywhere() {

    	boardHighlighter = this.gameObject.GetComponent<BoardHighlighting>();

    	boardHighlighter.HideHighlights();

    }



    //function to update the respawnUI

    public void UpdateRespawnUI ( ) {

    	//using VeritaManID = 1 (queen) whenever it doesnt matter which veritaMan type is passed.
    	// the update function on RespawnUI checked for the -1 first and if there is a -1 then the disabled state is activated

    	List<VeritaMenType> veritaManRespawnList = respawnManager.ReturnRespawnListOfTeam(Team.BLUE);

    	List<int> respawnTurnList ;

    	if (veritaManRespawnList.Count > 0) {

    		respawnTurnList = respawnManager.ReturnRespawnTurnListOfTeam(Team.BLUE);


    		if (veritaManRespawnList.Count == 1) {

    			int cd =  respawnTurnList[0] - turnManager.blueTurn + 1; 

				photonView.RPC("PRPC_CallingActualUpdate", PhotonTargets.AllViaServer, true, GlobalFunctions.ResolveVeritaManTypeToID(veritaManRespawnList[0]), cd, 1, -1);

    		} else {

    			int cd1 = respawnTurnList[0] - turnManager.blueTurn + 1;

    			int cd2 = respawnTurnList[1] - turnManager.blueTurn + 1;

				photonView.RPC("PRPC_CallingActualUpdate", PhotonTargets.AllViaServer, true, GlobalFunctions.ResolveVeritaManTypeToID(veritaManRespawnList[0]), cd1, GlobalFunctions.ResolveVeritaManTypeToID(veritaManRespawnList[1]), cd2);

    		}



    	} else {

			photonView.RPC("PRPC_CallingActualUpdate", PhotonTargets.AllViaServer,  true, 1, -1, 1, -1);

    	}



    	// same thing as above for the red team (reusing the variables)


		veritaManRespawnList = respawnManager.ReturnRespawnListOfTeam(Team.RED);


    	if (veritaManRespawnList.Count > 0) {

    		respawnTurnList = respawnManager.ReturnRespawnTurnListOfTeam(Team.RED);


    		if (veritaManRespawnList.Count == 1) {

    			int cd =  respawnTurnList[0] - turnManager.redTurn + 1; 

				photonView.RPC("PRPC_CallingActualUpdate", PhotonTargets.AllViaServer, false, GlobalFunctions.ResolveVeritaManTypeToID(veritaManRespawnList[0]), cd, 1, -1);

    		} else {

    			int cd1 = respawnTurnList[0] - turnManager.redTurn + 1;

    			int cd2 = respawnTurnList[1] - turnManager.redTurn + 1;

				photonView.RPC("PRPC_CallingActualUpdate", PhotonTargets.AllViaServer, false, GlobalFunctions.ResolveVeritaManTypeToID(veritaManRespawnList[0]), cd1, GlobalFunctions.ResolveVeritaManTypeToID(veritaManRespawnList[1]), cd2);

    		}



    	} else {

			photonView.RPC("PRPC_CallingActualUpdate", PhotonTargets.AllViaServer, false, 1, -1, 1, -1);

    	}


    }


    [PunRPC]

    void PRPC_CallingActualUpdate(bool isTeamBlue, int nextVeritaManID, int cd1, int afterNextVeritaManID, int cd2) {

    	Team team;

    	if(isTeamBlue) {

    		team = Team.BLUE;

    	} else {

    		team = Team.RED;

    	}

    	VeritaMenType veritaMan1 = (VeritaMenType) GlobalFunctions.ResolveIDToVeritaManType(nextVeritaManID);

    	VeritaMenType veritaMan2 = (VeritaMenType) GlobalFunctions.ResolveIDToVeritaManType(afterNextVeritaManID);


    	this.gameObject.GetComponentInChildren<RespawnUI>().UpdateCDVisuals(team, veritaMan1, cd1, veritaMan2, cd2);

    }



    public Team? WinnerOfCurrentGame() {

    	if (GlobalFunctions.theGameState == GameState.LOST || GlobalFunctions.theGameState == GameState.WON) {

    		return winnerOfGame;

    	}

    	return null; 

    }

	// If I make the game ending functions ClientRpc, then both the server and the client will be informed whether the
    // 	game has truly ended or not. 

    [PunRPC]
    
    public void PRPC_GameEndedWithWinner (Team team) {

        Team losingTeam;

        if (team == Team.BLUE)
        {

            losingTeam = Team.RED;

        } else
        {

            losingTeam = Team.BLUE;

        }


    	winnerOfGame = team;

        PhotonView pv = (PhotonView)clientTeam_photonVeiw[team];

        pv.RPC("PRPC_ResolveGameState", PhotonTargets.All, GlobalFunctions.ResolveGameStatetoID(GameState.WON));

        pv = (PhotonView)clientTeam_photonVeiw[losingTeam];

        pv.RPC("PRPC_ResolveGameState", PhotonTargets.All, GlobalFunctions.ResolveGameStatetoID(GameState.LOST));
        
    }



    //need to call this from outside the VeritaMan script because when the VeritaMan is destroyed, this function 
    // goes too.
	public void InvokeDeactivateSpecialEffectsInXSeconds(float x) {

		Invoke("DeactivateSpecialEffects", x);

	}


	public void DeactivateSpecialEffects() {

    	//called using the Invoke function
    	Debug.Log("SFX deactivate");

    	this.transform.Find("SpecialEffects").transform.Find("DeathSplatter").transform.Find("r_death").gameObject.SetActive(false);

    	this.transform.Find("SpecialEffects").transform.Find("DeathSplatter").transform.Find("b_death").gameObject.SetActive(false);

        this.transform.Find("SpecialEffects").transform.Find("WallBreaking").gameObject.SetActive(false);
    }





}
