﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class WallManager :  MonoBehaviour {


    public GameObject[,] wallsArray = new GameObject[2, 6];

    //If the pointBlocks satisfy the equation of the line formed by source and potential-destination squares, it means 
    //  that the route is blocked.
    // HG : High Ground
    // WS : Win Square
    public Vector2[][] pointBlocksHG;
    public Vector2[][] pointBLocksWS;


    private bool r_BdInPositionToDestroy = false;
    private bool b_BdInPositionToDestroy = false;


    public int theR_wallUnderAttack = 0;
    public int theB_wallUnderAttack = 0;

    // Attack positions on Blue : 0, Red : 1
    // W1 - 0, w6 - 5
    //x - 0, y - 0
    // So basically arrayOfAttackPositions [blue or red][w1-6][x or y]
    private int[][][] arrayOfAttackPositions;

    //We need to keep track of where the bulldozer was in the previous turn for the wall destroying mechanism.
    public VeritaMen theRedBd ;
    public VeritaMen theBlueBd ;

    // Will use the wall_block gameobject to spawn the wall blocks on the server
    public GameObject wall_block;


    public void StartOnBoardLoad()
    {

    	SpawnWallsOnNetwork();
    	
        InitializeWallsArray();
        InitializeArrayOfAttackPositions();


    }


    private void SpawnWallsOnNetwork() {

    	//Network spawn functions should be executed only on the server.

    	if (!PhotonNetwork.isMasterClient) {	

    		return;

    	}

    	//First lets spawn the blue walls 
    	// There are 6 walls to be spawned each with a fixed position


    	Vector2 wall_pos = new Vector2(4.5f, 6f);

		GameObject currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.Euler(new Vector3(0, 0, 180)), 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("B").transform);

		currentWallSpawned.name = "w1";



		wall_pos = new Vector2(4.5f, 5f);

        currentWallSpawned = (GameObject)PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos), Quaternion.Euler(new Vector3(0, 0, 180)), 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("B").transform);

        currentWallSpawned.name = "w2";

		


		wall_pos = new Vector2(5f, 4.5f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos), Quaternion.Euler(new Vector3(0,0,-90)), 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("B").transform);

        currentWallSpawned.name = "w3";



		wall_pos = new Vector2(6f, 4.5f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.Euler(new Vector3(0,0,-90)), 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("B").transform);

        currentWallSpawned.name = "w4";



		wall_pos = new Vector2(5.5f, 6f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.Euler(new Vector3(0, 0, 180)), 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("B").transform);

        currentWallSpawned.name = "w5";



        wall_pos = new Vector2(6f, 5.5f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.Euler(new Vector3(0,0,-90)) , 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("B").transform);

        currentWallSpawned.name = "w6";





		// **********RED WALLS************



		wall_pos = new Vector2(1.5f, 0f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.identity, 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("R").transform);

        currentWallSpawned.name = "w1";



		wall_pos = new Vector2(1.5f, 1f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.identity, 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("R").transform);

        currentWallSpawned.name = "w2";



		wall_pos = new Vector2(1f, 1.5f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.Euler(new Vector3(0,0,90)), 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("R").transform);

        currentWallSpawned.name = "w3";



		wall_pos = new Vector2(0f, 1.5f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.Euler(new Vector3(0,0,90)) , 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("R").transform);

        currentWallSpawned.name = "w4";



		wall_pos = new Vector2(0.5f, 0f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.identity, 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("R").transform);

        currentWallSpawned.name = "w5";



		wall_pos = new Vector2(0f, 0.5f);

		currentWallSpawned = (GameObject) PhotonNetwork.Instantiate("wall_block", GlobalFunctions.GridToWorldCoordinates(wall_pos) , Quaternion.Euler(new Vector3(0,0,90)) , 0);

        currentWallSpawned.transform.SetParent(this.transform.Find("R").transform);

        currentWallSpawned.name = "w6";



    }





    private void InitializeWallsArray()
    {

        // Since the walls are manually added to the game from the beginning, need some code that will add the walls to the array. 

        Transform r_walls = this.transform.Find("R").transform;

        Transform b_walls = this.transform.Find("B").transform;

        int i = 0;

        foreach (Transform child in b_walls)
        {

            wallsArray[0, i] = child.gameObject;

            i++;

        }

        i = 0;

        foreach (Transform child in r_walls)
        {

            wallsArray[1, i] = child.gameObject;

            i++;

        }



        //Initialize the point blockers while initializing the wall array.
        
        //Start with initializing the high ground point blockers first

        Vector2 p1 = new Vector2( 4.5f, 6f );
        Vector2 p2 = new Vector2( 4.5f, 5.5f);
        Vector2 p3 = new Vector2(4.5f, 5f);
        Vector2 p4 = new Vector2(4.5f, 4.5f);
        Vector2 p5 = new Vector2(5f, 4.5f);
        Vector2 p6 = new Vector2(5.5f, 4.5f);
        Vector2 p7 = new Vector2(6f, 4.5f);

        Vector2[] bluePointBlockers = { p1, p2, p3, p4, p5, p6, p7 };

        p1 = new Vector2(1.5f, 0f);
        p2 = new Vector2(1.5f, 0.5f);
        p3 = new Vector2(1.5f, 1f);
        p4 = new Vector2(1.5f, 1.5f);
        p5 = new Vector2(1f, 1.5f);
        p6 = new Vector2(0.5f, 1.5f);
        p7 = new Vector2(0f, 1.5f);

        Vector2[] redPointBlockers = { p1, p2, p3, p4, p5, p6, p7 };


        pointBlocksHG = new Vector2[][] { bluePointBlockers, redPointBlockers };


        // Initialize the Win Square Blockers

        p1 = new Vector2(6f, 5.5f);
        p2 = new Vector2(5.5f, 5.5f);
        p3 = new Vector2(5.5f, 6f);

        Vector2[] bluePointBlockersWS = { p1, p2, p3 };


        p1 = new Vector2(0.5f, 0f);
        p2 = new Vector2(0.5f, 0.5f);
        p3 = new Vector2(0f, 0.5f);

        Vector2[] redPointBlockersWS = { p1, p2, p3 };

        pointBLocksWS = new Vector2[][] { bluePointBlockersWS, redPointBlockersWS };

    }


    private void InitializeArrayOfAttackPositions()
    {
        int[] attackRedw1 = { 1, 0 };
        int[] attackRedw2 = { 1, 1 };
        int[] attackRedw3 = { 1, 1 };
        int[] attackRedw4 = { 0, 2 };
        int[] attackRedw5 = { 1, 0 };
        int[] attackRedw6 = { 0, 1 };

        int[] attackBluew1 = { 5, 6 };
        int[] attackBluew2 = { 5, 5 };
        int[] attackBluew3 = { 5, 5 };
        int[] attackBluew4 = { 6, 4 };
        int[] attackBluew5 = { 5, 6 };
        int[] attackBluew6 = { 6, 5 };



        int[][] allRedAttackPos = { attackRedw1, attackRedw2, attackRedw3, attackRedw4, attackRedw5, attackRedw6 };
        int[][] allBlueAttackPos = { attackBluew1, attackBluew2, attackBluew3, attackBluew4, attackBluew5, attackBluew6 };

        arrayOfAttackPositions = new int[][][] { allBlueAttackPos, allRedAttackPos};


    }




    public void SiegeWallOf(int a, int wallID)
    {
        //An enemy bulldozer has reached the wall belonging to the team passed in this method 

        //wall ID is from 0 to 5
        wallsArray[a, wallID - 1].GetComponent<Wall>().isBurning = true;

        wallsArray[a, wallID - 1].GetComponent<Wall>().UpdateWallSpriteAndAnim();
        
        if (a == 0)
        {

            r_BdInPositionToDestroy = true;
            
            //keeping this the same as wallID only to subtract 1 later (since the null case for this is 0)
            theB_wallUnderAttack = wallID;
            
        } else
        {

            b_BdInPositionToDestroy = true;

            theR_wallUnderAttack = wallID;

        }
        

    }

    public void ResetSiegeValues(Team team)
    {
        if (team == Team.BLUE)
        {

            b_BdInPositionToDestroy = false;
            theR_wallUnderAttack = 0;

            //put out the fire!
            for (int i = 0; i < 6; i++)
            {

                if (wallsArray[1, i])
                {

                    wallsArray[1, i].GetComponent<Wall>().isBurning = false;

                    wallsArray[1, i].GetComponent<Wall>().UpdateWallSpriteAndAnim();

                }

            }

        } else
        {

            r_BdInPositionToDestroy = false;
            theB_wallUnderAttack = 0;

            //put out the fire!
            for (int i = 0; i < 6; i++)
            {

                if (wallsArray[0, i])
                {

                    wallsArray[0, i].GetComponent<Wall>().isBurning = false;

                    wallsArray[0, i].GetComponent<Wall>().UpdateWallSpriteAndAnim();

                }

            }

        }



    }


    private void UpdatePointBlockers(int teamInt, int theDestroyedWallID)
    {

        if (theDestroyedWallID > 4)
        {

            if (theDestroyedWallID == 5)
            {

                pointBLocksWS[teamInt][0] = new Vector3(93, 3);

                pointBLocksWS[teamInt][1] = new Vector3(93, 3);

            } else if (theDestroyedWallID == 6)
            {

                pointBLocksWS[teamInt][1] = new Vector3(93, 3);

                pointBLocksWS[teamInt][2] = new Vector3(93, 3);

            }
            
        } else
        {

             if (theDestroyedWallID == 1)
            {

                pointBlocksHG[teamInt][0] = new Vector2(93, 3);

                pointBlocksHG[teamInt][1] = new Vector2(93, 3);

            }
            else if (theDestroyedWallID == 2)
            {

                pointBlocksHG[teamInt][1] = new Vector2(93, 3);

                pointBlocksHG[teamInt][2] = new Vector2(93, 3);

                pointBlocksHG[teamInt][3] = new Vector2(93, 3);
                
            }
            else if (theDestroyedWallID == 3)
            {

                pointBlocksHG[teamInt][3] = new Vector2(93, 3);

                pointBlocksHG[teamInt][4] = new Vector2(93, 3);

                pointBlocksHG[teamInt][5] = new Vector2(93, 3);

            }
            else if (theDestroyedWallID == 4)
            {

                pointBlocksHG[teamInt][5] = new Vector2(93, 3);

                pointBlocksHG[teamInt][6] = new Vector2(93, 3);

            }



        }


    }







    public void DestroyWallOf(Team team)
    {

        //The team passed in this method is the team whose wall will be destroyed 

        //check if r was in position to destroy 
        if (team == Team.BLUE)
        {
            if (r_BdInPositionToDestroy && theRedBd)
            {

                //check if position of bd at the beginning of this turn allows it to destroy the wall it is attacking
                // if YES, then destroy the wall
                // if NO, then do nothing

                if ((int)theRedBd.pos.x == arrayOfAttackPositions[0][theB_wallUnderAttack - 1][0] && (int)theRedBd.pos.y == arrayOfAttackPositions[0][theB_wallUnderAttack - 1][1])
                {

                    PhotonNetwork.Destroy(wallsArray[0, theB_wallUnderAttack - 1]);
                    UpdatePointBlockers(0, theB_wallUnderAttack);
                    ResetSiegeValues(Team.RED);
                    
                } 

            } 

        }
        else
        {

            

            if (b_BdInPositionToDestroy && theBlueBd)
            {

                //check if position of bd at the beginning of this turn allows it to destroy the wall it is attacking
                // if YES, then destroy the wall
                // if NO, then do nothing

                

                if ((int)theBlueBd.pos.x == arrayOfAttackPositions[1][theR_wallUnderAttack - 1][0] && (int)theBlueBd.pos.y == arrayOfAttackPositions[1][theR_wallUnderAttack - 1][1])
                {
                    
                    PhotonNetwork.Destroy(wallsArray[1, theR_wallUnderAttack - 1]);
                    UpdatePointBlockers(1, theR_wallUnderAttack);
                    ResetSiegeValues(Team.BLUE);
                    
                    
                } 

            }
        }
    }




}
