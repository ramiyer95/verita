﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioNetworking : Photon.MonoBehaviour {

    private AudioManager audioManager;


    private void Start()
    {

        audioManager = GameObject.FindObjectOfType<AudioManager>();

    }



    //Need to create an additional wrapper because PlaySound can be either -> play on both clients, play on 1 client
    [PunRPC]

    private void PRPC_PlaySound(string soundName)
    {

        audioManager.PlaySound(soundName);

    }


    [PunRPC]

    private void PRPC_InterruptSound()
    {

        foreach (Sound s in audioManager.sounds)
        {

            if (s.layer == AudioLayer.INDICATIONS || s.layer == AudioLayer.ABILITIES)
            {

                s.source.Stop();

            }

        }

        audioManager.VolumeLayerUpdater();

    }



    public void CallPRPCPlaySound(string soundName)
    {

        photonView.RPC("PRPC_PlaySound", PhotonTargets.All, soundName);

    }


    public void CallPRPCInterrupt()
    {

        photonView.RPC("PRPC_InterruptSound", PhotonTargets.All);
        
    }



}
