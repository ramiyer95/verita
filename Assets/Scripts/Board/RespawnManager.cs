﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RespawnManager : Photon.MonoBehaviour {

	private TurnManager turnManager;

	private Board board;

	[SerializeField]
	private List<VeritaMenType> blueRespawnList = new List<VeritaMenType>();

	[SerializeField]
	private List<VeritaMenType> redRespawnList = new List<VeritaMenType>();

	[SerializeField]
	private List<int> blueRespawnTurnList = new List<int>();

	[SerializeField]
	private List<int> redRespawnTurnList = new List<int>();

	public GameObject b_respawnAura;
	public GameObject r_respawnAura;


	//Since the list of the turns and veritaMen is separate, they should be modified only through a handler method, to make 
	//  sure that only one of them is not updated by mistake. 

	//Similarly, they should be accessed through methods also, to avoid any mismatch.



	void Start() {

		turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();

		board = this.GetComponent<Board>();

	}



	public void AddVeritaManToRespawnList ( VeritaMenType veritaMan1, int respawnAtTurn, Team team ) {

		if (team == Team.BLUE) {

			blueRespawnList.Add (veritaMan1 ) ;

			blueRespawnTurnList.Add (respawnAtTurn) ;

		} else {

			redRespawnList.Add( veritaMan1 );

			redRespawnTurnList.Add (respawnAtTurn);

		}

	}


	public List<VeritaMenType> ReturnRespawnListOfTeam (Team team) {

		if (team == Team.BLUE ) {

			return blueRespawnList;

		} else {

			return redRespawnList;

		}

	}


	public List<int> ReturnRespawnTurnListOfTeam (Team team) {

		if (team == Team.BLUE) {

			return blueRespawnTurnList;

		} else {

			return redRespawnTurnList;

		}

	}



	public int ReturnNextRespawnTurnOfTeam (Team team) {



		if (team == Team.BLUE) {

			if (blueRespawnTurnList.Count == 0) {

				return -1;

			}

			return blueRespawnTurnList[0];

		} else {

			if (redRespawnTurnList.Count == 0) {

				return -1;

			}

			return redRespawnTurnList[0];

		}

	}


	public VeritaMenType ReturnNextRespawnVeritaManTypeOfTeam ( Team team ) {

		if (team == Team.BLUE) {

			return blueRespawnList[0];

		} else {

			return redRespawnList[0];

		}

	}


	public void RespawnNextPieceOf (Team team) {

		if (team == Team.BLUE) {
				
			int respawnNextBlueOnTurn = ReturnNextRespawnTurnOfTeam(Team.BLUE);

			if (respawnNextBlueOnTurn == turnManager.blueTurn) {

				photonView.RPC("PRPC_ActivateBlueRespawnAura", PhotonTargets.AllViaServer);

				// Destroy the piece currently sitting on the respawn square, if it exists

				if (board.veritaMenOnBoard[4, 6]) {

					PhotonNetwork.Destroy(board.veritaMenOnBoard[4, 6].gameObject);

				}


				VeritaMenType nextVeritaManToSpawn = ReturnNextRespawnVeritaManTypeOfTeam(Team.BLUE);

				switch (nextVeritaManToSpawn) {

					case VeritaMenType.QUEEN : 

						board.SpawnQueen(Team.BLUE, 4, 6);

						break;


					case VeritaMenType.ROOK :

						board.SpawnRook(Team.BLUE, 4, 6);

						break;


					case VeritaMenType.BISHOP :

						board.SpawnBishop(Team.BLUE, 4, 6);

						break;


					case VeritaMenType.SUPERPAWN :

						board.SpawnSuperpawn(Team.BLUE, 4, 6);

						break;


					case VeritaMenType.BULLDOZER :

						board.SpawnBulldozer(Team.BLUE, 4, 6);

						break;

				}

				//remove the first elements from the relevant lists

				blueRespawnList.RemoveAt(0);

				blueRespawnTurnList.RemoveAt(0);

			}

		} else {


			int respawnNextRedOnTurn = ReturnNextRespawnTurnOfTeam(Team.RED);

			if (respawnNextRedOnTurn == turnManager.redTurn) {

                // destroy veritaMen on the respawn square if they exist 

                photonView.RPC("PRPC_ActivateRedRespawnAura", PhotonTargets.AllViaServer);

				if(board.veritaMenOnBoard[2, 0]) {

					PhotonNetwork.Destroy(board.veritaMenOnBoard[2,0].gameObject);

				}

				VeritaMenType nextVeritaManToSpawn = ReturnNextRespawnVeritaManTypeOfTeam(Team.RED);

                board.GetComponent<AudioNetworking>().CallPRPCPlaySound("respawn_audio");

                switch (nextVeritaManToSpawn) {

					case VeritaMenType.QUEEN :

						board.SpawnQueen(Team.RED, 2, 0);

						break;

					case VeritaMenType.ROOK :

						board.SpawnRook(Team.RED, 2, 0);

						break;

					case VeritaMenType.BISHOP :

						board.SpawnBishop(Team.RED, 2, 0);

						break;

					case VeritaMenType.SUPERPAWN :

						board.SpawnSuperpawn( Team.RED, 2, 0);

						break;

					case VeritaMenType.BULLDOZER :

						board.SpawnBulldozer( Team.RED, 2, 0);

						break;

				}

				redRespawnList.RemoveAt(0);

				redRespawnTurnList.RemoveAt(0);

			}



		}

	}


	[PunRPC] 

	void PRPC_ActivateBlueRespawnAura() {

		b_respawnAura.SetActive(true);

	}


	[PunRPC]

	void PRPC_ActivateRedRespawnAura() {

		r_respawnAura.SetActive(true);

	}


	[PunRPC]

	public void PRPC_DeactivateRespawnAuras() {

		b_respawnAura.SetActive(false);

		r_respawnAura.SetActive(false);

	}


}
