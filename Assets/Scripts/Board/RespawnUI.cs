﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RespawnUI : MonoBehaviour {

	// Set the following references from the editor

	//reference to all the gameobjects for the blue team
	public GameObject b_next_respawn_sprite;
	public GameObject b_next_respawn_timer_icon;
	public GameObject b_next_respawn_text;
	public GameObject b_after_next_respawn_sprite;
	public GameObject b_after_next_respawn_timer_icon;
	public GameObject b_after_next_respawn_text;

	//reference to all the gameobjects for the red team
	public GameObject r_next_respawn_sprite;
	public GameObject r_next_respawn_timer_icon;
	public GameObject r_next_respawn_text;
	public GameObject r_after_next_respawn_sprite;
	public GameObject r_after_next_respawn_timer_icon;
	public GameObject r_after_next_respawn_text;

    //references to normal sprites
    public List<Sprite> b_graveStones = new List<Sprite>();
    public List<Sprite> r_graveStones = new List<Sprite>();

    //references to glowing sprites
    public List<Sprite> b_glowGraveStones = new List<Sprite>();
    public List<Sprite> r_glowGraveStones = new List<Sprite>();



    Hashtable blueVeritaManType_sprite = new Hashtable();
    Hashtable blueVeritaManType_glowSprite = new Hashtable();

	Hashtable redVeritaManType_sprite = new Hashtable();
    Hashtable redVeritaManType_glowSprite = new Hashtable();

    public GameObject r_glowingMotif;
    public GameObject b_glowingMotif;

    void Start() {
                
		blueVeritaManType_sprite.Add(VeritaMenType.QUEEN, b_graveStones[0]);
		blueVeritaManType_sprite.Add(VeritaMenType.ROOK, b_graveStones[1]);
		blueVeritaManType_sprite.Add(VeritaMenType.BISHOP, b_graveStones[2]);
		blueVeritaManType_sprite.Add(VeritaMenType.SUPERPAWN, b_graveStones[3]);
		blueVeritaManType_sprite.Add(VeritaMenType.BULLDOZER, b_graveStones[4]);
        
		redVeritaManType_sprite.Add(VeritaMenType.QUEEN, r_graveStones[0]);
		redVeritaManType_sprite.Add(VeritaMenType.ROOK, r_graveStones[1]);
		redVeritaManType_sprite.Add(VeritaMenType.BISHOP, r_graveStones[2]);
		redVeritaManType_sprite.Add(VeritaMenType.SUPERPAWN, r_graveStones[3]);
		redVeritaManType_sprite.Add(VeritaMenType.BULLDOZER, r_graveStones[4]);



        blueVeritaManType_glowSprite.Add(VeritaMenType.QUEEN, b_glowGraveStones[0]);
        blueVeritaManType_glowSprite.Add(VeritaMenType.ROOK, b_glowGraveStones[1]);
        blueVeritaManType_glowSprite.Add(VeritaMenType.BISHOP, b_glowGraveStones[2]);
        blueVeritaManType_glowSprite.Add(VeritaMenType.SUPERPAWN, b_glowGraveStones[3]);
        blueVeritaManType_glowSprite.Add(VeritaMenType.BULLDOZER, b_glowGraveStones[4]);

        redVeritaManType_glowSprite.Add(VeritaMenType.QUEEN, r_glowGraveStones[0]);
        redVeritaManType_glowSprite.Add(VeritaMenType.ROOK, r_glowGraveStones[1]);
        redVeritaManType_glowSprite.Add(VeritaMenType.BISHOP, r_glowGraveStones[2]);
        redVeritaManType_glowSprite.Add(VeritaMenType.SUPERPAWN, r_glowGraveStones[3]);
        redVeritaManType_glowSprite.Add(VeritaMenType.BULLDOZER, r_glowGraveStones[4]);

    }



    public void UpdateCDVisuals( Team team, VeritaMenType theNextVeritaMan, int nextCD, VeritaMenType theAfterNextVeritaMan, int afterNextCD ) {

		if(nextCD == -1) {
			
			DisableNextVisualsOf(team);

			DisableAfterNextVisualsOfTeam(team);

            if(team == Team.BLUE)
            {

                b_glowingMotif.SetActive(false);

            } else
            {

                r_glowingMotif.SetActive(false);

            }


            return;

		}

		if (team == Team.BLUE) {

			ActivateNextVisualsOf(team);

            if (nextCD == 1)
            {

                b_next_respawn_sprite.GetComponent<SpriteRenderer>().sprite = (Sprite)blueVeritaManType_glowSprite[theNextVeritaMan];

                b_glowingMotif.SetActive(true);

            } else
            {

                b_next_respawn_sprite.GetComponent<SpriteRenderer>().sprite = (Sprite)blueVeritaManType_sprite[theNextVeritaMan];

                b_glowingMotif.SetActive(false);

            }


            b_next_respawn_text.GetComponent<Text>().text = nextCD.ToString();

            if (afterNextCD == -1) {

				DisableAfterNextVisualsOfTeam(team);

				return;

			}

			ActivateAfterNextVisualsOf(team);

			b_after_next_respawn_sprite.GetComponent<SpriteRenderer>().sprite = (Sprite) blueVeritaManType_sprite[theAfterNextVeritaMan];

			b_after_next_respawn_text.GetComponent<Text>().text = afterNextCD.ToString();


		} else {

			ActivateNextVisualsOf(team);

            if (nextCD == 1)
            {
                r_next_respawn_sprite.GetComponent<SpriteRenderer>().sprite = (Sprite)redVeritaManType_glowSprite[theNextVeritaMan];

                r_glowingMotif.SetActive(true);

            } else
            {

                r_next_respawn_sprite.GetComponent<SpriteRenderer>().sprite = (Sprite)redVeritaManType_sprite[theNextVeritaMan];

                r_glowingMotif.SetActive(false);

            }

            r_next_respawn_text.GetComponent<Text>().text = nextCD.ToString();


			if (afterNextCD == -1) {

				DisableAfterNextVisualsOfTeam(team);

				return;

			}

			ActivateAfterNextVisualsOf(team);

			r_after_next_respawn_sprite.GetComponent<SpriteRenderer>().sprite = (Sprite) redVeritaManType_sprite[theAfterNextVeritaMan];

			r_after_next_respawn_text.GetComponent<Text>().text = afterNextCD.ToString();


		}
	}










	// following is code to disable/enable the various gameObjects

	void DisableNextVisualsOf (Team team) {

		if (team == Team.BLUE) {

			b_next_respawn_sprite.SetActive(false);

			b_next_respawn_text.SetActive(false);

			b_next_respawn_timer_icon.SetActive(false);

		} else {

			r_next_respawn_sprite.SetActive(false);

			r_next_respawn_text.SetActive(false);

			r_next_respawn_timer_icon.SetActive(false);

		}

	}

	void DisableAfterNextVisualsOfTeam (Team team) {

		if(team == Team.BLUE) {

			b_after_next_respawn_sprite.SetActive(false);

			b_after_next_respawn_text.SetActive(false);

			b_after_next_respawn_timer_icon.SetActive(false);

		} else {

			r_after_next_respawn_sprite.SetActive(false);

			r_after_next_respawn_text.SetActive(false);

			r_after_next_respawn_timer_icon.SetActive(false);

		}

	}


	void ActivateNextVisualsOf (Team team) {

		if (team == Team.BLUE) {

			b_next_respawn_sprite.SetActive(true);

			b_next_respawn_text.SetActive(true);

			b_next_respawn_timer_icon.SetActive(true);

		} else {

			r_next_respawn_sprite.SetActive(true);

			r_next_respawn_text.SetActive(true);

			r_next_respawn_timer_icon.SetActive(true);

		}	


	}


	void ActivateAfterNextVisualsOf (Team team) {

		if(team == Team.BLUE) {

			b_after_next_respawn_sprite.SetActive(true);

			b_after_next_respawn_text.SetActive(true);

			b_after_next_respawn_timer_icon.SetActive(true);

		} else {

			r_after_next_respawn_sprite.SetActive(true);

			r_after_next_respawn_text.SetActive(true);

			r_after_next_respawn_timer_icon.SetActive(true);

		}


	}

}
