﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class TurnManager : Photon.MonoBehaviour {

	public Board board;


	//Add the start of the, both 
	public int redTurn = 0;
	public int blueTurn = 0;

	[Tooltip("Time given to each player per turn (in seconds)")]
	public int timePerTurn ;


	public GameObject blueSlider;

	public GameObject redSlider;


	public GameObject sideHighlightBlue;

	public GameObject sideHighlightRed;

    


	// Use this for initialization
	public void StartOnBoardLoad () {

		board = GameObject.Find("Board").GetComponent<Board>();

		sideHighlightBlue.SetActive(true);

		StartCoroutine("RunBlueTimer");

	}


	public void TurnEnded ( Team team) {

		if (team == Team.BLUE) {

			blueTurn++;

		} else {

			redTurn++;

		}

        // Stop both coroutines first 
        // (dont want to use StopAllCoroutines because I may use coroutines in newer patches)
        
        photonView.RPC("PRPC_StopAndResetTimers", PhotonTargets.AllViaServer);
        		

		if ( team == Team.BLUE) {

            //passing in parameter 'false' in PRPC_RunTimer
            photonView.RPC("PRPC_RunTimer", PhotonTargets.AllViaServer, false);
            		

		} else {

            //passing in parameter 'false' in PRPC_RunTimer
            photonView.RPC("PRPC_RunTimer", PhotonTargets.AllViaServer, true);
            
		}

        // If the turn changed as a result of timeout, then the coroutines will disable the playercontrollers
        //      until they get reactivated here again.
		if (PhotonNetwork.isMasterClient) {

            photonView.RPC("PRPC_ActivatePlayerControllers", PhotonTargets.AllViaServer);

        }

	}


		

	public IEnumerator RunBlueTimer () {

		while ( blueSlider.transform.Find("Fill_Image").GetComponent<Image>().fillAmount > 0 ) {

			blueSlider.transform.Find("Fill_Image").GetComponent<Image>().fillAmount -= (Time.deltaTime/timePerTurn);

			yield return new WaitForEndOfFrame();

		}

		if (PhotonNetwork.isMasterClient) {

            photonView.RPC("PRPC_DeactivatePlayerControllers", PhotonTargets.AllViaServer);

            board.SwitchTurn( true );

		}

		yield return null;

	}


	public IEnumerator RunRedTimer () {

		while ( redSlider.transform.Find("Fill_Image").GetComponent<Image>().fillAmount > 0 ) {

			redSlider.transform.Find("Fill_Image").GetComponent<Image>().fillAmount -= Time.deltaTime/timePerTurn;

			yield return new WaitForEndOfFrame();

		}

		if (PhotonNetwork.isMasterClient) {

            photonView.RPC("PRPC_DeactivatePlayerControllers", PhotonTargets.AllViaServer);

			board.SwitchTurn(false);

		}

		yield return null;

	}



	[PunRPC] 

	public void PRPC_RunTimer (bool isTeamBlue) {

		if (isTeamBlue) {

			StartCoroutine("RunBlueTimer");

			sideHighlightBlue.SetActive(true);

			sideHighlightRed.SetActive(false);

		} else {

			StartCoroutine("RunRedTimer");

			sideHighlightBlue.SetActive(false);

			sideHighlightRed.SetActive(true);

		}

	}




	[PunRPC]

	public void PRPC_StopAndResetTimers () {

		StopCoroutine("RunBlueTimer");
		StopCoroutine("RunRedTimer");

		blueSlider.transform.Find("Fill_Image").GetComponent<Image>().fillAmount = 1;

		redSlider.transform.Find("Fill_Image").GetComponent<Image>().fillAmount = 1;

	}



    //Deactivating and reactivating PCs is important because we dont want an erroneous requests as a result of
    //  players spamming the controllers, which could lead to misbehaviour is such requests are processed. 
    //  Thus, while the turn is being switched, both the PCs are deactivated. 
    // Of course, we only need this when the turn has changed as a result of timeout. Since if the player has
    //   already played his turn, he would not be allowed to give any more inputs. 

	[PunRPC] 

	public void PRPC_DeactivatePlayerControllers () {

		foreach (PlayerController pc in GameObject.FindObjectsOfType<PlayerController>()) {

			pc.enabled = false;

		}

	}


	[PunRPC] 

	public void PRPC_ActivatePlayerControllers () {

		foreach (PlayerController pc in GameObject.FindObjectsOfType<PlayerController>()) {

			pc.enabled = true;

		}

	}


}
