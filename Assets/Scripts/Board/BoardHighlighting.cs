﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardHighlighting : MonoBehaviour {

	public GameObject highlightPanel;

    public GameObject fortifyHighlightPanel;

    public GameObject sacrificeHighlightPanel;


    private List<GameObject> highlightPanelList = new List<GameObject>();

	private List<GameObject> fortifyHighlightPanelList = new List<GameObject>();

	private List<GameObject> sacrificeHighlightPanelList = new List<GameObject>();


        
    private GameObject highlighterCanvas;

    private Board board;





    public void Start() {

		board = this.gameObject.GetComponent<Board>();

    }


	// returns a highlighter panel that is not active or creates a new one and returns it.
    private GameObject GetHighlighter()
    {
		highlighterCanvas = GameObject.Find("Highlighter");
        
        GameObject highlighter = highlightPanelList.Find(g => !g.activeSelf);

        if (!highlighter)
        {

            GameObject highlighter1 = Instantiate(highlightPanel, highlighterCanvas.transform) as GameObject;

            highlightPanelList.Add(highlighter1);

            return highlighter1;
        }

        return highlighter;

    }


	private GameObject GetFortifyHighlighter()
    {

        highlighterCanvas = GameObject.Find("FortifyHighlighter");

        GameObject highlighter = fortifyHighlightPanelList.Find(g => !g.activeSelf);

        if (!highlighter)
        {

            GameObject highlighter1 = Instantiate(fortifyHighlightPanel, highlighterCanvas.transform) as GameObject;

            fortifyHighlightPanelList.Add(highlighter1);

            return highlighter1;
        }

        return highlighter;

    }



	private GameObject GetSacrificeHighlighter()
    {

        highlighterCanvas = GameObject.Find("SacrificeHighlighter");

        GameObject highlighter = sacrificeHighlightPanelList.Find(g => !g.activeSelf);

        if (!highlighter)
        {

            GameObject highlighter1 = Instantiate(sacrificeHighlightPanel, highlighterCanvas.transform) as GameObject;

            sacrificeHighlightPanelList.Add(highlighter1);

            return highlighter1;
        }

        return highlighter;

    }




    //Actually does the highlighting of squares
    public void HighlightAllowedMoves (bool[,] movesArray)
    {
        board = this.GetComponent<Board>();

        for (int i = 0; i<7; i++)
        {

            for (int j = 0; j<7; j++)
            {

                if (movesArray[i,j])
                {

                    GameObject highlighterPane = GetHighlighter();

                    highlighterPane.transform.position = GlobalFunctions.GridToWorldCoordinates(new Vector2(i, j));

                    highlighterPane.SetActive(true);
                }

            }


        }


    }


	public void HighlightFortifiableUnits (bool[,] movesArray)
    {
        board = this.GetComponent<Board>();

        for (int i = 0; i<7; i++)
        {

            for (int j = 0; j<7; j++)
            {

                if (movesArray[i,j])
                {

                    GameObject highlighterPane = GetFortifyHighlighter();

                    highlighterPane.transform.position = GlobalFunctions.GridToWorldCoordinates(new Vector2(i, j));

                    highlighterPane.SetActive(true);
                }

            }


        }


    }


	public void HighlightSacrificeUnits (bool[,] movesArray)
    {
        board = this.GetComponent<Board>();

        for (int i = 0; i<7; i++)
        {

            for (int j = 0; j<7; j++)
            {

                if (movesArray[i,j])
                {
                    GameObject highlighterPane = GetSacrificeHighlighter();

                    highlighterPane.transform.position = GlobalFunctions.GridToWorldCoordinates(new Vector2(i, j));

                    highlighterPane.SetActive(true);
                }

            }


        }


    }







    //Hides all the highlights on the board
    public void HideHighlights ()
    {

        foreach(GameObject h in highlightPanelList)
        {

            h.SetActive(false);

        }

        foreach (GameObject h in fortifyHighlightPanelList) {

        	h.SetActive(false);

        }


        foreach (GameObject h in sacrificeHighlightPanelList) {

        	h.SetActive(false);

        }


    }




}
