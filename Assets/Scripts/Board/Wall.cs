﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

    public Sprite wallOnFire;

    public Sprite normalWall;

    public GameObject smokePS_1;
    public GameObject smokePS_2;
    public GameObject smokePS_3;

    public GameObject destroyPS;

    public bool isBurning;

    Board board;

    private void Start()
    {

        board = GameObject.FindObjectOfType<Board>();

    }

    public void UpdateWallSpriteAndAnim()
    {

        this.gameObject.GetComponent<PhotonView>().RPC("PRPC_UpdateWallSpritesAndAnim", PhotonTargets.AllViaServer, isBurning);
        

    }


    [PunRPC]

    void PRPC_UpdateWallSpritesAndAnim(bool isburning)
    {

        if (isburning)
        {

            this.gameObject.GetComponent<SpriteRenderer>().sprite = wallOnFire;

            smokePS_1.SetActive(true);
            smokePS_2.SetActive(true);
            smokePS_3.SetActive(true);

        }
        else
        {

            this.gameObject.GetComponent<SpriteRenderer>().sprite = normalWall;


            smokePS_1.SetActive(false);
            smokePS_2.SetActive(false);
            smokePS_3.SetActive(false);

        }

    } 





    private void OnDestroy()
    {

        board.GetComponent<AudioNetworking>().CallPRPCPlaySound("wallbreak_audio");


        GameObject wallBreaking = board.transform.Find("SpecialEffects").transform.Find("WallBreaking").gameObject;

        wallBreaking.transform.position = this.transform.position;

        wallBreaking.transform.rotation = this.transform.rotation;
        


        wallBreaking.SetActive(true);

        board.InvokeDeactivateSpecialEffectsInXSeconds(0.8f);

    }






}
