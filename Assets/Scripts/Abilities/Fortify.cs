﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Fortify : Abilities {

	// Need to intercept the calculation of legalMovesArray

	public BoardHighlighting boardHighlighter;

	private bool[,] theMovesArray;

	public GameObject fortifyRotatingShield;

	private bool interactingWithThis;

	public PlayerController thisPlayerController;

	private NetworkConnection currentClientConnection;


	// Remove clientAuthority in interrupt only when the ability use has been initiated
	private bool initiated = false;


	public new void StartOnBoardLoad() {

		boardHighlighter = board.gameObject.GetComponent<BoardHighlighting>();

        Debug.Log(boardHighlighter.name);

	}



	public void ExecuteAbility(int viewIDOfPlayerController) {


		currentTeam = (Team)board.photonViewID_clientTeam[viewIDOfPlayerController];

		if (!IsReadyToUse(currentTeam)) {

			return;

		}


		if (currentTeam == Team.BLUE) {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().bluePickedAbilities[AbilityType.FORTIFY];

		} else {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().redPickedAbilities[AbilityType.FORTIFY];

		}


		if (!isPicked) {

			return;

		}

        thisPlayerController = PhotonView.Find(viewIDOfPlayerController).gameObject.GetComponent<PlayerController>();

        //Dont know what these lines are for, but keeping them here for reference
        //currentClientConnection = NetworkServer.objects[(NetworkInstanceId) board.netId_netIdObject[netIdOfPlayerController]].connectionToClient;
		//this.gameObject.GetComponent<NetworkIdentity>().AssignClientAuthority(currentClientConnection);

		initiated = true;

		int i = 0;
		int j = 6;

		theMovesArray = ReturnBlankMovesArray();

		// Should only proceed with this function if there is atleast one target to fortify. Thus this bool to check this.
		bool isThereATargetToFortify = false;


		// set this flag so that the player cannot interact with the remaining portion of the board.

		do {

			
			if (board.veritaMenOnBoard[i,j]) {

				if (board.veritaMenOnBoard[i,j].team == this.currentTeam && !board.veritaMenOnBoard[i,j].GetComponent<Bulldozer>()) {

					theMovesArray[i,j] = true;

					isThereATargetToFortify = true;

				}

			}

			i++;
			j--;

		} while ( i<7 && j > -1) ;


		if (isThereATargetToFortify) {

			byte[] serializedMovesArray = GlobalFunctions.SerializeToByteArray<bool[,]>(theMovesArray);

            this.gameObject.GetComponent<PhotonView>().RPC("PRPC_StepOneFortify", PhotonTargets.AllViaServer, serializedMovesArray, viewIDOfPlayerController);

		}
	}



	public bool[,] ReturnBlankMovesArray()
    {

        bool[,] theMovesArray = new bool[7, 7];

        for (int i = 0; i<7; i++)
        {

            for (int j =0; j<7; j++)
            {

                theMovesArray[i, j] = false;  

            }

        }

        return theMovesArray;

    }



	void OnMouseDown ()
    {

    	if (!interactingWithThis) {

    		return;

    	}

        AudioManager.instance.PlaySound("tap_audio");

        // what we need is that when mouse is clicked, the output is the numerical value of the square

        // The height and width is 415 each

        Vector2 clickedPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

		Vector2 mousePos = GlobalFunctions.WorldToGridCoordinates(clickedPos);

        int pos_x = (int) mousePos.x;
        int pos_y = (int) mousePos.y;

        this.gameObject.GetComponent<PhotonView>().RPC("PRPC_OnPlayerInput", PhotonTargets.MasterClient, pos_x, pos_y);

    }



    [PunRPC] 
    private void PRPC_StepOneFortify (byte[] serializedMovesArray, int viewIDOfPlayerController ) {

        PhotonView pv = PhotonView.Find(viewIDOfPlayerController);

        if (!pv.isMine)
        {
            return;
        }

        theMovesArray = GlobalFunctions.DeserializeToObject<bool[,]>(serializedMovesArray);

        thisPlayerController = pv.gameObject.GetComponent<PlayerController>();
        
		boardHighlighter = thisPlayerController.board.GetComponent<BoardHighlighting>();

		thisPlayerController.interactingWithAbilityUI = true;

		interactingWithThis = true;

		boardHighlighter.HighlightFortifiableUnits(theMovesArray);

		this.gameObject.GetComponent<BoxCollider2D>().enabled = true;

    }



    [PunRPC] 
    private void PRPC_OnPlayerInput( int pos_x, int pos_y) {

		
		if (theMovesArray[pos_x, pos_y] ) {


			// If a fortification already exists, destroy the earlier one before spawning the new one. 

			foreach (GameObject shield in GameObject.FindGameObjectsWithTag("fortify_shield")) {

	    		if (shield.GetComponent<FortifyShield>().shieldOfTeam == currentTeam) {

	    			PhotonNetwork.Destroy(shield);

	    		}

	        }


			Vector2 mousePos1 = new Vector2(pos_x, pos_y);

        	board.fortifiedSqaures[this.currentTeam] = new Vector2 (pos_x, pos_y);

			GameObject thisfortifyShield = (GameObject) PhotonNetwork.Instantiate("Fortify-Shield", GlobalFunctions.GridToWorldCoordinates(mousePos1), Quaternion.identity, 0);

            thisfortifyShield.transform.SetParent(this.transform);

			thisfortifyShield.GetComponent<FortifyShield>().shieldOfTeam = this.currentTeam;

            this.gameObject.GetComponent<PhotonView>().RPC("PRPC_StepTwoFortify", PhotonTargets.All);

            board.GetComponent<AudioNetworking>().CallPRPCPlaySound("fortify_audio");

            board.SwitchTurn(false);

       		tracker.SetCooldownOfAbilityForTeam(thisAbilityType, currentTeam);

       		
       		initiated = false;

        }


    }


    [PunRPC] 

    private void PRPC_StepTwoFortify() {

		this.gameObject.GetComponent<BoxCollider2D>().enabled = false;

        thisPlayerController.interactingWithAbilityUI = false;

        boardHighlighter.HideHighlights();

        interactingWithThis = false;

    }


    public override void InterruptReset( ) {

    	// if playerController is null, it means that Execute ability has never run on this client. Thus it impliex that
    	//   interacting with abilityUI will be false. (similarly with board highlighter)

    	if (thisPlayerController != null) {
    		    	
    		thisPlayerController.interactingWithAbilityUI = false;

    	}

    	this.gameObject.GetComponent<BoxCollider2D>().enabled = false;

    	if (boardHighlighter != null) {

    		boardHighlighter.HideHighlights();

    	}

		/*if (this.gameObject.GetComponent<NetworkIdentity>().clientAuthorityOwner != null) {

			this.gameObject.GetComponent<NetworkIdentity>().RemoveClientAuthority(this.gameObject.GetComponent<NetworkIdentity>().clientAuthorityOwner);

    	}*/

    	interactingWithThis = false;

    }


}
