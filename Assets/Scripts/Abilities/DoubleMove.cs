﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleMove : Abilities {

    public Sprite indicatorSpriteGrey;

    public Sprite indicatorSpriteGreen;

    bool superPawnFound = false;

	public void ExecuteAbility(int viewIDOfPlayerController) {

		currentTeam = (Team)board.photonViewID_clientTeam[viewIDOfPlayerController];

		if (!IsReadyToUse(currentTeam)) {

			return;

		}

        superPawnFound = false;

        bool isBlue;

		if (currentTeam == Team.BLUE) {

            isBlue = true;

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().bluePickedAbilities[AbilityType.DOUBLEMOVE];

            foreach(VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
            {

                if (v.team == Team.BLUE && v.veritamenTypeOfThis == VeritaMenType.SUPERPAWN)
                {

                    superPawnFound = true;

                }


            }

		} else {

            isBlue = false;

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().redPickedAbilities[AbilityType.DOUBLEMOVE];

            foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
            {

                if (v.team == Team.RED && v.veritamenTypeOfThis == VeritaMenType.SUPERPAWN)
                {

                    superPawnFound = true;

                }


            }

        }



		if (!isPicked || !superPawnFound) {

			return;

		}

        board.GetComponent<AudioNetworking>().CallPRPCPlaySound("doublemove_audio");

        board.isDoubleMove = true;

		tracker.SetCooldownOfAbilityForTeam(thisAbilityType, currentTeam);

        CallUpdateDMIndicator(isBlue, 0);


        //remove glow from all VeritaMen except the superpawns
        foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
        {

            if (currentTeam == v.team)
            {

                if(v.tempState.Contains(VeritaManState.TURNGLOW) && !(v.veritamenTypeOfThis == VeritaMenType.SUPERPAWN))
                {

                    v.tempState.Remove(VeritaManState.TURNGLOW);

                    v.gameObject.GetComponent<SpriteManager>().CallPRPCUpdateSprites();

                }

            }

        }
               

	}



	public override void InterruptReset() {

		board.isDoubleMove = false;

	}



    public void CallUpdateDMIndicator(bool isTeamBlue, int movesOver)
    {

        this.gameObject.GetComponent<PhotonView>().RPC("PRPC_UpdateDoubleMoveIndicator", PhotonTargets.AllViaServer, isTeamBlue, movesOver);
        
    }



    [PunRPC]

    void PRPC_UpdateDoubleMoveIndicator(bool isTeamBlue, int movesOver)
    {
        if (movesOver == 0)
        {

            if (isTeamBlue)
            {

                foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
                {

                    if (v.veritamenTypeOfThis == VeritaMenType.SUPERPAWN && v.team == Team.BLUE)
                    {

                        GameObject indicator = v.transform.Find("DoubleMoveIndicator").gameObject;

                        indicator.transform.Find("move1").GetComponent<SpriteRenderer>().sprite = indicatorSpriteGrey;

                        indicator.transform.Find("move2").GetComponent<SpriteRenderer>().sprite = indicatorSpriteGrey;

                        indicator.SetActive(true);
                        

                    }


                }


            }
            else
            {

                foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
                {

                    if (v.veritamenTypeOfThis == VeritaMenType.SUPERPAWN && v.team == Team.RED)
                    {

                        GameObject indicator = v.transform.Find("DoubleMoveIndicator").gameObject;

                        indicator.transform.Find("move1").GetComponent<SpriteRenderer>().sprite = indicatorSpriteGrey;

                        indicator.transform.Find("move2").GetComponent<SpriteRenderer>().sprite = indicatorSpriteGrey;

                        indicator.SetActive(true);



                    }


                }


            }


        } else if (movesOver == 1)
        {

            if (isTeamBlue)
            {

                foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
                {

                    if (v.veritamenTypeOfThis == VeritaMenType.SUPERPAWN && v.team == Team.BLUE)
                    {

                        GameObject indicator = v.transform.Find("DoubleMoveIndicator").gameObject;

                        indicator.transform.Find("move1").GetComponent<SpriteRenderer>().sprite = indicatorSpriteGreen;

                        indicator.SetActive(true);
                        

                    }


                }


            }
            else
            {

                foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
                {

                    if (v.veritamenTypeOfThis == VeritaMenType.SUPERPAWN && v.team == Team.RED)
                    {

                        GameObject indicator = v.transform.Find("DoubleMoveIndicator").gameObject;

                        indicator.transform.Find("move1").GetComponent<SpriteRenderer>().sprite = indicatorSpriteGreen;
                        

                    }


                }


            }



        } else if (movesOver == 2)
        {
                                              
            if (isTeamBlue)
            {

                foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
                {

                    if (v.veritamenTypeOfThis == VeritaMenType.SUPERPAWN && v.team == Team.BLUE)
                    {

                        v.transform.Find("DoubleMoveIndicator").gameObject.SetActive(false);

                    }


                }


            }
            else
            {

                foreach (VeritaMen v in GameObject.FindObjectsOfType<VeritaMen>())
                {

                    if (v.veritamenTypeOfThis == VeritaMenType.SUPERPAWN && v.team == Team.RED)
                    {

                        v.transform.Find("DoubleMoveIndicator").gameObject.SetActive(false);
                        
                    }


                }


            }


        }

    }

}
