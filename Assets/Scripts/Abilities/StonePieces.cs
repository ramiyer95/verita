﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class StonePieces : Abilities {

	public int backToNormalAtTurn = -1;


	public void ExecuteAbility(int viewIDOfPlayerController) {

		//Stone pieces is the only ability where additional conditions need not be checked since it can be activated anytime.


		currentTeam = (Team)board.photonViewID_clientTeam[viewIDOfPlayerController];



		if (!IsReadyToUse(currentTeam)) {

			return;

		}

		bool isThisTeamBlue;

		if (currentTeam == Team.BLUE) {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().bluePickedAbilities[AbilityType.STONEPIECES];
			isThisTeamBlue = true;

		} else {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().redPickedAbilities[AbilityType.STONEPIECES];
			isThisTeamBlue = false;

		}

		if(!isPicked) {

			return;

		}

        UpdateSprites(isThisTeamBlue);
		
		// To block the movement of enemy pieces
		board.stonePiecesOfTeam[this.currentTeam] = true;



		// setting the turn on which we want to get back to normal
		if (this.currentTeam == Team.BLUE) {

			tracker.blueBackToNormal_SP = turnManager.blueTurn + 1;

		} else {

			tracker.redBackToNormal_SP = turnManager.redTurn + 1;

		}

		tracker.SetCooldownOfAbilityForTeam(thisAbilityType, currentTeam);

        board.GetComponent<AudioNetworking>().CallPRPCPlaySound("stonepieces_audio");

        //using StonePieces does not end the turn. 

    }



	public void RevertAppearanceToNormal( bool isTeamBlue ) {

        // Dont need to call the update sprites function because after this method is called, the sprites are 
        //  updated (as a result of turn end) in the Board Class.

		if (isTeamBlue) {

			foreach(VeritaMen theVeritaMan in GameObject.FindObjectsOfType<VeritaMen>()) {

				if (theVeritaMan.team == Team.BLUE) {

					if (theVeritaMan.tempState.Contains(VeritaManState.STONE))
                    {

                        theVeritaMan.tempState.Remove(VeritaManState.STONE);

                    }
                    
				}

			}

		} else {


			foreach(VeritaMen theVeritaMan in GameObject.FindObjectsOfType<VeritaMen>()) {

				if (theVeritaMan.team == Team.RED) {

                    if (theVeritaMan.tempState.Contains(VeritaManState.STONE))
                    {

                        theVeritaMan.tempState.Remove(VeritaManState.STONE);

                    }

                }

			}



		}


	}





	private void UpdateSprites( bool isTeamBlue) {

		if (isTeamBlue) {

			foreach(VeritaMen theVeritaMan in GameObject.FindObjectsOfType<VeritaMen>()) {

				if(theVeritaMan.team == Team.BLUE) {

                    theVeritaMan.tempState.Add(VeritaManState.STONE);

                    theVeritaMan.GetComponent<SpriteManager>().CallPRPCUpdateSprites();

                }

			}

		} else {

			foreach(VeritaMen theVeritaMan in GameObject.FindObjectsOfType<VeritaMen>()) {

				if(theVeritaMan.team == Team.RED) {

                    theVeritaMan.tempState.Add(VeritaManState.STONE);

                    theVeritaMan.GetComponent<SpriteManager>().CallPRPCUpdateSprites();

                }

			}

		}
				

	}




}
