﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AbilitiesUI : MonoBehaviour {

	public PlayerController thisPlayerController;

	public Button rookBishopSwapButton;
	public Button canonDefenderButton;
	public Button fortifyButton;
	public Button doubleMoveButton;
	public Button stonePiecesButton;
	public Button sacrificeButton;

	public Button blueHiddenAbility;
	public Button redHiddenAbility;

	public GameObject sacrificeUI;

	public GameObject specialAbilitiesManager;

	public GameObject cd_overlay;

	//Sort of 'DATABASE' Hashtables to be populated at the start
	private Hashtable ability_button = new Hashtable();

	private Hashtable ability_normalSprite = new Hashtable();

	private Hashtable ability_cdSprite = new Hashtable();

	// need the cooldown sprites
	public Sprite rookBishopSwapCDIcon;
	public Sprite canonDefenderCDIcon;
	public Sprite fortifyCDIcon;
	public Sprite doubleMoveCDIcon;
	public Sprite sacrificeCDIcon;
	public Sprite stonePiecesCDIcon;

	public Sprite rookBishopSwapIcon;
	public Sprite canonDefenderIcon;
	public Sprite fortifyIcon;
	public Sprite doubleMoveIcon;
	public Sprite sacrificeIcon;
	public Sprite stonePiecesIcon;


	//The buttons which are actually spawned
	Button theAbilityButton1;
	Button theAbilityButton2;
	Button theHiddenAbilityButton;

	//Hashtables storing metadata about the buttons actually spawned
	// (required for programming effects onto the buttons)
	Hashtable ability_spawnedButton = new Hashtable();

	Hashtable ability_cdOverlay = new Hashtable();

    IEnumerator coroutine;



    void Start() {

		thisPlayerController = this.transform.parent.gameObject.GetComponent<PlayerController>();

		specialAbilitiesManager = GameObject.Find("SpecialAbilitiesManager");


		ability_button.Add(AbilityType.ROOKBISHOPSWAP, rookBishopSwapButton);
		ability_button.Add(AbilityType.CANONDEFENDER, canonDefenderButton);
		ability_button.Add(AbilityType.FORTIFY, fortifyButton);
		ability_button.Add(AbilityType.DOUBLEMOVE, doubleMoveButton);
		ability_button.Add(AbilityType.SACRIFICE, sacrificeButton);
		ability_button.Add(AbilityType.STONEPIECES, stonePiecesButton);


		ability_cdSprite.Add(AbilityType.ROOKBISHOPSWAP, rookBishopSwapCDIcon);
		ability_cdSprite.Add(AbilityType.CANONDEFENDER, canonDefenderCDIcon);
		ability_cdSprite.Add(AbilityType.FORTIFY, fortifyCDIcon);
		ability_cdSprite.Add(AbilityType.DOUBLEMOVE, doubleMoveCDIcon);
		ability_cdSprite.Add(AbilityType.SACRIFICE, sacrificeCDIcon);
		ability_cdSprite.Add(AbilityType.STONEPIECES, stonePiecesCDIcon);

		ability_normalSprite.Add(AbilityType.ROOKBISHOPSWAP, rookBishopSwapIcon);
		ability_normalSprite.Add(AbilityType.CANONDEFENDER, canonDefenderIcon);
		ability_normalSprite.Add(AbilityType.FORTIFY, fortifyIcon);
		ability_normalSprite.Add(AbilityType.DOUBLEMOVE, doubleMoveIcon);
		ability_normalSprite.Add(AbilityType.SACRIFICE, sacrificeIcon);
		ability_normalSprite.Add(AbilityType.STONEPIECES, stonePiecesIcon);

	} 



	// This function needs to be called when the board is loaded. It should be called AFTER the player PICKS the abilities
	//  he wants. (There was also an idea to preload the abilities from user preference memory.)


	public void AddListenersToButtons( Button theButton, AbilityType theAbility ) {


		if (theAbility == AbilityType.ROOKBISHOPSWAP) {

			theButton.onClick.AddListener( delegate {
				
			 thisPlayerController.photonView.RPC("PRPC_ExecuteRookBishopSwap", PhotonTargets.MasterClient);


            } );

		} else if (theAbility ==  AbilityType.CANONDEFENDER) {

			theButton.onClick.AddListener ( delegate {

				thisPlayerController.photonView.RPC("PRPC_ExecuteCanonDefender", PhotonTargets.MasterClient);


            } );

		} else if (theAbility == AbilityType.FORTIFY) {

			theButton.onClick.AddListener ( delegate {

                thisPlayerController.photonView.RPC("PRPC_ExecuteFortify", PhotonTargets.MasterClient);

			} ) ;

		} else if (theAbility == AbilityType.DOUBLEMOVE) {

			theButton.onClick.AddListener ( delegate {

				thisPlayerController.photonView.RPC("PRPC_ExecuteDoubleMove", PhotonTargets.MasterClient);


            } ) ;

		} else if (theAbility == AbilityType.SACRIFICE) {

			theButton.onClick.AddListener ( delegate {

				thisPlayerController.photonView.RPC("PRPC_ExecuteSacrifice", PhotonTargets.MasterClient);


            } );

		} else if (theAbility == AbilityType.STONEPIECES) {

			theButton.onClick.AddListener ( delegate {

				thisPlayerController.photonView.RPC("PRPC_ExecuteStonePieces", PhotonTargets.MasterClient);


            } );

		}


	}



	public void SpawnButtons() {

		// can directly use the ability prefs class to do this. Dont need to route through the tracker. 
		// Tracker is also using data directly from the Ability Prefs


		AbilityPrefs theAbilityPref = this.gameObject.GetComponent<AbilityPrefs>();

		Button hiddenButton = (Button) ability_button[theAbilityPref.hiddenPicked];
		Button normalButton1 = (Button) ability_button[theAbilityPref.normalPicked[0]];
		Button normalButton2 = (Button) ability_button[theAbilityPref.normalPicked[1]];

		Vector3 hiddenButtonPos ;
		Vector3 normalButton1Pos ;
		Vector3 normalButton2Pos ;

        if (thisPlayerController.clientTeam == Team.BLUE) {
                        
			hiddenButtonPos = new Vector3 (385, -144, 0);
			normalButton1Pos = new Vector3 (385, 144, 0);
			normalButton2Pos = new Vector3 (385, 0 , 0);

		} else {

			hiddenButtonPos = new Vector3 (-385, -144, 0);
			normalButton1Pos = new Vector3 (-385, 144, 0);
			normalButton2Pos = new Vector3 (-385, 0 , 0);
            
		}

        	

		if(thisPlayerController.clientTeam == Team.BLUE) {

			this.gameObject.transform.position = new Vector3 (0, 75, 0);


		} else {

			this.gameObject.transform.position = new Vector3 (0, 75, 0);

		}


		theHiddenAbilityButton = Instantiate(hiddenButton, this.transform) as Button;

		theAbilityButton1 = Instantiate(normalButton1, this.transform) as Button;

		theAbilityButton2 = Instantiate(normalButton2, this.transform) as Button;



		theHiddenAbilityButton.transform.localPosition = hiddenButtonPos;
		theAbilityButton1.transform.localPosition = normalButton1Pos;
		theAbilityButton2.transform.localPosition = normalButton2Pos;

		AddListenersToButtons(theHiddenAbilityButton, theAbilityPref.hiddenPicked);
		AddListenersToButtons(theAbilityButton1, theAbilityPref.normalPicked[0]);
		AddListenersToButtons(theAbilityButton2, theAbilityPref.normalPicked[1]);

        
		ability_spawnedButton.Add(theAbilityPref.hiddenPicked, theHiddenAbilityButton);
        ability_spawnedButton.Add(theAbilityPref.normalPicked[0], theAbilityButton1);
        ability_spawnedButton.Add(theAbilityPref.normalPicked[1], theAbilityButton2);


		if (this.thisPlayerController.clientTeam == Team.BLUE) {

			//basically I am switching the position of the infopanel if the abilityButtons belong to the blue team

			//to do this, I am switching the 'left' and 'right' values of the rect transform.

			//understanding this is a little tough, so refer to the documentation and read up on how the rect transform works

			RectTransform theRectTransform;

			theRectTransform = theHiddenAbilityButton.transform.Find("abilityInfoPanel").transform as RectTransform;

			Vector2 offsetMin = theRectTransform.offsetMin;

			Vector2 offsetMax = theRectTransform.offsetMax;

			float right = offsetMax.x;

			offsetMax.x = -offsetMin.x;

			offsetMin.x = -right;

			theRectTransform.offsetMax = offsetMax;

			theRectTransform.offsetMin = offsetMin;




			theRectTransform = theAbilityButton1.transform.Find("abilityInfoPanel").transform as RectTransform;

			offsetMin = theRectTransform.offsetMin;

			offsetMax = theRectTransform.offsetMax;

			right = offsetMax.x;

			offsetMax.x = -offsetMin.x;

			offsetMin.x = -right;

			theRectTransform.offsetMax = offsetMax;

			theRectTransform.offsetMin = offsetMin;



			theRectTransform = theAbilityButton2.transform.Find("abilityInfoPanel").transform as RectTransform;

			offsetMin = theRectTransform.offsetMin;

			offsetMax = theRectTransform.offsetMax;

			right = offsetMax.x;

			offsetMax.x = -offsetMin.x;

			offsetMin.x = -right;

			theRectTransform.offsetMax = offsetMax;

			theRectTransform.offsetMin = offsetMin;


		}




		//after the buttons are spawned, we can spawn the cd_overlay and set them to inactive. 
		// Then, add them to the hashtable to keep a track of abilityButton and its overlay


		GameObject theOverlay = Instantiate(cd_overlay, this.transform) as GameObject;

		theOverlay.transform.localPosition = hiddenButtonPos;

		ability_cdOverlay.Add(theAbilityPref.hiddenPicked, theOverlay);

		theOverlay.SetActive(false);



		theOverlay = Instantiate(cd_overlay, this.transform) as GameObject;

		theOverlay.transform.localPosition = normalButton1Pos;

		ability_cdOverlay.Add(theAbilityPref.normalPicked[0], theOverlay);

		theOverlay.SetActive(false);



		theOverlay = Instantiate(cd_overlay, this.transform) as GameObject;

		theOverlay.transform.localPosition = normalButton2Pos;

		ability_cdOverlay.Add(theAbilityPref.normalPicked[1], theOverlay);

		theOverlay.SetActive(false);

	}



	//need public method to spawn enemy team's ability icons

	public void SpawnEnemyButtons (int normalAbilityOneID, int normalAbilityTwoID) {

		Vector3 hiddenButtonPos ;
		Vector3 normalButton1Pos ;
		Vector3 normalButton2Pos ;

		Button normalButton1 = (Button)ability_button[GlobalFunctions.ResolveIDToAbility(normalAbilityOneID)];
		Button normalButton2 = (Button)ability_button[GlobalFunctions.ResolveIDToAbility(normalAbilityTwoID)];
		Button hiddenButton;

		if (thisPlayerController.clientTeam == Team.RED) {

			hiddenButton = redHiddenAbility;

			hiddenButtonPos = new Vector3 (-385, -144, 0);
			normalButton1Pos = new Vector3 (-385, 144, 0);
			normalButton2Pos = new Vector3 (-385, 0 , 0);

		} else {

			hiddenButton = blueHiddenAbility;

			hiddenButtonPos = new Vector3 (385, -144, 0);
			normalButton1Pos = new Vector3 (385, 144, 0);
			normalButton2Pos = new Vector3 (385, 0 , 0);

		}


		theHiddenAbilityButton = Instantiate(hiddenButton, this.transform) as Button;

		theAbilityButton1 = Instantiate(normalButton1, this.transform) as Button;

		theAbilityButton2 = Instantiate(normalButton2, this.transform) as Button;


		theHiddenAbilityButton.transform.localPosition = hiddenButtonPos;
		theAbilityButton1.transform.localPosition = normalButton1Pos;
		theAbilityButton2.transform.localPosition = normalButton2Pos;


		theAbilityButton1.GetComponent<Image>().sprite = (Sprite)ability_cdSprite[GlobalFunctions.ResolveIDToAbility(normalAbilityOneID)];

		theAbilityButton2.GetComponent<Image>().sprite = (Sprite)ability_cdSprite[GlobalFunctions.ResolveIDToAbility(normalAbilityTwoID)];

        
        ability_spawnedButton.Add("hidden", theHiddenAbilityButton);
        ability_spawnedButton.Add(GlobalFunctions.ResolveIDToAbility(normalAbilityOneID), theAbilityButton1);
        ability_spawnedButton.Add(GlobalFunctions.ResolveIDToAbility(normalAbilityTwoID), theAbilityButton2);
        



		if (thisPlayerController.clientTeam == Team.BLUE) {

			//basically I am switching the position of the infopanel if the abilityButtons belong to the blue team

			//to do this, I am switching the 'left' and 'right' values of the rect transform.

			//understanding this is a little tough, so refer to the documentation and read up on how the rect transform works

			RectTransform theRectTransform;


			theRectTransform = theAbilityButton1.transform.Find("abilityInfoPanel").transform as RectTransform;

			Vector2 offsetMin = theRectTransform.offsetMin;

			Vector2 offsetMax = theRectTransform.offsetMax;

			float right = offsetMax.x;

			offsetMax.x = -offsetMin.x;

			offsetMin.x = -right;

			theRectTransform.offsetMax = offsetMax;

			theRectTransform.offsetMin = offsetMin;



			theRectTransform = theAbilityButton2.transform.Find("abilityInfoPanel").transform as RectTransform;

			offsetMin = theRectTransform.offsetMin;

			offsetMax = theRectTransform.offsetMax;

			right = offsetMax.x;

			offsetMax.x = -offsetMin.x;

			offsetMin.x = -right;

			theRectTransform.offsetMax = offsetMax;

			theRectTransform.offsetMin = offsetMin;


		}





		GameObject theOverlay = Instantiate(cd_overlay, this.transform) as GameObject;

		theOverlay.transform.localPosition = normalButton1Pos;

		ability_cdOverlay.Add(GlobalFunctions.ResolveIDToAbility(normalAbilityOneID), theOverlay);

		theOverlay.SetActive(false);



		theOverlay = Instantiate(cd_overlay, this.transform) as GameObject;

		theOverlay.transform.localPosition = normalButton2Pos;

		ability_cdOverlay.Add(GlobalFunctions.ResolveIDToAbility(normalAbilityTwoID), theOverlay);

		theOverlay.SetActive(false);

	}


	public void SpawnHiddenEnemyButton (int hiddenAbilityID) {

		Vector3 hiddenButtonPos ;

		Button hiddenButton = (Button)ability_button[GlobalFunctions.ResolveIDToAbility(hiddenAbilityID)];

		if (thisPlayerController.clientTeam == Team.RED) {

			hiddenButtonPos = new Vector3 (-385, -144, 0);

		} else {

			hiddenButtonPos = new Vector3 (385, -144, 0);

		}


		theHiddenAbilityButton = Instantiate(hiddenButton, this.transform) as Button;

		theHiddenAbilityButton.transform.localPosition = hiddenButtonPos;

		theHiddenAbilityButton.GetComponent<Image>().sprite = (Sprite)ability_cdSprite[GlobalFunctions.ResolveIDToAbility(hiddenAbilityID)];

        
		ability_spawnedButton.Add(GlobalFunctions.ResolveIDToAbility(hiddenAbilityID), theHiddenAbilityButton);

		ability_spawnedButton.Remove("hidden");


		GameObject theOverlay = Instantiate(cd_overlay, this.transform) as GameObject;

		theOverlay.transform.localPosition = hiddenButtonPos;

		ability_cdOverlay.Add(GlobalFunctions.ResolveIDToAbility(hiddenAbilityID), theOverlay);

		theOverlay.SetActive(false);


	}



	// Need a method to set cooldown visual
	// cooldowns are tracked in the tracker



	public void UpdateAbilityCooldownsVisual( AbilityType theAbilityType, int cooldown) {

		if (!thisPlayerController.photonView.isMine) {

			// reaching here means that this playerController is of the enemy team for the current client

			if (!ability_spawnedButton.ContainsKey(theAbilityType) ) {

				// if the abilityType passed as a parameter is not contained in the spawned hash function, it means that 
				// 	the ability is hidden. Hence, we donot want to do anything to it as of yet. 

				return;

			}

		}

		GameObject theOverlay = (GameObject) ability_cdOverlay[theAbilityType];

        
		int totalCoolDown = GlobalFunctions.GetCooldownOfAbility(theAbilityType);

		if (cooldown != -1) {

			theOverlay.SetActive(true);

			theOverlay.GetComponentInChildren<Text>().text = cooldown.ToString();

			theOverlay.GetComponentInChildren<Image>().fillAmount = (float) cooldown/totalCoolDown;

		} else {

			theOverlay.SetActive(false);
                     
            
		}

	}



    public void AbilityReadyEffect(Team team, List<int> cdArray)
    {

        Debug.Log("called");

            foreach (AbilityButton ab in this.gameObject.GetComponentsInChildren<AbilityButton>())
            {

                if (cdArray[GlobalFunctions.ResolveAbilityToID(ab.myAbilityType) - 1] == -1)
                {

                    ab.gameObject.transform.Find("cd_over_effect").gameObject.SetActive(true);

                }

                coroutine = DeactivateCDEffect();

                StartCoroutine(coroutine);

            }
                   

    }



    IEnumerator DeactivateCDEffect()
    {
        
        yield return new WaitForSecondsRealtime(2.0f);

        foreach(AbilityButton ab in this.gameObject.GetComponentsInChildren<AbilityButton>())
        {

            ab.gameObject.transform.Find("cd_over_effect").gameObject.SetActive(false);

        }

        

    }



}
