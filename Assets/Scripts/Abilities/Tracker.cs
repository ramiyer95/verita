﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Tracker : Photon.MonoBehaviour {

	Board board;


	// The structure of both these hashtables would be such that the key is the enum AbilityType and the value is true or false

	public Hashtable bluePickedAbilities =  new Hashtable();

	public Hashtable redPickedAbilities = new Hashtable();



	//Stone Pieces (turn at which to revert to normal)
 	public int blueBackToNormal_SP;
	public int redBackToNormal_SP;


	// Since SpecialAbilitiesManager comes preloaded in the scene, it is ok to use the Start() function
	public List<int> blueAbilitiesCooldown;
	public List<int> redAbilitiesCooldown;


	// Following variables are here to keep a track of the hidden abilities;
	AbilityType blueHiddenAbility;
	AbilityType redHiddenAbility;

	bool blueHiddenAbilityUsed = false;
	bool redHiddenAbilityUsed = false;

	private TurnManager turnManager;


	void Start() {

		// Initializing the Hashtables of ability picked or not

		bluePickedAbilities.Add(AbilityType.ROOKBISHOPSWAP, false);
		bluePickedAbilities.Add(AbilityType.CANONDEFENDER, false);
		bluePickedAbilities.Add(AbilityType.FORTIFY, false);
		bluePickedAbilities.Add(AbilityType.DOUBLEMOVE, false);
		bluePickedAbilities.Add(AbilityType.SACRIFICE, false);
		bluePickedAbilities.Add(AbilityType.STONEPIECES, false);

		redPickedAbilities.Add(AbilityType.ROOKBISHOPSWAP, false);
		redPickedAbilities.Add(AbilityType.CANONDEFENDER, false);
		redPickedAbilities.Add(AbilityType.FORTIFY, false);
		redPickedAbilities.Add(AbilityType.DOUBLEMOVE, false);
		redPickedAbilities.Add(AbilityType.SACRIFICE, false);
		redPickedAbilities.Add(AbilityType.STONEPIECES, false);


		for (int i = 0; i<6; i++) {

			blueAbilitiesCooldown.Add(-1);
			redAbilitiesCooldown.Add(-1);

		}

		turnManager = GameObject.FindObjectOfType<TurnManager>();

	}


	public void StartOnBoardLoad() {

		board = GameObject.FindObjectOfType<Board>();

	}


	public void ResolveAbilities(Team team, int hiddenAbilityID, int normalAbilityOneID, int normalAbilityTwoID) {

		
		if (team == Team.BLUE) {

			bluePickedAbilities[GlobalFunctions.ResolveIDToAbility(hiddenAbilityID)] = true;

			blueHiddenAbility = (AbilityType) GlobalFunctions.ResolveIDToAbility(hiddenAbilityID);

			bluePickedAbilities[GlobalFunctions.ResolveIDToAbility(normalAbilityOneID)] = true;

			bluePickedAbilities[GlobalFunctions.ResolveIDToAbility(normalAbilityTwoID)] = true;

		} else {

			redPickedAbilities[GlobalFunctions.ResolveIDToAbility(hiddenAbilityID)] = true;

			redHiddenAbility = (AbilityType) GlobalFunctions.ResolveIDToAbility(hiddenAbilityID);

			redPickedAbilities[GlobalFunctions.ResolveIDToAbility(normalAbilityOneID)] = true;

			redPickedAbilities[GlobalFunctions.ResolveIDToAbility(normalAbilityTwoID)] = true;

		}


	}



	public void SetCooldownOfAbilityForTeam (AbilityType abilityType, Team team) {

		int indexInList = GlobalFunctions.ResolveAbilityToID(abilityType);


		if (team == Team.BLUE) {

			int currentTurn = turnManager.blueTurn;

			blueAbilitiesCooldown[indexInList - 1] = currentTurn + GlobalFunctions.GetCooldownOfAbility(abilityType);
            
			if ((blueHiddenAbility == abilityType) && !blueHiddenAbilityUsed) {

				photonView.RPC("PRPC_SpawnHiddenAbilityButtonOfTeam", PhotonTargets.All, true, GlobalFunctions.ResolveAbilityToID(abilityType));

                blueHiddenAbilityUsed = true;

			}

		} else {

			int currentTurn = turnManager.redTurn;

			redAbilitiesCooldown[indexInList - 1] = currentTurn + GlobalFunctions.GetCooldownOfAbility(abilityType);


			if ((redHiddenAbility == abilityType) && !redHiddenAbilityUsed) {

				photonView.RPC("PRPC_SpawnHiddenAbilityButtonOfTeam", PhotonTargets.All, false, GlobalFunctions.ResolveAbilityToID(abilityType));

                redHiddenAbilityUsed = true;

			}

		}


		// need to call the update abilityUI visuals function here that will be run on the AbilityUI component

		UpdateAbilityVisualsOfTeam(team);

	}



	[PunRPC] 

	void PRPC_SpawnHiddenAbilityButtonOfTeam(bool isTeamBlue, int hiddenAbilityID) {

		Team theTeam;

		if(isTeamBlue) {

			theTeam = Team.BLUE;

		} else {

			theTeam = Team.RED;

		}

        PhotonView pv;

        foreach (PlayerController pc in GameObject.FindObjectsOfType<PlayerController>())
        {

            if (pc.clientTeam == theTeam)
            {                              

                pv = pc.gameObject.GetComponent<PhotonView>();

                if (!pv.isMine)
                {

                    pv.gameObject.GetComponentInChildren<AbilitiesUI>().SpawnHiddenEnemyButton(hiddenAbilityID);

                }

            }

        }
        

    }



	public void UpdateAbilityVisualsOfTeam( Team team ) {

		PhotonView pv = (PhotonView) board.clientTeam_photonVeiw[team];

        //using photontargets.all when calling RPCs because the player should see an updated ability icon as
        //  soon as they press the ability button

		if (team == Team.BLUE) {

			for(int i=1; i<7; i++) {

				if ((bool)bluePickedAbilities[GlobalFunctions.ResolveIDToAbility(i)]) {

					if (blueAbilitiesCooldown[i - 1] != -1) {

						int cd = blueAbilitiesCooldown[i - 1] - turnManager.blueTurn;

						pv.RPC("PRPC_UpdateAbilityCooldownVisuals", PhotonTargets.All, i, cd);


                    } else {

                        pv.RPC("PRPC_UpdateAbilityCooldownVisuals", PhotonTargets.All, i, -1);

					}
				}

			}

		} else {

			for(int i=1; i<7; i++) {

				if ((bool)redPickedAbilities[GlobalFunctions.ResolveIDToAbility(i)]) {

					if (redAbilitiesCooldown[i - 1] != -1) {

						int cd = redAbilitiesCooldown[i - 1] - turnManager.redTurn;

                        pv.RPC("PRPC_UpdateAbilityCooldownVisuals", PhotonTargets.All, i, cd);

					} else {

                        pv.RPC("PRPC_UpdateAbilityCooldownVisuals", PhotonTargets.All, i, -1);

					}
				}

			}


		}


	}

}


	
