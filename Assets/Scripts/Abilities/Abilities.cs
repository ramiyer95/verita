﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Abilities : Photon.MonoBehaviour {

	public Board board;

	public Team currentTeam;

	//change this later based on whether the ability was actually picked or not.
	public bool isPicked = false;

	public TurnManager turnManager;

	public Tracker tracker;

	public AbilityType thisAbilityType;


	public void StartOnBoardLoad() {

		board = GameObject.Find("Board").GetComponent<Board>();

		turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();

		tracker = this.gameObject.GetComponent<Tracker>();

	}


	public bool IsReadyToUse (Team theTeam) {

		// Only allow a player to use abilities when it is his turn.
		if (theTeam != board.currentTurn) {

			return false;

		}


		// cooldown of -1 in relevant list means that the ability is ready to use
		if (theTeam == Team.BLUE) {

			if (tracker.blueAbilitiesCooldown[GlobalFunctions.ResolveAbilityToID(thisAbilityType) - 1] != -1) {

				return false;

			}

		} else {

			if (tracker.redAbilitiesCooldown[GlobalFunctions.ResolveAbilityToID(thisAbilityType) - 1] != -1) {

				return false;

			}

		}


		// if cooldownOverAtTurn less than or equal to currentTurnNumber, then return true, else false

		return true;

	} 


	public virtual void ShowErrorToPlayer() {

		// If conditions to execute the ability are not favorable, then the user must be shown this message.

		// For example, if there are no enemy units on your side of the 'mountains' and you use the canon defender
		// or if you use DoubleMove but you dont have any pawns
		// or if you dont have units on the Mountain squares but you use fortify
		// or if both rook and bishop are dead but you use Rook-Bishop Swap

	}


	public virtual void InterruptReset() {

		// the purpose of this function is to reset the game state when the player input is blocked off by the timer

		// each ability will have some sort of different thing. Thus this is a virtual function to be overriden by the 
		//  individual abilities


	}



}
