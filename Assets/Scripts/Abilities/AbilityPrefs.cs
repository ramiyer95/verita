﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityPrefs : MonoBehaviour {


	public AbilityType hiddenPicked;

	public List<AbilityType> normalPicked = new List<AbilityType>();

    
	public void ExtractAbilityPrefsFromMemory() {


		int numberOfHiddenAb = 0;
		int numberOfNormalAb = 0;

        bool h_rookBishopSwap = false;
        bool h_cannonDefender = false;
        bool h_fortify = false;
        bool h_doubleMove = false;
        bool h_sacrifice = false;
        bool h_stonePieces = false;

        bool n_rookBishopSwap = false;
        bool n_cannonDefender = false;
        bool n_fortify = false;
        bool n_doubleMove = false;
        bool n_sacrifice = false;
        bool n_stonePieces = false;


        
        if (PlayerPrefs.GetInt("h_rookBishopSwap") == 1)
        {
            h_rookBishopSwap = true;
            numberOfHiddenAb++;

        }

        if (PlayerPrefs.GetInt("h_canonDefender") == 1)
        {
            h_cannonDefender = true;
            numberOfHiddenAb++;

        }

        if (PlayerPrefs.GetInt("h_fortify") == 1)
        {
            h_fortify = true;
            numberOfHiddenAb++;

        }

        if (PlayerPrefs.GetInt("h_doubleMove") == 1)
        {
            h_doubleMove = true;
            numberOfHiddenAb++;

        }

        if (PlayerPrefs.GetInt("h_sacrifice") == 1)
        {
            h_sacrifice = true;
            numberOfHiddenAb++;

        }

        if (PlayerPrefs.GetInt("h_stonePieces") == 1)
        {
            h_stonePieces = true;
            numberOfHiddenAb++;

        }




        if (PlayerPrefs.GetInt("n_rookBishopSwap") == 1)
        {
            n_rookBishopSwap = true;
            numberOfNormalAb++;

        }

        if (PlayerPrefs.GetInt("n_canonDefender") == 1)
        {
            n_cannonDefender = true;
            numberOfNormalAb++;

        }

        if (PlayerPrefs.GetInt("n_fortify") == 1)
        {
            n_fortify = true;
            numberOfNormalAb++;

        }

        if (PlayerPrefs.GetInt("n_doubleMove") == 1)
        {
            n_doubleMove = true;
            numberOfNormalAb++;

        }

        if (PlayerPrefs.GetInt("n_sacrifice") == 1)
        {
            n_sacrifice = true;
            numberOfNormalAb++;

        }

        if (PlayerPrefs.GetInt("n_stonePieces") == 1)
        {
            n_stonePieces = true;
            numberOfNormalAb++;

        }
        
        

		if (!(numberOfHiddenAb == 1 && numberOfNormalAb == 2)) {

			ResetToDefaults();

			return;

		}


		if (h_rookBishopSwap) {

			hiddenPicked = AbilityType.ROOKBISHOPSWAP;

		} else if (h_cannonDefender) {

			hiddenPicked = AbilityType.CANONDEFENDER;

		} else if (h_fortify) {

			hiddenPicked = AbilityType.FORTIFY;

		} else if (h_doubleMove) {

			hiddenPicked = AbilityType.DOUBLEMOVE;

		} else if (h_sacrifice) {

			hiddenPicked = AbilityType.SACRIFICE;

		} else if (h_stonePieces) {

			hiddenPicked = AbilityType.STONEPIECES;

		}



		if (n_rookBishopSwap) {

			if (hiddenPicked == AbilityType.ROOKBISHOPSWAP) {

				ResetToDefaults();

				return;
			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.ROOKBISHOPSWAP);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add( AbilityType.ROOKBISHOPSWAP);

			}

		}


		if (n_cannonDefender) {

			if (hiddenPicked == AbilityType.CANONDEFENDER) {

				ResetToDefaults();

				return;
			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.CANONDEFENDER);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add(  AbilityType.CANONDEFENDER);

			}

		}

		if (n_fortify) {

			if (hiddenPicked == AbilityType.FORTIFY) {

				ResetToDefaults();

				return;

			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.FORTIFY);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add( AbilityType.FORTIFY);

			}

		}


		if (n_doubleMove) {

			if (hiddenPicked == AbilityType.DOUBLEMOVE) {

				ResetToDefaults();

				return;

			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.DOUBLEMOVE);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add( AbilityType.DOUBLEMOVE);

			}

		}


		if (n_sacrifice) {

			if (hiddenPicked == AbilityType.SACRIFICE) {

				ResetToDefaults();

				return;
			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.SACRIFICE);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add( AbilityType.SACRIFICE);

			}

		}


		if (n_stonePieces) {

			if (hiddenPicked == AbilityType.STONEPIECES) {

				ResetToDefaults();

				return;
			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.STONEPIECES);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add(  AbilityType.STONEPIECES);

			}

		}


	}



	private void ResetToDefaults() {

		normalPicked.Add(AbilityType.CANONDEFENDER);
		normalPicked.Add( AbilityType.STONEPIECES);

		hiddenPicked = AbilityType.SACRIFICE;

	}

}
