﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class RookBishopSwap : Abilities {

	
	public Rook rook;

	public Bishop bishop;



	public bool AreSpecialConditionsForAbilityMet() {

		if (GameObject.FindObjectOfType<Rook>()) {

			foreach (Rook theRook in GameObject.FindObjectsOfType<Rook>()) {

				if (theRook.team == this.currentTeam) {

					this.rook = theRook;

				}

			}

		}

		if (GameObject.FindObjectOfType<Bishop>()) {

			foreach (Bishop theBishop in GameObject.FindObjectsOfType<Bishop>()) {

				if (theBishop.team == this.currentTeam) {

					this.bishop = theBishop;

				}

			}

		}



		if (!rook && !bishop) {

			return false;	

		} 


		if (!bishop) {

			if (this.currentTeam == Team.BLUE) {

				if (board.veritaMenOnBoard[6,5]) {

					return false;

				}

			} else {

				if(board.veritaMenOnBoard[0,1]) {

					return false;

				}

			}

		}


		if (!rook) {

			if (this.currentTeam == Team.BLUE) {

				if (board.veritaMenOnBoard[5,6]) {

					return false;

				}

			} else {

				if(board.veritaMenOnBoard[1,0]) {

					return false;

				}

			}

		}


		return true;

	}




	public void ExecuteAbility(int viewIDOfPlayerController) {

		// The following line resolves the team that is current using the ability.
		// This line is crucial because the SpecialAbilitiesManager is a singular entity and needs to know which
		// 	team it is supposed to run the ability on behalf of.


		currentTeam = (Team)board.photonViewID_clientTeam[viewIDOfPlayerController];

		if (!IsReadyToUse(currentTeam))  {

			return;

		}

		if (currentTeam == Team.BLUE) {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().bluePickedAbilities[AbilityType.ROOKBISHOPSWAP];

		} else {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().redPickedAbilities[AbilityType.ROOKBISHOPSWAP];

		}

		if (!isPicked) {

			return;

		}


		if (!AreSpecialConditionsForAbilityMet()) {

			return;

		}


		// Return true if the said ability was executed successfully, otherwise return false.
		// Can use the boolean returned from this function as the condition to Switch Turns in the Board script.

		Vector2 newBishopPos;

		Vector2 newRookPos;


		if (rook) {

			newBishopPos = rook.pos;

		} else {

			if (this.currentTeam == Team.BLUE ) {

				newBishopPos = new Vector2 (5,6);

			} else {

				newBishopPos = new Vector2 (1,0);

			}

		}


		if (bishop) {

			newRookPos = bishop.pos;

		} else {

			if (this.currentTeam == Team.BLUE) {

				newRookPos = new Vector2 (6,5);

			} else {

				newRookPos = new Vector2 (0,1);

			}

		}

		// execute the actual swap

		if (rook) {

			rook.transform.position = GlobalFunctions.GridToWorldCoordinates(newRookPos);

			rook.pos = newRookPos;

			board.veritaMenOnBoard[(int)newRookPos.x, (int)newRookPos.y] = rook;

		}

		if (bishop) {

			bishop.transform.position = GlobalFunctions.GridToWorldCoordinates(newBishopPos);

			bishop.pos = newBishopPos;

			board.veritaMenOnBoard[(int)newBishopPos.x, (int)newBishopPos.y] = bishop;

		}

		InterruptReset();

        board.GetComponent<AudioNetworking>().CallPRPCPlaySound("rbs_audio");

        tracker.SetCooldownOfAbilityForTeam(thisAbilityType, currentTeam);

		board.SwitchTurn(false);

	}



	public override void InterruptReset() {

		rook = null;

		bishop = null;

	}





}
