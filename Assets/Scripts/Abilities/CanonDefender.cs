﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CanonDefender : Abilities {

	private int i;
	private int j;

	private List<int[]> veritaMenToKill ;

	private NetworkConnection currentClientConnection;



	// Loop through veritaMenOnBoard since it will be more efficient
	public void ExecuteAbility(int viewIDOfPlayerController) {
        
		currentTeam = (Team)board.photonViewID_clientTeam[viewIDOfPlayerController];

		if (!IsReadyToUse(currentTeam)) {

			return;

		}

		if (currentTeam == Team.BLUE) {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().bluePickedAbilities[AbilityType.CANONDEFENDER];

		} else {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().redPickedAbilities[AbilityType.CANONDEFENDER];

		}


		if(!isPicked) {

			return;

		}

		veritaMenToKill = new List<int[]>();


		for (i = 0; i<7; i++) {

			for (j = 0; j<7; j++) {

				if (board.veritaMenOnBoard[i,j]) {

					if(!(board.veritaMenOnBoard[i,j].team == this.currentTeam)) {

						if (this.currentTeam == Team.BLUE) {

							if ( (int)board.veritaMenOnBoard[i,j].pos.x + (int)board.veritaMenOnBoard[i,j].pos.y >= 7) {

								veritaMenToKill.Add(new int[2] {i, j} );

							} 


						} else {

							if ( (int)board.veritaMenOnBoard[i,j].pos.x + (int)board.veritaMenOnBoard[i,j].pos.y <= 5) {

								veritaMenToKill.Add(new int[2] {i, j} );

							} 


						}

					}


				}


			}

		}


		// after running the loop, the relevant list is ready
		// need to choose a random number between 0 and the list count

		if (veritaMenToKill.Count < 1) {

			return;

		}

        board.GetComponent<AudioNetworking>().CallPRPCPlaySound("canon_audio");

        int k = Random.Range(1, veritaMenToKill.Count) ;


		//for the same Ghost Bulldozer problem that is faced in the VeritaMan Script
		if (board.veritaMenOnBoard[veritaMenToKill[k-1][0], veritaMenToKill[k-1][1]].gameObject.GetComponent<Bulldozer>())
        {
            if (currentTeam == Team.BLUE)
            {

                GameObject.FindObjectOfType<WallManager>().ResetSiegeValues(Team.RED);

            } else
            {

				GameObject.FindObjectOfType<WallManager>().ResetSiegeValues(Team.BLUE);

            }
            

			board.veritaMenOnBoard[veritaMenToKill[k-1][0], veritaMenToKill[k-1][1]].gameObject.GetComponent<Bulldozer>().toBeDestroyedInCurrentFrame = true;

        }


		PhotonNetwork.Destroy(board.veritaMenOnBoard[veritaMenToKill[k-1][0], veritaMenToKill[k-1][1]].gameObject);
                       
		board.veritaMenOnBoard[veritaMenToKill[k-1][0], veritaMenToKill[k-1][1]] = null;

		veritaMenToKill = null;	

		tracker.SetCooldownOfAbilityForTeam(thisAbilityType, currentTeam);

		board.SwitchTurn(false);

	}
}
