﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Sacrifice : Abilities {

	public BoardHighlighting boardHighlighter;

	private bool[,] theMovesArray;

    private PhotonView pv;

	private bool interactingWithThis;

	private PlayerController thisPlayerController;

	private NetworkConnection currentClientConnection;

	// Remove clientAuthority in interrupt only when the ability use has been initiated
	private bool initiated = false;

	public GameObject b_queen;
	public GameObject b_rook;
	public GameObject b_bishop;
	public GameObject b_superpawn;

	public GameObject r_queen;
	public GameObject r_rook;
	public GameObject r_bishop;
	public GameObject r_superpawn;

	public GameObject parentSacrificeUI;


	private GameObject queenUI;
	private GameObject rookUI;
	private GameObject bishopUI;
	private GameObject superPawnUI;

	public VeritaMen upgradeFrom;


    public void InstantiateUIPrefabs () {

		
		if (parentSacrificeUI.transform.childCount > 0) {

			return;

		}



		if (this.currentTeam == Team.BLUE) {

			queenUI = Instantiate(b_queen, parentSacrificeUI.transform) as GameObject;
			rookUI = Instantiate(b_rook, parentSacrificeUI.transform) as GameObject;
			bishopUI = Instantiate(b_bishop, parentSacrificeUI.transform) as GameObject;
			superPawnUI = Instantiate(b_superpawn, parentSacrificeUI.transform) as GameObject;

		} else {


			queenUI = Instantiate(r_queen, parentSacrificeUI.transform) as GameObject;
			rookUI = Instantiate(r_rook, parentSacrificeUI.transform) as GameObject;
			bishopUI = Instantiate(r_bishop, parentSacrificeUI.transform) as GameObject;
			superPawnUI = Instantiate(r_superpawn, parentSacrificeUI.transform) as GameObject;

		}


        //Add appropriate listeners for the buttons too.

        // This is because we are passing multiple parameters into the Sacrifice function
        // Multiple parameters cannot be passed directly
        
        queenUI.transform.Find("Rook").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "rook", -1)  ) ;
        
        queenUI.transform.Find("Bishop").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "bishop", -1)  ) ;

        queenUI.transform.Find("Superpawn").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "superpawn", -1)) ;


		rookUI.transform.Find("Queen").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "queen", GlobalFunctions.rook_sacrifice[0] + 1)) ;

		rookUI.transform.Find("Queen").transform.Find("Text").GetComponent<Text>().text = GlobalFunctions.rook_sacrifice[0].ToString();

        rookUI.transform.Find("Bishop").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "bishop", GlobalFunctions.rook_sacrifice[1] + 1)) ;

        rookUI.transform.Find("Bishop").transform.Find("Text").GetComponent<Text>().text = GlobalFunctions.rook_sacrifice[1].ToString();

        rookUI.transform.Find("Superpawn").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "superpawn", -1)) ;


		bishopUI.transform.Find("Queen").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "queen", GlobalFunctions.bishop_sacrifice[0] + 1)) ;

		bishopUI.transform.Find("Queen").GetComponentInChildren<Text>().text = GlobalFunctions.bishop_sacrifice[0].ToString();

        bishopUI.transform.Find("Rook").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "rook", GlobalFunctions.bishop_sacrifice[1] + 1)) ;

        bishopUI.transform.Find("Rook").GetComponentInChildren<Text>().text = GlobalFunctions.bishop_sacrifice[1].ToString();

        bishopUI.transform.Find("Superpawn").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "superpawn", -1)) ;


		superPawnUI.transform.Find("Rook").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "rook", GlobalFunctions.pawn_sacrifice[1] + 1)) ;

        superPawnUI.transform.Find("Rook").GetComponentInChildren<Text>().text = GlobalFunctions.pawn_sacrifice[1].ToString();

        superPawnUI.transform.Find("Bishop").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "bishop", GlobalFunctions.pawn_sacrifice[2] + 1)) ;

        superPawnUI.transform.Find("Bishop").GetComponentInChildren<Text>().text = GlobalFunctions.pawn_sacrifice[2].ToString();

        superPawnUI.transform.Find("Queen").GetComponent<Button>().onClick.AddListener( 
            () => photonView.RPC("PRPC_CallSacrificePieceOnServer", PhotonTargets.MasterClient, "queen", GlobalFunctions.pawn_sacrifice[0] + 1)) ;

        superPawnUI.transform.Find("Queen").GetComponentInChildren<Text>().text = GlobalFunctions.pawn_sacrifice[0].ToString(); 


	}




	public void ExecuteAbility(int viewIDOfPlayerController) {


		currentTeam = (Team)board.photonViewID_clientTeam[viewIDOfPlayerController];

		if(!IsReadyToUse(currentTeam)) {

			return;

		}


		bool isThisBlue;

		if (currentTeam == Team.BLUE) {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().bluePickedAbilities[AbilityType.SACRIFICE];

			isThisBlue = true;

		} else {

			isPicked = (bool) this.gameObject.GetComponent<Tracker>().redPickedAbilities[AbilityType.SACRIFICE];

			isThisBlue = false;
		}



		if (!isPicked) {

			return;

		}


        pv = PhotonView.Find(viewIDOfPlayerController);
        
        thisPlayerController = pv.gameObject.GetComponent<PlayerController>();

		/*currentClientConnection = NetworkServer.objects[(NetworkInstanceId) board.netId_netIdObject[netIdOfPlayerController]].connectionToClient;

		this.gameObject.GetComponent<NetworkIdentity>().AssignClientAuthority(currentClientConnection);*/
        
		initiated = true;

		theMovesArray = ReturnBlankMovesArray();

		int i = 0;

		int j = 0;


		for (i = 0; i <7; i++) {

			for(j = 0; j < 7; j++) {

				if (board.veritaMenOnBoard[i,j]) {

					if (board.veritaMenOnBoard[i,j].team == this.currentTeam) {

						if (!board.veritaMenOnBoard[i,j].GetComponent<Bulldozer>()) {

							theMovesArray[i,j] = true;

						}

					}

				}

			}

		}


		byte[] serializedMovesArray = GlobalFunctions.SerializeToByteArray<bool[,]>(theMovesArray);

        photonView.RPC("PRPC_StepOneSacrifice", PhotonTargets.All, serializedMovesArray, viewIDOfPlayerController, isThisBlue);

        board.GetComponent<AudioNetworking>().CallPRPCPlaySound("sacrifice_audio");

    }




	public bool[,] ReturnBlankMovesArray()
    {

        bool[,] theMovesArray = new bool[7, 7];

        for (int i = 0; i<7; i++)
        {

            for (int j =0; j<7; j++)
            {

                theMovesArray[i, j] = false;  

            }

        }

        return theMovesArray;

    }






    //Ability activate from here. This is the first step. (Second step is to choose which piece to spawn)

    public void OnMouseDown () {

    	if(!interactingWithThis) {

    		return;

    	}

        AudioManager.instance.PlaySound("tap_audio");

        // depending on which VeritaMan was clicked, we need to activate the relevant canvas

        Vector2 clickedPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

		Vector2 mousePos = GlobalFunctions.WorldToGridCoordinates(clickedPos);

        int pos_x = (int) mousePos.x;
        int pos_y = (int) mousePos.y;

        photonView.RPC("PRPC_TellMeWhichUItoActivate", PhotonTargets.MasterClient, pos_x, pos_y);		

    }



    public void SacrificePiece( string upgradeTo, int killPieceInXTurns ) {

    	int thisTurn = 0;

    	if (this.currentTeam == Team.BLUE) {

    		thisTurn = turnManager.blueTurn;

    	} else {

    		thisTurn = turnManager.redTurn;

    	}

    	int spawnAtX = (int) upgradeFrom.pos.x;

    	int spawnAtY = (int) upgradeFrom.pos.y;



    	string upgradeFromString = " ";

    	if (upgradeFrom.GetComponent<Queen>()) {

    		upgradeFromString = "queen";

    	} else if (upgradeFrom.GetComponent<Rook>()) {

    		upgradeFromString = "rook";

    	} else if (upgradeFrom.GetComponent<Bishop>()) {

    		upgradeFromString = "bishop";

    	} else if (upgradeFrom.GetComponent<Superpawn>()) {

    		upgradeFromString = "superpawn";

    	}


    	upgradeFrom.addThisToRespawnList = false;

    	PhotonNetwork.Destroy(upgradeFrom.gameObject);


    	if (upgradeTo == "queen") {

    		VeritaMen newQueen = board.SpawnQueen(this.currentTeam, spawnAtX, spawnAtY);
                		
    		newQueen.killThisAtTurn = thisTurn + killPieceInXTurns;

    		newQueen.beforeSacrifice = upgradeFromString;

    		newQueen.permaState = VeritaManState.CORRUPTED;

            newQueen.GetComponent<SpriteManager>().CallPRPCUpdateSprites();
    	}


    	if (upgradeTo == "rook") {

            VeritaMen newRook = board.SpawnRook(this.currentTeam, spawnAtX, spawnAtY);

            if (killPieceInXTurns != -1) {
                // killPieceInXTurns = -1 would mean that downgrade has happened, hence piece need not be killed.

                newRook.killThisAtTurn = thisTurn + killPieceInXTurns;

    			newRook.permaState = VeritaManState.CORRUPTED;


            } else {

    			//Code for downgrading
                
				newRook.killThisAtTurn = -1;

    			newRook.permaState = VeritaManState.DOWNGRADED;

            }

            newRook.beforeSacrifice = upgradeFromString;

            newRook.gameObject.GetComponent<SpriteManager>().CallPRPCUpdateSprites();

        }

		if (upgradeTo == "bishop") {

            VeritaMen newBishop = board.SpawnBishop(this.currentTeam, spawnAtX, spawnAtY);

            if (killPieceInXTurns != -1) {
    		// killPieceInXTurns = -1 would mean that downgrade has happened, hence piece need not be killed.
                			
    			newBishop.killThisAtTurn = thisTurn + killPieceInXTurns;
                
    			newBishop.permaState = VeritaManState.CORRUPTED;

            } else {

    			//Code for downgrading
                
				newBishop.killThisAtTurn = -1;
                
    			newBishop.permaState = VeritaManState.DOWNGRADED;

            }

            newBishop.beforeSacrifice = upgradeFromString;

            newBishop.GetComponent<SpriteManager>().CallPRPCUpdateSprites();

        }


		if (upgradeTo == "superpawn") {


    		VeritaMen newSuperPawn = board.SpawnSuperpawn(this.currentTeam, spawnAtX, spawnAtY);

    		newSuperPawn.killThisAtTurn = -1;

    		newSuperPawn.beforeSacrifice = upgradeFromString;

    		newSuperPawn.permaState = VeritaManState.DOWNGRADED;

            newSuperPawn.GetComponent<SpriteManager>().CallPRPCUpdateSprites();
            
        }


        //now that the sacrifice is complete, we need to get the board back into the normal state.

        // 1. No longer interacting with ability UI (reset property on board)
        // 2. Hide the canvas UI (set it to inactive)
        photonView.RPC("PRPC_RevertToNormalState", PhotonTargets.All);
        
		
		initiated = false;

		tracker.SetCooldownOfAbilityForTeam(thisAbilityType, currentTeam);

    	board.SwitchTurn(false);
    }





    private void HideSacrificeUI() {

    	//need to set all sacrifice UI to false

    	// since this function can also be called from the InterruptAbility() function, we need to check for null before 
    	// 		using setactive function.

    	// since all of them are spawned at the same time, it is ok if only one is checked 

    	if (queenUI != null) {

	    	queenUI.SetActive(false);

	    	rookUI.SetActive(false);

	    	bishopUI.SetActive(false);

	    	superPawnUI.SetActive(false);

    	}
    }




    [PunRPC]

    void PRPC_StepOneSacrifice (byte[] serializedMovesArray, int viewIDOfPlayerController, bool isThisBlue) {

        pv = PhotonView.Find(viewIDOfPlayerController);


        if (!pv.isMine) 
        {

            return;
            
        }


    	if (isThisBlue) {

    		currentTeam = Team.BLUE ;

    	} else {

    		currentTeam = Team.RED ;

    	}


        thisPlayerController = pv.gameObject.GetComponent<PlayerController>();
        
    	theMovesArray = GlobalFunctions.DeserializeToObject<bool[,]>(serializedMovesArray);

		interactingWithThis = true;

		//Instantiate the prefabs if they donot already exist (condition checked in the function)
		InstantiateUIPrefabs();

		boardHighlighter = GameObject.Find("Board").GetComponent<BoardHighlighting>();

		thisPlayerController.interactingWithAbilityUI = true;

		boardHighlighter.HighlightSacrificeUnits(theMovesArray);

		this.gameObject.GetComponent<BoxCollider2D>().enabled = true;

    }



    //to be called only on the master client
    [PunRPC]

    void PRPC_CallSacrificePieceOnServer ( string upgradeTo, int killPieceInXTurns) {

    	SacrificePiece(upgradeTo, killPieceInXTurns);

    }



    [PunRPC]

    void PRPC_TellMeWhichUItoActivate (int pos_x, int pos_y) {

		if (theMovesArray[pos_x, pos_y]) {


			if(board.veritaMenOnBoard[pos_x, pos_y].GetComponent<Superpawn>()) {

                photonView.RPC("PRPC_ActivateSuperPawnUI", PhotonTargets.All);

			}

			if(board.veritaMenOnBoard[pos_x, pos_y].GetComponent<Bishop>()) {

                photonView.RPC("PRPC_ActivateBishopUI", PhotonTargets.All);

			}

			if(board.veritaMenOnBoard[pos_x, pos_y].GetComponent<Rook>()) {

                photonView.RPC("PRPC_ActivateRookUI", PhotonTargets.All);

            }

			if(board.veritaMenOnBoard[pos_x, pos_y].GetComponent<Queen>()) {

                photonView.RPC("PRPC_ActivateQueenUI", PhotonTargets.All);

            }

			upgradeFrom = board.veritaMenOnBoard[pos_x, pos_y];

		}


    }


    [PunRPC] 

    void PRPC_ActivateSuperPawnUI () {

        if (!pv.isMine)
        {

            return;

        }

    	HideSacrificeUI();

		superPawnUI.SetActive(true);

		boardHighlighter.HideHighlights();

    }


	[PunRPC] 

    void PRPC_ActivateBishopUI () {

        if (!pv.isMine)
        {

            return;

        }

        HideSacrificeUI();

		bishopUI.SetActive(true);

		boardHighlighter.HideHighlights();

    }

	[PunRPC] 

    void PRPC_ActivateRookUI () {

        if (!pv.isMine)
        {

            return;

        }

        HideSacrificeUI();

		rookUI.SetActive(true);

		boardHighlighter.HideHighlights();

    }

	[PunRPC] 

    void PRPC_ActivateQueenUI () {

        if (!pv.isMine)
        {

            return;

        }

        HideSacrificeUI();

		queenUI.SetActive(true);

		boardHighlighter.HideHighlights();

    }


    [PunRPC]

    void PRPC_RevertToNormalState () {

        if (!pv.isMine)
        {

            return;

        }

        thisPlayerController.interactingWithAbilityUI = false;

    	this.GetComponent<BoxCollider2D>().enabled = false;

    	HideSacrificeUI();

    }



    public override void InterruptReset() {

		// if playerController is null, it means that Execute ability has never run on this client. Thus it impliex that
    	//   interacting with abilityUI will be false. (similarly with board highlighter)


		if (thisPlayerController != null) {

    		thisPlayerController.interactingWithAbilityUI = false;

    	}

    	this.GetComponent<BoxCollider2D>().enabled = false;

    	if ( boardHighlighter != null ) {

    		boardHighlighter.HideHighlights();

    	}


    	//clientAuthorityOwner contains the connection object that has client authority for this network identity
    	// thus, if it exists, we simply remove authority from that client

		/*if (this.gameObject.GetComponent<NetworkIdentity>().clientAuthorityOwner != null) {

			this.gameObject.GetComponent<NetworkIdentity>().RemoveClientAuthority(this.gameObject.GetComponent<NetworkIdentity>().clientAuthorityOwner);

    	}*/

    	initiated = false;

    	HideSacrificeUI();


    }



}
