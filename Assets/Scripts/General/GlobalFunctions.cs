﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;



// ********** VERSION OF THE GAME ***************

public static class VersionHelper
{
    //Version 1 was the game with UNet

    //Version 2 is the game with Photon 

    public const string VERSIONNUMBER = "2";

}


// The ENUMS used throughout the game

public enum Team { BLUE, RED };

public enum VeritaMenType { QUEEN, ROOK, BISHOP, SUPERPAWN, BULLDOZER };

public enum AbilityType { ROOKBISHOPSWAP, CANONDEFENDER, FORTIFY, DOUBLEMOVE, SACRIFICE, STONEPIECES };

public enum GameState { NOTSTARTED, PLAY, RECONNECTING, PAUSED, WON, LOST };

public enum VeritaManState { CORRUPTED, DOWNGRADED, STONE, TURNGLOW, NORMAL };

public enum AudioLayer { BACKGROUND, GENERAL, INDICATIONS, ABILITIES };
//The purpose of creating audio layers is to ensure two things :
//  If there is a conflict within a layer, it can be managed with layer specific logic
//  If there is a conflict between layers, then the audio of the higher layer should be played at a higher volume
//    than the audio of a lower layer




public class GlobalFunctions : MonoBehaviour {
	
	public static string sceneToLoadNext = "start_scene";

    
    public static GameState? theGameState = GameState.NOTSTARTED;

    // need to set this in the editor
    [Tooltip("Queen,Rook,Bishop,Superpawn,Bulldozer")]
	public List<int> visibleRespawnCD = new List<int>();
	public static List<int> respawnCD = new List<int>();

	[Tooltip("RBS, CanonDef, Fortify, DoubleMove, Sacrifice, StonePieces")]
	public List<int> visibleAbilitiesCD = new List<int>();
	public static List<int> abilitiesCD = new List<int>();

    
	[Tooltip("to Queen (>= 1), to Bishop (>= 1), to Pawn")]
	public List<int> visibleRook_sacrifice = new List<int>();
	public static List<int> rook_sacrifice = new List<int>();


	[Tooltip("to Queen (>= 1), to Rook (>= 1), to Pawn")]
	public List<int> visibleBishop_sacrifice = new List<int>();
	public static List<int> bishop_sacrifice = new List<int>();

    
	[Tooltip("to Queen (>= 1), to Rook (>= 1), to Bishop (>= 1)")]
	public List<int> visiblePawn_sacrifice = new List<int>();
	public static List<int> pawn_sacrifice = new List<int>();

	//using two variables here because one is to be seen in the editor and the other is to be used in the static function
	// on the start, setting the static variable equal to the visible variable

	// THESE LISTS SHOULD BE THE SINGLE INTERFACE FOR SETTING RESPAWN CDS

	void Start() {

		respawnCD = visibleRespawnCD;

		abilitiesCD = visibleAbilitiesCD;

		pawn_sacrifice = visiblePawn_sacrifice;
		rook_sacrifice = visibleRook_sacrifice;
		bishop_sacrifice = visibleBishop_sacrifice ;

	}


	public static byte[] SerializeToByteArray<T> (T toSerialize) {

		BinaryFormatter bf = new BinaryFormatter();

		MemoryStream ms = new MemoryStream();

		bf.Serialize(ms, toSerialize);

		return ms.ToArray();

	}


	public static T DeserializeToObject<T> (byte[] toDeserialize) {

		BinaryFormatter bf = new BinaryFormatter();

		MemoryStream ms = new MemoryStream(toDeserialize);

		return (T)bf.Deserialize(ms);

	}




	// This function can return null (Question mark takes care of that)

	public static AbilityType? ResolveIDToAbility(int abilityID) {

		if (abilityID == 1) {

			return AbilityType.ROOKBISHOPSWAP;

		} else if (abilityID == 2) {

			return AbilityType.CANONDEFENDER;

		} else if (abilityID == 3) {

			return AbilityType.FORTIFY;

		} else if (abilityID == 4) {

			return AbilityType.DOUBLEMOVE;

		} else if (abilityID == 5) {

			return AbilityType.SACRIFICE;

		} else if (abilityID == 6) {

			return AbilityType.STONEPIECES;

		} else {

			return null;

		}

	}



	public static int ResolveAbilityToID ( AbilityType theAbility) {

		// can rewrite this using switch case (and other functions in this class)

		if (theAbility == AbilityType.ROOKBISHOPSWAP) {

			return 1;

		} else if (theAbility == AbilityType.CANONDEFENDER) {

			return 2;

		} else if (theAbility == AbilityType.FORTIFY) {

			return 3;

		} else if (theAbility == AbilityType.DOUBLEMOVE) {

			return 4;

		} else if (theAbility == AbilityType.SACRIFICE) {

			return 5;

		} else {

			return 6;

		}

	}


    public static GameState? ResolveIDtoGameState (int gameStateID)
    {

        if (gameStateID == 1)
        {

            return GameState.NOTSTARTED;

        } else if (gameStateID == 2)
        {

            return GameState.PLAY;

        } else if (gameStateID == 3)
        {

            return GameState.RECONNECTING;

        } else if (gameStateID == 4)
        {

            return GameState.PAUSED;

        } else if (gameStateID == 5)
        {

            return GameState.LOST;

        } else if (gameStateID == 6)
        {

            return GameState.WON;

        }

        return null;

    }


    public static int ResolveGameStatetoID (GameState gameState)
    {

        switch (gameState)
        {
            case GameState.NOTSTARTED:

                return 1;

            case GameState.PLAY:

                return 2;

            case GameState.RECONNECTING:

                return 3;

            case GameState.PAUSED:

                return 4;

            case GameState.LOST:

                return 5;

            case GameState.WON:

                return 6;
                
        }

        return 0;

    }
        



	public static Vector2 WorldToGridCoordinates(Vector2 worldUnits) {

        //Use this function unviersally to convert any point on the board (in world coordinates) to the
        //  7x7 grid that is widely used throughout the game code. 

		int x = (int) ( (worldUnits.x + 261.5) * 7 / 523 );

		int y = (int) ( (worldUnits.y + 261.5) * 7 / 523 );

		if (x > 6) {

			x = 6;

		} else if (x < 0) {

			x = 0;

		}


		if (y > 6) {

			y = 6;

		} else if (y < 0) {

			y = 0;

		}


		Vector2 output = new Vector2 (x, y);

		return output;

	}




    public static Vector3 GridToWorldCoordinates(Vector2 inputBoardCoordinates)
    {
        //Use this function to convert point on the 7x7 grid to worlds coordinates


        Vector3 output = new Vector3((-260 + (520 / 14) + ((inputBoardCoordinates.x * 523) / 7)), (-260 + (520 / 14) + ((inputBoardCoordinates.y * 523) / 7)), 0);

        return output;

    }



    public static VeritaMenType? ResolveStringToVeritaManType ( string theVeritaMan ) {

		if (theVeritaMan == "queen") {

			return VeritaMenType.QUEEN;

		} else if (theVeritaMan == "rook") {

			return VeritaMenType.ROOK;

		} else if (theVeritaMan == "bishop") {

			return VeritaMenType.BISHOP;

		} else if (theVeritaMan == "superpawn") {

			return VeritaMenType.SUPERPAWN;

		} else if (theVeritaMan == "bulldozer") {

			return VeritaMenType.BULLDOZER;

		} else {

		// if a faulty string is passed return null

			return null;

		}

	}



	public static int GetCooldownOfVeritaMantype ( VeritaMenType veritaManType ) {

		switch (veritaManType) {

			case VeritaMenType.QUEEN :

				return respawnCD[0]; 

			case VeritaMenType.ROOK :

				return respawnCD[1] ;

			case VeritaMenType.BISHOP :

				return respawnCD[2] ;

			case VeritaMenType.SUPERPAWN :

				return respawnCD[3] ;

			case VeritaMenType.BULLDOZER :

				return respawnCD[4];

		}

		return -1;

	}



	public static int GetCooldownOfAbility (AbilityType abilityType) {

		switch (abilityType) {

			case AbilityType.ROOKBISHOPSWAP :

				return abilitiesCD[0];

			case AbilityType.CANONDEFENDER :

				return abilitiesCD[1];

			case AbilityType.FORTIFY :

				return abilitiesCD[2];

			case AbilityType.DOUBLEMOVE :

				return abilitiesCD[3];

			case AbilityType.SACRIFICE :

				return abilitiesCD[4];

			case AbilityType.STONEPIECES :

				return 	abilitiesCD[5];

			
		}

		return -1;

	}


    public static int ResolveVeritaManTypeToID ( VeritaMenType theVeritaManType ) {

    	switch (theVeritaManType)
    	{

    		case VeritaMenType.QUEEN :

    			return 1;

			case VeritaMenType.ROOK :

				return 2;

			case VeritaMenType.BISHOP :

				return 3;

			case VeritaMenType.SUPERPAWN :

				return 4;

			case VeritaMenType.BULLDOZER :

				return 5;

    	}

    	return -1;

    }


    public static VeritaMenType? ResolveIDToVeritaManType (int veritaManTypeID) {

    	if (veritaManTypeID == 1) {

    		return VeritaMenType.QUEEN;

    	} else if (veritaManTypeID == 2) {

    		return VeritaMenType.ROOK;

    	} else if (veritaManTypeID == 3) {

    		return VeritaMenType.BISHOP;

    	} else if (veritaManTypeID == 4) {

    		return VeritaMenType.SUPERPAWN;

    	} else if (veritaManTypeID == 5) {

    		return VeritaMenType.BULLDOZER;

    	} 

    	return null;


    }



    public static int ResolveVeritaManStateToID (VeritaManState theState)
    {

        switch (theState)
        {

            case VeritaManState.CORRUPTED:

                return 1;

            case VeritaManState.DOWNGRADED:

                return 2;

            case VeritaManState.STONE:

                return 3;

            case VeritaManState.TURNGLOW:

                return 4;

            case VeritaManState.NORMAL:

                return 5;
                
        }

        return -1;
        
    }


    public static VeritaManState? ResovleIDToVeritaManState(int stateID)
    {
        if (stateID == 1)
        {

            return VeritaManState.CORRUPTED;

        } else if (stateID == 2)
        {

            return VeritaManState.DOWNGRADED;

        } else if (stateID == 3)
        {

            return VeritaManState.STONE;    

        } else if (stateID == 4)
        {

            return VeritaManState.TURNGLOW;

        } else if (stateID == 5)
        {

            return VeritaManState.NORMAL;

        }


        return null;
    }



	//To make sure that the screen never turns off

	public static void ScreenNeverSleep() {

		Screen.sleepTimeout = SleepTimeout.NeverSleep;

	}

}


