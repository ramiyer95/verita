﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
            

    public void LoadPlayScreen() {
                

		SceneManager.LoadScene("play_scene");
                

	}

	public void LoadSettingsScreen() {

		SceneManager.LoadScene("settings_scene");

	}

	public void LoadLearnScreen() {

		SceneManager.LoadScene("learn_scene");

    }

	public void LoadStartScreen() {

		SceneManager.LoadScene("start_scene");

    }

	public void LoadTalentsScreen() {

		SceneManager.LoadScene("talentselection_scene");

    }


	public void QuitGame() {

		Application.Quit();

	}





    


}
