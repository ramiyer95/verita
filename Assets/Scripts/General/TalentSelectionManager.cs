﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TalentSelectionManager : MonoBehaviour {

	// n = normal
	// h = hidden

	public Sprite offSprite;
	public Sprite n_onSprite;
	public Sprite h_onSprite;

	public Button n_rookBishopSwap;
	public Button n_canonDefender;
	public Button n_fortify;
	public Button n_doubleMove;
	public Button n_sacrifice;
	public Button n_stonePieces;

	public Button h_rookBishopSwap;
	public Button h_canonDefender;
	public Button h_fortify;
	public Button h_doubleMove;
	public Button h_sacrifice;
	public Button h_stonePieces;

	// Using these arrays for the purpose of looping
	public Button[] hiddenButtons = new Button[6];
	public Button[] normalButtons = new Button[6];

	private Hashtable ability_hiddenButtons = new Hashtable();
	private Hashtable ability_normalButtons = new Hashtable();

	//Order of the following arrays is : 
	//  1. Rook Bishop Swap
	//  2. Canon Defender
	//  3. Fortify
	//  4. Double Move
	//  5. Sacrifice
	//  6. Stone Pieces


	public List<AbilityType> normalPicked = new List<AbilityType>();
	public AbilityType hiddenPicked;




	void Start() {

		ability_hiddenButtons[AbilityType.ROOKBISHOPSWAP] = h_rookBishopSwap;
		ability_hiddenButtons[AbilityType.CANONDEFENDER] = h_canonDefender;
		ability_hiddenButtons[AbilityType.FORTIFY] = h_fortify;
		ability_hiddenButtons[AbilityType.DOUBLEMOVE] = h_doubleMove;
		ability_hiddenButtons[AbilityType.SACRIFICE] = h_sacrifice;
		ability_hiddenButtons[AbilityType.STONEPIECES] = h_stonePieces;

		ability_normalButtons[AbilityType.ROOKBISHOPSWAP] = n_rookBishopSwap;
		ability_normalButtons[AbilityType.CANONDEFENDER] = n_canonDefender;
		ability_normalButtons[AbilityType.FORTIFY] = n_fortify;
		ability_normalButtons[AbilityType.DOUBLEMOVE] = n_doubleMove;
		ability_normalButtons[AbilityType.SACRIFICE] = n_sacrifice;
		ability_normalButtons[AbilityType.STONEPIECES] = n_stonePieces;


		n_rookBishopSwap.onClick.AddListener(() => OnButtonPressed(AbilityType.ROOKBISHOPSWAP, n_rookBishopSwap, false));
		n_canonDefender.onClick.AddListener(() => OnButtonPressed(AbilityType.CANONDEFENDER, n_canonDefender, false));
		n_fortify.onClick.AddListener( () => OnButtonPressed(AbilityType.FORTIFY, n_fortify, false));
		n_doubleMove.onClick.AddListener( () => OnButtonPressed(AbilityType.DOUBLEMOVE, n_doubleMove, false));
		n_sacrifice.onClick.AddListener( () => OnButtonPressed(AbilityType.SACRIFICE, n_sacrifice, false));
		n_stonePieces.onClick.AddListener(() => OnButtonPressed(AbilityType.STONEPIECES, n_stonePieces, false));

		h_rookBishopSwap.onClick.AddListener(delegate { OnButtonPressed(AbilityType.ROOKBISHOPSWAP, h_rookBishopSwap, true); });
		h_canonDefender.onClick.AddListener(delegate { OnButtonPressed(AbilityType.CANONDEFENDER, h_canonDefender, true); });
		h_fortify.onClick.AddListener(delegate { OnButtonPressed(AbilityType.FORTIFY, h_fortify, true); });
		h_doubleMove.onClick.AddListener(delegate { OnButtonPressed(AbilityType.DOUBLEMOVE, h_doubleMove, true); });
		h_sacrifice.onClick.AddListener(delegate { OnButtonPressed(AbilityType.SACRIFICE, h_sacrifice, true); });
		h_stonePieces.onClick.AddListener(delegate { OnButtonPressed(AbilityType.STONEPIECES, h_stonePieces, true); });


		// make sure that more than 2 items are not marked green
		// make sure that abilities are not repeated

		// ANY FISHY BUSINESS AND RESETTODEFAULT


		int numberOfHiddenAb = 0;
		int numberOfNormalAb = 0;



		if (PlayerPrefs.GetInt("h_rookBishopSwap") == 1) {

			numberOfHiddenAb++;

		} 

		if (PlayerPrefs.GetInt("h_canonDefender") == 1) {

			numberOfHiddenAb++;

		} 

		if (PlayerPrefs.GetInt("h_fortify") == 1) {

			numberOfHiddenAb++;

		} 

		if (PlayerPrefs.GetInt("h_doubleMove") == 1) {

			numberOfHiddenAb++;

		} 

		if (PlayerPrefs.GetInt("h_sacrifice") == 1) {

			numberOfHiddenAb++;

		} 

		if (PlayerPrefs.GetInt("h_stonePieces") == 1){

			numberOfHiddenAb++;

		}




		if (PlayerPrefs.GetInt("n_rookBishopSwap") == 1) {

			numberOfNormalAb++;

		} 

		if (PlayerPrefs.GetInt("n_canonDefender") == 1) {

			numberOfNormalAb++;

		} 

		if (PlayerPrefs.GetInt("n_fortify") == 1) {

			numberOfNormalAb++;

		} 

		if (PlayerPrefs.GetInt("n_doubleMove") == 1) {

			numberOfNormalAb++;

		} 

		if (PlayerPrefs.GetInt("n_sacrifice") == 1) {

			numberOfNormalAb++;

		} 

		if (PlayerPrefs.GetInt("n_stonePieces") == 1){

			numberOfNormalAb++;

		}




		if (!(numberOfHiddenAb == 1 && numberOfNormalAb == 2)) {

			ResetToDefaults();

			goto ResolveButtons;

		}


		if (PlayerPrefs.GetInt("h_rookBishopSwap") == 1) {

			hiddenPicked = AbilityType.ROOKBISHOPSWAP;

		} else if (PlayerPrefs.GetInt("h_canonDefender") == 1) {

			hiddenPicked = AbilityType.CANONDEFENDER;

		} else if (PlayerPrefs.GetInt("h_fortify") == 1) {

			hiddenPicked = AbilityType.FORTIFY;

		} else if (PlayerPrefs.GetInt("h_doubleMove") == 1) {

			hiddenPicked = AbilityType.DOUBLEMOVE;

		} else if (PlayerPrefs.GetInt("h_sacrifice") == 1) {

			hiddenPicked = AbilityType.SACRIFICE;

		} else if (PlayerPrefs.GetInt("h_stonePieces") == 1) {

			hiddenPicked = AbilityType.STONEPIECES;

		}



		if (PlayerPrefs.GetInt("n_rookBishopSwap") == 1) {

			if (hiddenPicked == AbilityType.ROOKBISHOPSWAP) {

				ResetToDefaults();

				goto ResolveButtons;
			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.ROOKBISHOPSWAP);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add( AbilityType.ROOKBISHOPSWAP);

			}

		}


		if (PlayerPrefs.GetInt("n_canonDefender") == 1) {

			if (hiddenPicked == AbilityType.CANONDEFENDER) {

				ResetToDefaults();

				goto ResolveButtons;
			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.CANONDEFENDER);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add(  AbilityType.CANONDEFENDER);

			}

		}

		if (PlayerPrefs.GetInt("n_fortify") == 1) {

			if (hiddenPicked == AbilityType.FORTIFY) {

				ResetToDefaults();

				goto ResolveButtons;

			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.FORTIFY);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add( AbilityType.FORTIFY);

			}

		}


		if (PlayerPrefs.GetInt("n_doubleMove") == 1) {

			if (hiddenPicked == AbilityType.DOUBLEMOVE) {

				ResetToDefaults();

				goto ResolveButtons;

			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.DOUBLEMOVE);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add( AbilityType.DOUBLEMOVE);

			}

		}


		if (PlayerPrefs.GetInt("n_sacrifice") == 1) {

			if (hiddenPicked == AbilityType.SACRIFICE) {

				ResetToDefaults();

				goto ResolveButtons;
			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.SACRIFICE);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add( AbilityType.SACRIFICE);

			}

		}


		if (PlayerPrefs.GetInt("n_stonePieces") == 1) {

			if (hiddenPicked == AbilityType.STONEPIECES) {

				ResetToDefaults();

				goto ResolveButtons;
			}

			if (normalPicked.Count == 0 ) {

				normalPicked.Add( AbilityType.STONEPIECES);

			} else if (normalPicked.Count == 1) {

				normalPicked.Add(  AbilityType.STONEPIECES);

			}

		}

		// Using goto so that the program jumps directly to resolving the buttons after it resets to defaults


		ResolveButtons :

		// Now that the abilities have been resolved, lets display them


		Button button1 = (Button) ability_hiddenButtons[hiddenPicked];
		Button button2 = (Button) ability_normalButtons[normalPicked[0]];
		Button button3 = (Button) ability_normalButtons[normalPicked[1]];

		// Assing the correct visuals and making the appropriate buttons non interactable
		button1.GetComponent<Image>().sprite = h_onSprite;
		button1.transform.parent.gameObject.GetComponentInChildren<Text>().color = Color.yellow;
		button1.interactable = false;
		button1.transform.parent.Find("normal_button").GetComponent<Button>().interactable = false;


		button2.GetComponent<Image>().sprite = n_onSprite;
		button2.transform.parent.gameObject.GetComponentInChildren<Text>().color = Color.green;
		button2.interactable = false;
		button2.transform.parent.Find("hidden_button").GetComponent<Button>().interactable = false;


		button3.GetComponent<Image>().sprite = n_onSprite;
		button3.transform.parent.gameObject.GetComponentInChildren<Text>().color = Color.green;
		button3.interactable = false;
		button3.transform.parent.Find("hidden_button").GetComponent<Button>().interactable = false;

	}



	public void OnButtonPressed ( AbilityType triggeringAbility, Button theButton, bool isHiddenButton) {

		if (isHiddenButton) {


			theButton.interactable = false;
			theButton.GetComponent<Image>().sprite = h_onSprite;
			theButton.transform.parent.gameObject.GetComponentInChildren<Text>().color = Color.yellow;
			theButton.transform.parent.Find("normal_button").GetComponent<Button>().interactable = false;


			Button theButtonThatTurns1 = (Button)ability_hiddenButtons[hiddenPicked];

			theButtonThatTurns1.interactable = true;
			theButtonThatTurns1.GetComponent<Image>().sprite = offSprite;
			theButtonThatTurns1.transform.parent.gameObject.GetComponentInChildren<Text>().color = Color.white;
			theButtonThatTurns1.transform.parent.Find("normal_button").GetComponent<Button>().interactable = true;

			hiddenPicked = triggeringAbility;

			return;

		}


		// in case of normal Button


		// The Button system works first in first out.
		// The Button that was triggered first will be replaced when a new Button is selected.

		Button theButtonThatTurns = (Button) ability_normalButtons[normalPicked[1]] ; 

		theButtonThatTurns.interactable = true;
		theButtonThatTurns.GetComponent<Image>().sprite = offSprite;
		theButtonThatTurns.transform.parent.gameObject.GetComponentInChildren<Text>().color = Color.white;


		//bringing the Button at 0 to 1
		normalPicked[1] = normalPicked[0];


		//replacing Button at 0
		normalPicked[0] = triggeringAbility;

		theButton.interactable = false;
		theButton.GetComponent<Image>().sprite = n_onSprite;
		theButton.transform.parent.GetComponentInChildren<Text>().color = Color.green;


		theButton.transform.parent.Find("hidden_button").GetComponent<Button>().interactable = false;
		theButtonThatTurns.transform.parent.Find("hidden_button").GetComponent<Button>().interactable = true;

	}




	public void UpdateArrays() {

		// before this function is called, data must be transfered from the Button objects to the arrays

		// The following is to update the ability arrays depending on what is picked.

		// For the hidden abilities, their display need not be changed because that aspect is controlled by Button group 




		if (hiddenPicked == AbilityType.ROOKBISHOPSWAP) {

			PlayerPrefs.SetInt("h_rookBishopSwap" , 1);

		} else {

			PlayerPrefs.SetInt("h_rookBishopSwap" , 0);

		}

		if (hiddenPicked == AbilityType.CANONDEFENDER) {

			PlayerPrefs.SetInt("h_canonDefender" , 1);

		} else {

			PlayerPrefs.SetInt("h_canonDefender" , 0);

		}

		if (hiddenPicked == AbilityType.FORTIFY) {

			PlayerPrefs.SetInt("h_fortify" , 1);

		} else {

			PlayerPrefs.SetInt("h_fortify" , 0);

		}

		if (hiddenPicked == AbilityType.DOUBLEMOVE) {

			PlayerPrefs.SetInt("h_doubleMove" , 1);

		} else {

			PlayerPrefs.SetInt("h_doubleMove" , 0);

		}

		if (hiddenPicked == AbilityType.SACRIFICE) {

			PlayerPrefs.SetInt("h_sacrifice" , 1);

		} else {

			PlayerPrefs.SetInt("h_sacrifice" , 0);

		}




		// For the normal abilities, the visuals need to be changed 

		int pos_rookBishopSwapInArray;
		int pos_canonDefenderInArray;
		int pos_fortifyInArray;
		int pos_doubleMoveInArray;
		int pos_sacrificeInArray;
		int pos_stonePiecesInArray;

		pos_rookBishopSwapInArray = normalPicked.IndexOf(AbilityType.ROOKBISHOPSWAP);
		pos_canonDefenderInArray = normalPicked.IndexOf(AbilityType.CANONDEFENDER);
		pos_fortifyInArray = normalPicked.IndexOf(AbilityType.FORTIFY);
		pos_doubleMoveInArray = normalPicked.IndexOf(AbilityType.DOUBLEMOVE);
		pos_sacrificeInArray = normalPicked.IndexOf(AbilityType.SACRIFICE);
		pos_stonePiecesInArray = normalPicked.IndexOf(AbilityType.STONEPIECES);



		if (pos_rookBishopSwapInArray > -1) {

			PlayerPrefs.SetInt("n_rookBishopSwap", 1);

		} else {

			PlayerPrefs.SetInt("n_rookBishopSwap", 0);

		}



		if (pos_canonDefenderInArray > -1) {

			PlayerPrefs.SetInt("n_canonDefender", 1);

		} else {

			PlayerPrefs.SetInt("n_canonDefender", 0);

		}



		if (pos_fortifyInArray > -1) {

			PlayerPrefs.SetInt("n_fortify", 1);

		} else {

			PlayerPrefs.SetInt("n_fortify", 0);

		}


		if (pos_doubleMoveInArray > -1) {

			PlayerPrefs.SetInt("n_doubleMove", 1);

		} else {

			PlayerPrefs.SetInt("n_doubleMove", 0);

		}



		if (pos_sacrificeInArray > -1) {

			PlayerPrefs.SetInt("n_sacrifice", 1);

		} else {

			PlayerPrefs.SetInt("n_sacrifice", 0);

		}



		if (pos_stonePiecesInArray> -1) {

			PlayerPrefs.SetInt("n_stonePieces", 1);

		} else {

			PlayerPrefs.SetInt("n_stonePieces", 0);

		}


		// Flag to check if player has ever visited the Talent Selection part before
		// If this flag is 9, always resolve to the default abilities
		PlayerPrefs.SetInt("HasSaved", 1);

		PlayerPrefs.Save();

		Debug.Log("Ran save");

	}    


	private void ResetToDefaults() {

		normalPicked.Add(AbilityType.CANONDEFENDER);
		normalPicked.Add( AbilityType.STONEPIECES);

		hiddenPicked = AbilityType.SACRIFICE;

	}



}
