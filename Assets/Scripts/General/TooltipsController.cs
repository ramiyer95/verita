﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipsController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public Team team;


	//when using OnPointerDown/Up , these parent classes have to be used AND the methods need to be private

	public void OnPointerDown(PointerEventData eventData) {


		foreach ( PlayerController pc in GameObject.FindObjectsOfType<PlayerController>()) {

			if (pc.clientTeam == team) {

				foreach (Transform t in pc.transform.Find("Abilities UI").transform) {

					if (t.Find("abilityInfoPanel") != null) {

						t.Find("abilityInfoPanel").gameObject.SetActive(true);

					}

				}

			} else {

				//Just to make sure that if someone taps both the buttons together, it doesnt give bad results

				foreach (Transform t in pc.transform.Find("Abilities UI").transform) {

					if (t.Find("abilityInfoPanel") != null) {

						t.Find("abilityInfoPanel").gameObject.SetActive(false);

					}

				}

			}

		}


	}



	public void OnPointerUp (PointerEventData eventData) {

		foreach ( PlayerController pc in GameObject.FindObjectsOfType<PlayerController>()) {

			if (pc.clientTeam == team) {

				foreach (Transform t in pc.transform.Find("Abilities UI").transform) {

					if (t.Find("abilityInfoPanel") != null) {

						t.Find("abilityInfoPanel").gameObject.SetActive(false);

					}

				}
			}

		}

	}

}
