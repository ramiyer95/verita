﻿using System;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour {

    [Range(0f, 1f)]
    public float userVolume;

    [Range(0f, 1f)]
    public float dampeningFactor;

    public static AudioManager instance = null;

    public Sound[] sounds;
        


    private void Awake()
    {
                        
        if (instance!= null)
        {

            Destroy(this.gameObject);

        } else
        {

            instance = this;

            GameObject.DontDestroyOnLoad(this.gameObject);

        }


        foreach (Sound s in sounds)
        {

            s.source = this.gameObject.transform.Find("audioSources").gameObject.AddComponent<AudioSource>();

            s.source.playOnAwake = false;

            s.source.clip = s.clip;

            s.source.volume = s.volume * userVolume;

            if (s.name == "background_audio")
            {

                s.source.loop = true;

            }

        }

        SceneManager.sceneLoaded += AddTapAudioToButtonsOnSceneLoad;

    }


    private void AddTapAudioToButtonsOnSceneLoad(Scene scene, LoadSceneMode loadSceneMode)
    {

        foreach(Button b in GameObject.FindObjectsOfType<Button>())
        {

            b.onClick.AddListener(() => PlaySound("tap_audio"));

        }

        if(scene.name == "win_scene")
        {
            if(IsBackgroundPlaying())
            {

                StopPlaying("background_audio");

            }

            PlaySound("win_audio");

        }

        if (scene.name == "defeat_scene")
        {
            if (IsBackgroundPlaying())
            {

                StopPlaying("background_audio");

            }


            PlaySound("lose_audio");
                       

        }

        if (scene.name == "disconnect_scene")
        {
            if (IsBackgroundPlaying())
            {

                StopPlaying("background_audio");

            }


            PlaySound("disconnect_audio");
                        
        }

        if (IsBackgroundPlaying() && !(scene.name == "lose_scene") && !(scene.name == "win_scene") && !(scene.name == "disconnect_scene"))
        {

            PlaySound("background_audio");

        }

    }



    private void Start()
    {
        
        if (!IsBackgroundPlaying())
        {

            PlaySound("background_audio");

        }

        

    }


    public void PlaySound(string soundName)
    {

        Sound s2 = Array.Find(sounds, s1 => s1.name == soundName);

        s2.source.Play();

        //Update volumes at the start of the clip
        VolumeLayerUpdater();

        //Update volumes right after the clip has ended
        Invoke("VolumeLayerUpdater", s2.clip.length + 0.05f);
        
    }

    public void VolumeLayerUpdater()
    {
        List<AudioLayer> activeLayers = new List<AudioLayer>();

        bool abilitiesLayer = false;
        bool indicationsLayer = false;
        bool generalLayer = false;
        bool backgroundLayer = false;


        foreach (Sound s in sounds)
        {

            if (s.source.isPlaying)
            {

                if (s.layer == AudioLayer.BACKGROUND)
                {

                    backgroundLayer = true;

                }
                else if (s.layer == AudioLayer.GENERAL)
                {

                    generalLayer = true;

                }
                else if (s.layer == AudioLayer.INDICATIONS)
                {

                    indicationsLayer = true;

                }
                else if (s.layer == AudioLayer.ABILITIES)
                {

                    abilitiesLayer = true;

                }


            }

        }


        if (backgroundLayer)
        {

            activeLayers.Add(AudioLayer.BACKGROUND);

        }


        if (generalLayer)
        {

            activeLayers.Add(AudioLayer.GENERAL);

        }

        if (indicationsLayer)
        {

            activeLayers.Add(AudioLayer.INDICATIONS);

        }

        if (abilitiesLayer)
        {

            activeLayers.Add(AudioLayer.ABILITIES);

        }


        //ADJUST LAYER VOLUMES


        int i = 0;

        foreach (AudioLayer al in activeLayers)
        {

            SetLayerVolumes(al, (float)(userVolume * Math.Pow(dampeningFactor, i)));

            i++;

        }
    }


    // This is a little inefficient because it is run the same number of times as the number of active layers
    //  However, doing this another way would require creating additional variables.

    // If in the future, the number of layers remains the same but the number of audio clips increases by a 
    //   significant margin, then it would probably be a good idea to re-implement the same with addiotional vars.
    // However, if the case is that the number of layers is increasing faster than the number of clips, then this
    //   implementation is better because we would require a greater number of additional vars.

    private void SetLayerVolumes(AudioLayer al, float volumeCalculated)
    {

        foreach(Sound s in sounds)
        {

            if (s.source.isPlaying && s.layer == al)
            {

                s.source.volume = volumeCalculated;

            }

        }
        

    }




    public bool IsBackgroundPlaying()
    {

        foreach(Sound s in sounds)
        {

            if (s.source.isPlaying && s.name == "background_audio")
            {

                return true;

            }

        }

        return false;

    }

    private void StopPlaying (string soundName)
    {

        foreach(Sound s in sounds)
        {

            if (s.name == "soundsName")
            {

                s.source.Stop();

            }

        }

    }


}
