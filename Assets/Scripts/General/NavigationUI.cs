﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationUI : MonoBehaviour {

	public GameObject quitMenu;

	public void ActivateQuitMenu() {

		quitMenu.SetActive(true);

	}


	public void DeactivateQuitMenu() {

		quitMenu.SetActive(false);

	}

}
