﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LearnUIManager : MonoBehaviour {

	public GameObject MenuCanvas;

	public GameObject TalentsCanvas;

	public GameObject MapNPiecesCanvas;

	public GameObject GettingStartCanvas;


	public void ActivateMenuCanvas () {

		MenuCanvas.SetActive(true);

		TalentsCanvas.SetActive(false);

		MapNPiecesCanvas.SetActive(false);

		GettingStartCanvas.SetActive(false);


	}


	public void ActivateTalentsCanvas() {

		MenuCanvas.SetActive(false);

		TalentsCanvas.SetActive(true);

		MapNPiecesCanvas.SetActive(false);

		GettingStartCanvas.SetActive(false);

		int activeChildren = 0;

		for(int i=0; i<TalentsCanvas.transform.childCount; i++) {

			GameObject go = TalentsCanvas.transform.GetChild(i).gameObject;

			if (go.activeSelf) {

				activeChildren++;

			}

		}


		// 2 because the back button will always be active
		if (activeChildren != 2) {

			TalentsCanvas.transform.GetChild(0).gameObject.SetActive(true);

		}		

	}


	public void ActivateMapNPiecesCanvas() {

		MenuCanvas.SetActive(false);

		TalentsCanvas.SetActive(false);

		MapNPiecesCanvas.SetActive(true);

		GettingStartCanvas.SetActive(false);

		int activeChildren = 0;

		for(int i=0; i<MapNPiecesCanvas.transform.childCount; i++) {

			GameObject go = MapNPiecesCanvas.transform.GetChild(i).gameObject;

			if (go.activeSelf) {

				activeChildren++;

			}

		}

		if (activeChildren != 2) {

			MapNPiecesCanvas.transform.GetChild(0).gameObject.SetActive(true);

		}	

	}


	public void ActivateGettingStartCanvas() {

		MenuCanvas.SetActive(false);

		TalentsCanvas.SetActive(false);

		MapNPiecesCanvas.SetActive(false);

		GettingStartCanvas.SetActive(true);

	}


	public void NavigateInTalentsCanvas ( int pageNumber ) {

		for (int i =0; i<TalentsCanvas.transform.childCount ; i++) {

			if (TalentsCanvas.transform.GetChild(i).gameObject.name == "BackButton") {

				break;

			}

			TalentsCanvas.transform.GetChild(i).gameObject.SetActive(false);

		}

		TalentsCanvas.transform.Find("Page" + pageNumber.ToString()).gameObject.SetActive(true);

	}


	public void NavigateMapNPiecesCanvas ( int pageNumber ) {

		for (int i =0; i<MapNPiecesCanvas.transform.childCount ; i++) {

			if (MapNPiecesCanvas.transform.GetChild(i).gameObject.name == "BackButton") {

				break;

			}

			MapNPiecesCanvas.transform.GetChild(i).gameObject.SetActive(false);

		}

		MapNPiecesCanvas.transform.Find("Page" + pageNumber.ToString()).gameObject.SetActive(true);

	}




}
