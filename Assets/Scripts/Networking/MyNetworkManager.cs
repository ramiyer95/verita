﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Networking;
using UnityEngine.Networking.Match;

using UnityEngine.SceneManagement;


public class MyNetworkManager : Photon.MonoBehaviour {


    public GameObject quitMenu;

    //Photon method
	void OnDisconnectedFromPhoton()
	{

        if (GlobalFunctions.theGameState != GameState.NOTSTARTED)
        {
            SceneManager.LoadScene(SceneToLoadOnDisconnect()); 
        }
	}


	public void DisconnectMeFromServer() {
        
        //Player who initiates disconenct manually is the one who loses the game. 

        foreach (PlayerController pc in GameObject.FindObjectsOfType<PlayerController>())
        {

            PhotonView pv = pc.gameObject.GetComponent<PhotonView>();

            if(pv.isMine)
            {
                pv.RPC("PRPC_ResolveGameState", PhotonTargets.All, GlobalFunctions.ResolveGameStatetoID(GameState.LOST));

            } else
            {

                pv.RPC("PRPC_ResolveGameState", PhotonTargets.All, GlobalFunctions.ResolveGameStatetoID(GameState.WON));

            }

            
            pv.RPC("PRPC_ExecuteDisconnect", PhotonTargets.AllViaServer);

        }
        
                
    }
        

    private string SceneToLoadOnDisconnect()
    {
        if (GlobalFunctions.theGameState == GameState.PLAY)
        {

            return "disconnect_scene";

        } else if (GlobalFunctions.theGameState == GameState.WON)
        {

            return "win_scene";

        } else if (GlobalFunctions.theGameState == GameState.LOST)
        {

            return "defeat_scene";

        }

        return "";

    }



    //PlayerTTL is set to 5000ms. This means that 5000ms after a player disconnects, he is gone from the room. 
    //At this point, this update function captures that, and calls a win on the other client. 
    //This is to ensure that the other player is not stuck in LIMBO after a player disconnects.
    //In the future, this can be used to implement the RECONNECTION feature (using the IsActive property of PhotonPlayers
    //  which works on similar lines)

    private void Update()
    {
        
        if(GlobalFunctions.theGameState == GameState.PLAY)
        {

            if (PhotonNetwork.playerList.Length != 2)
            {

                Debug.Log("Need to disconnect since there is only 1 player left");

                GlobalFunctions.theGameState = GameState.WON;

                PhotonNetwork.Disconnect();

            }
            

            if(Input.GetKeyDown(KeyCode.Escape))
            {

                quitMenu.SetActive(true);

            }

        }

    }


}
