﻿using UnityEngine;
using System.Security.Cryptography;
using System.Text;
using UnityEngine.Networking.Match;
using UnityEngine.UI;
using System;

public class MyNMHUD : MonoBehaviour {

	MyNetworkManager networkManager;

    [SerializeField]
	Text connectingText;

	[SerializeField]
	GameObject joinMatchButton;

	[SerializeField]
	GameObject createMatchButton;

	[SerializeField]
	GameObject backToMainMenuButton;

	[SerializeField]
	GameObject backToCreateJoinButton;

	[SerializeField]
	GameObject joinMatchKeyInput;

	[SerializeField]
	GameObject matchKeyDisplay;

    [SerializeField]
    GameObject locationPreferenceDropdown;

    [SerializeField]
    GameObject pingText;

    [SerializeField]
    GameObject loadingSpiral;

    string matchNameString;

    int currentPing;

    bool creatingMatch = false;


    void Start() {

        OnLandingAtCreateJoinScreen();

        PhotonNetwork.autoJoinLobby = false;

        GlobalFunctions.theGameState = GameState.NOTSTARTED;

    }


	public void OnClickCreateMatch() {

        // generate random string 

        // check if a match with the same name already exists

        // if not then create match with the name

        // if yes then go back to the first step

        createMatchButton.SetActive(false);

        joinMatchButton.SetActive(false);

        backToMainMenuButton.SetActive(false);

        locationPreferenceDropdown.SetActive(false);

        loadingSpiral.SetActive(true);

        creatingMatch = true;

        switch (locationPreferenceDropdown.GetComponent<Dropdown>().value)
        {
            case 0:

                //Australia
                PhotonNetwork.ConnectToRegion(CloudRegionCode.au, VersionHelper.VERSIONNUMBER);
                break;

            case 1:

                //Europe
                PhotonNetwork.ConnectToRegion(CloudRegionCode.eu, VersionHelper.VERSIONNUMBER);
                break;

            case 2:

                //India
                PhotonNetwork.ConnectToRegion(CloudRegionCode.@in, VersionHelper.VERSIONNUMBER);
                break;

            case 3:

                //Japan
                PhotonNetwork.ConnectToRegion(CloudRegionCode.jp, VersionHelper.VERSIONNUMBER);
                break;

            case 4:

                //Russia
                PhotonNetwork.ConnectToRegion(CloudRegionCode.ru, VersionHelper.VERSIONNUMBER);
                break;

            case 5:

                //Singapore
                PhotonNetwork.ConnectToRegion(CloudRegionCode.asia, VersionHelper.VERSIONNUMBER);
                break;

            case 6:

                //South America
                PhotonNetwork.ConnectToRegion(CloudRegionCode.sa, VersionHelper.VERSIONNUMBER);
                break;

            case 7:

                //US East
                PhotonNetwork.ConnectToRegion(CloudRegionCode.us, VersionHelper.VERSIONNUMBER);
                break;

            case 8:

                //US West
                PhotonNetwork.ConnectToRegion(CloudRegionCode.usw, VersionHelper.VERSIONNUMBER);
                break;
        }

        //wait for the ping to come and then continue with executing the rest of the program
        PlayerPrefs.SetInt("Region", locationPreferenceDropdown.GetComponent<Dropdown>().value);
                      
        
	}



    void OnCreatedRoom ()
    {

        loadingSpiral.SetActive(false);

        backToCreateJoinButton.SetActive(true);

        matchKeyDisplay.SetActive(true);

        matchKeyDisplay.transform.Find("theKey").GetComponent<Text>().text = matchNameString.Substring(matchNameString.Length - 4);

        pingText.SetActive(true);

        currentPing = PhotonNetwork.GetPing();

        pingText.GetComponent<Text>().text = "PING : " + currentPing.ToString() + " ms";

        SetPingTextColor();

    }


    void OnPhotonCreateRoomFailed()
    {

        OnClickCreateMatch();

    }




    public void OnLandingAtCreateJoinScreen()
    {        

        locationPreferenceDropdown.GetComponent<Dropdown>().value = PlayerPrefs.GetInt("Region");
     
    }


    //photon method
    private void OnConnectedToMaster()
    {
        Debug.Log("connected to master");

        if (creatingMatch)
        {
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = Convert.ToByte(2);
            roomOptions.PlayerTtl = 5000;
            roomOptions.EmptyRoomTtl = 60000;
            roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable() { { "version", VersionHelper.VERSIONNUMBER } };


            matchNameString = GenerateUniqueKey(4);

            matchNameString = "MIDASPA_VERITA_MATCH_" + matchNameString;


            //Dont want to worry too much about lobbies at this point since it does not matter (hence using default)

            PhotonNetwork.CreateRoom(matchNameString, roomOptions, TypedLobby.Default);

        } else
        {

            loadingSpiral.SetActive(false);

            joinMatchKeyInput.SetActive(true);

            backToCreateJoinButton.SetActive(true);

            
        }




    }


    //photon method
    private void OnDisconnectedFromPhoton()
    {

        

    }




    public void OnClickJoinMatch() {

        
        createMatchButton.SetActive(false);

        joinMatchButton.SetActive(false);

        backToMainMenuButton.SetActive(false);

        locationPreferenceDropdown.SetActive(false);

        loadingSpiral.SetActive(true);

        creatingMatch = false;

        switch (locationPreferenceDropdown.GetComponent<Dropdown>().value)
        {
            case 0:

                //Australia
                PhotonNetwork.ConnectToRegion(CloudRegionCode.au, VersionHelper.VERSIONNUMBER);
                break;

            case 1:

                //Europe
                PhotonNetwork.ConnectToRegion(CloudRegionCode.eu, VersionHelper.VERSIONNUMBER);
                break;

            case 2:

                //India
                PhotonNetwork.ConnectToRegion(CloudRegionCode.@in, VersionHelper.VERSIONNUMBER);
                break;

            case 3:

                //Japan
                PhotonNetwork.ConnectToRegion(CloudRegionCode.jp, VersionHelper.VERSIONNUMBER);
                break;

            case 4:

                //Russia
                PhotonNetwork.ConnectToRegion(CloudRegionCode.ru, VersionHelper.VERSIONNUMBER);
                break;

            case 5:

                //Singapore
                PhotonNetwork.ConnectToRegion(CloudRegionCode.asia, VersionHelper.VERSIONNUMBER);
                break;

            case 6:

                //South America
                PhotonNetwork.ConnectToRegion(CloudRegionCode.sa, VersionHelper.VERSIONNUMBER);
                break;

            case 7:

                //US East
                PhotonNetwork.ConnectToRegion(CloudRegionCode.us, VersionHelper.VERSIONNUMBER);
                break;

            case 8:

                //US West
                PhotonNetwork.ConnectToRegion(CloudRegionCode.usw, VersionHelper.VERSIONNUMBER);
                break;
        }

        
        PlayerPrefs.SetInt("Region", locationPreferenceDropdown.GetComponent<Dropdown>().value);




    }



	public void OnValueChangeJoinInput() {

        joinMatchKeyInput.transform.Find("ErrorText").gameObject.SetActive(false);

        string matchName = joinMatchKeyInput.transform.Find("InputText").GetComponent<Text>().text;

		 // we dont want to anything to happen unless the minimum text length is met.
		 if (matchName.Length < 4) {

		 	return;

		 }

		 //now, need to convert the entire string to uppercase

		 matchName = matchName.ToUpper();

         matchName = "MIDASPA_VERITA_MATCH_" + matchName;

         PhotonNetwork.JoinRoom(matchName);
        
	}


    //photon method
    private void OnPhotonJoinRoomFailed()
    {

        joinMatchKeyInput.transform.Find("ErrorText").gameObject.SetActive(true);

    }


    //photon method
    private void OnJoinedRoom ()
    {

        //in Unet, overridable functions OnMatchJoined and OnServerConnect were both called. This was because
        //  OnMatchJoined was called ONLY on the client, and the OnServerConnect was called ONLY on the MasterClient
        //  Additionally, the number of players in the room were kept track of using a variable (clientsConnected)
        //  None of this needs to be done in Photon.


        if (PhotonNetwork.playerList.Length == 2)
        {
            Debug.Log("reached here");
            this.gameObject.transform.Find("NetworkManagerUI").gameObject.SetActive(false);
            this.gameObject.transform.Find("LoadScreenAfterJoin").gameObject.SetActive(true);
        }

        PhotonNetwork.Instantiate("Player Controller UI 1 1", new Vector3(0, 0, 0), Quaternion.identity, Convert.ToByte(0));

    }

    private void OnPhotonPlayerConnected(PhotonPlayer player)
    {

        if (PhotonNetwork.playerList.Length == 2)
        {
            
            this.gameObject.transform.Find("NetworkManagerUI").gameObject.SetActive(false);
            this.gameObject.transform.Find("LoadScreenAfterJoin").gameObject.SetActive(true);
        }

    }





	public void OnClickBackToCreateJoin ( ) {

        // if person goes back while the error is showing, he will see it at the start when he comes back
        joinMatchKeyInput.transform.Find("ErrorText").gameObject.SetActive(false);

        PhotonNetwork.Disconnect();


		createMatchButton.SetActive(true);

		joinMatchButton.SetActive(true);

        locationPreferenceDropdown.SetActive(true);

		backToMainMenuButton.SetActive(true);

		matchKeyDisplay.SetActive(false);

		joinMatchKeyInput.SetActive(false);

		backToCreateJoinButton.SetActive(false);

        pingText.SetActive(false);

	}







	private string GenerateUniqueKey (int size = 8) {

		// by default the size is going to be 8. 

		char[] chars = new char[36];

		//chars has the array of characters to generate the random string from.
		chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();


		//Important to note that we have 36 characters but a byte can take 256 distinct values. This means that there will be
		// a higher probability of some characters being picked over others, resulting in a bias that defeats true randomness.

		// Hence we can only pick bytes that take a value from 0-252 (36*7 = 252) to ensure that each character has an 
		//   equal probability of being picked.


		// we will fill up this byte array with truly random bytes (byte is unsigned 8-bit integer from 0-255)
		byte[] randomBytesInRange = new byte[size];

		// we will store the output of RNGCryptoServiceProvider in this byte array
		byte[] theRandomByte ;

		// the using statement is effective if you want to free up memory after execution of a particular code bit. 
		RNGCryptoServiceProvider theCryptoService = new RNGCryptoServiceProvider();

		for (int i = 0; i < (size - 1); i++) {

			theRandomByte = new byte[1];

			GenerateRandomByte :

			theCryptoService.GetBytes(theRandomByte);

			if (theRandomByte[0] <= 252) {

				randomBytesInRange[i] = theRandomByte[0];

			} else {

				goto GenerateRandomByte;

			}

		}


		StringBuilder result = new StringBuilder();

		foreach (byte theByte in randomBytesInRange) {

			result.Append(chars[theByte % (chars.Length)]);

		}

		return result.ToString();

	}



    private void SetPingTextColor()
    {
        //setting the color as a function of the ping
        if (currentPing < 150)
        {

            int red = (int)(currentPing / 150) * 255;
            int green = 255;
            int blue = 0;

            pingText.GetComponent<Text>().color = new Color32(Convert.ToByte(red), Convert.ToByte(green), Convert.ToByte(blue), Convert.ToByte(255));

        }
        else
        {

            int red = 255;
            float tempGreen = 255 - (((currentPing - 150) / 200) * 255);
            tempGreen = Mathf.Clamp(tempGreen, 0f, 255f);

            int green = (int)tempGreen;

            int blue = 0;

            pingText.GetComponent<Text>().color = new Color32(Convert.ToByte(red), Convert.ToByte(green), Convert.ToByte(blue), Convert.ToByte(255));

        }
    }



    private void Update()
    {
        
        if(GlobalFunctions.theGameState ==  GameState.NOTSTARTED)
        {

            if(backToMainMenuButton.activeInHierarchy)
            {

                if(Input.GetKeyDown(KeyCode.Escape))
                {

                    GameObject.FindObjectOfType<LevelManager>().LoadStartScreen();

                }

            }

            if(backToCreateJoinButton.activeInHierarchy)
            {

                if(Input.GetKeyDown(KeyCode.Escape))
                {

                    OnClickBackToCreateJoin();

                }

            }

        }

    }


}
