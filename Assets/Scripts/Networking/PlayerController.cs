﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerController : Photon.MonoBehaviour {

    public Board board;

    public GameObject boardPrefab;

    private BoardHighlighting highlighter;

    public bool interactingWithAbilityUI = false;

    // Need a variable that specifies which team this client controls. 

    public Team clientTeam;

    public GameObject specialAbilitiesManager;

    public TurnManager turnManager;

    private AbilityPrefs abilityPref;

    private LevelManager levelManager;

    

    void Start() {

        board = GameObject.Find("Board").GetComponent<Board>();


        // The following functions use board and networkIdentity hence need to assign them here.
        // Classes not relevant to the following two functions can be assigned in the Start() function.

        photonView.RPC("PRPC_InformServerMyPhotonView", PhotonTargets.MasterClient);


        // This function calls the equivalent of the Start function on the board. 
        photonView.RPC("PRPC_RunBoardStartFunc", PhotonTargets.MasterClient);


        if (!photonView.isMine) {

            this.transform.Find("BoardUI").GetComponent<BoxCollider2D>().enabled = false;

        }

        highlighter = GameObject.Find("Board").GetComponent<BoardHighlighting>();

        specialAbilitiesManager = GameObject.Find("SpecialAbilitiesManager");

        turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();

        abilityPref = this.gameObject.transform.Find("Abilities UI").GetComponent<AbilityPrefs>();

    }


    // *********** BOARD INPUTS **************** 
    // responsible for sending the inputs from the player controller to the board on the server.
    // corresponding player object on the server runs the function called inside this method.
    [PunRPC]

    public void PRPC_SendInputsToServer(Vector2 pos) {

        board.SelectVeritaMan(pos, photonView.viewID);

    }



    [PunRPC]

    private void PRPC_RunBoardStartFunc() {

        if (!PhotonNetwork.isMasterClient || !photonView.isMine)
        {
            return;
        }

		board.WhenAllPlayersConnect();
                
	}


	[PunRPC]

	private void PRPC_InformServerMyPhotonView() {

		if (board.playerOnePhotonView == null) {

			board.playerOnePhotonView = photonView;

            Debug.Log("PlayerOnePhotonView assigned");

			return;

		}

		if (board.playerTwoPhotonView == null) {

			board.playerTwoPhotonView = photonView;

            Debug.Log("PlayerTwoPhotonView assigned");

			return;
		}

	}


    [PunRPC]

    public void PRPC_ResolveGameState(int gameStateID)
    {

        if (photonView.isMine)
        {
            GlobalFunctions.theGameState = GlobalFunctions.ResolveIDtoGameState(gameStateID);

        }
    }



	[PunRPC]

	public void PRPC_ResolveTeam(bool isThisTeamBlue) {

		// Since syncvars can only transmit basic types, we use this function to update the IMPORTANT clientTeam after the
		// syncvar updates.

		if (isThisTeamBlue) {

			clientTeam = Team.BLUE;

            if (photonView.isMine)
            {
                board.transform.Find("TeamIndicator/B").gameObject.SetActive(true);

            }

		} else {

			clientTeam = Team.RED;

            if (photonView.isMine)
            {
                board.transform.Find("TeamIndicator/R").gameObject.SetActive(true);
            }
        }

    }


	[PunRPC] 

	public void PRPC_AssignTimerColor (bool isThisBlue) {

		if (photonView.isMine) {

			if (isThisBlue) {

				turnManager.blueSlider.transform.Find("Fill_Image").gameObject.GetComponent<Image>().color = Color.green;

				turnManager.redSlider.transform.Find("Fill_Image").gameObject.GetComponent<Image>().color = Color.gray;

			} else {

				turnManager.blueSlider.transform.Find("Fill_Image").gameObject.GetComponent<Image>().color = Color.gray;

				turnManager.redSlider.transform.Find("Fill_Image").gameObject.GetComponent<Image>().color = Color.green;

			}

		}


	}



	//Need a function to call the highlighters on the client side. 

	[PunRPC]

	public void PRPC_Highlight(byte[] serializedMovesArray) {

        if (photonView.isMine)
        {
            bool[,] movesArray = GlobalFunctions.DeserializeToObject<bool[,]>(serializedMovesArray);

            highlighter.HighlightAllowedMoves(movesArray);
        }
	}


	[PunRPC]

	public void PRPC_HideHighlight() {

		highlighter.HideHighlights();

	}


	// Need to call the functions for the StartOnBoardLoad function of all the abilities
	// Since at start(), board is inactive, it cannot be 'found' in the hierarchy
	// Thus calling this method manually to load the starting properties of the Abilities class.

	
    [PunRPC]
	public void PRPC_RunStartFuncOfUtilities() {

		if (!photonView.isMine) {

			return;

		}

        
        //Inside this loop, only the StartOnBoardLoad function of the Abilities class gets called. Thus, it is 
        //  not enough to only call this, especially for Fortify where there is an additional assignment in the
        //  'new' StartOnBoardLoad method inside the derived class. Hence, doing this. The additional complexity 
        //  or load on the computer is ONLY because of the extra GameObject.FindObjectOfType, since the method of
        //  the derived class does not call the method on the base class. 
		foreach (Abilities ability in GameObject.FindObjectsOfType<Abilities>()) {

            ability.StartOnBoardLoad();

		}

        GameObject.FindObjectOfType<Fortify>().StartOnBoardLoad();



        // Call the ability prefs StartOnBoardLoad. This informs the server about the player prefs from each player.
        // Sending of information to the server needs to be done after the board has loaded so that the playerControllers know
        //  which team they belong to. Calling the following function on Start() causes a bug where both playerController think that
        //  they are the (default) BLUE team. 

        AbilityPrefs abilityPrefs = this.GetComponentInChildren<AbilityPrefs>();

        abilityPrefs.ExtractAbilityPrefsFromMemory();

        int h = GlobalFunctions.ResolveAbilityToID(abilityPrefs.hiddenPicked);
        int n1 = GlobalFunctions.ResolveAbilityToID(abilityPrefs.normalPicked[0]);
        int n2 = GlobalFunctions.ResolveAbilityToID(abilityPrefs.normalPicked[1]);

        photonView.RPC("PRPC_InformServerMyAbilities", PhotonTargets.MasterClient, h, n1, n2);


        //Lets call the start function of the TurnManger here
        //Since the turnmanager starts the timer, it is a good idea to start it at the end. 
        turnManager.StartOnBoardLoad();


        //At this point, all the prerequisites of the board are loaded and the game is ready to accept inputs and
        //  begin play. 
        //The screen should be disabled till this point. 

        photonView.RPC("PRPC_DeactivateLoadScreen", PhotonTargets.AllViaServer);


    }                           

    
    [PunRPC]

    private void PRPC_DeactivateLoadScreen()
    {

        GameObject.Find("Network Manager").transform.Find("LoadScreenAfterJoin").gameObject.SetActive(false);

    }



	// **************** ABILITY INPUTS ******************
	// Following RPCs are responsible for sending ability inputs to the server and executing the ExecutingAbility
	//   functions of the server.
    // ** FOLLOWING RPCs SHOULD ONLY RUN ON THE SERVER.

	[PunRPC]

	public void PRPC_InformServerMyAbilities( int hiddenAbility, int normalAbilityOne, int normalAbilityTwo) {


		// Feed data into the tracker component of the specialAbilitiesManager on the server end

		specialAbilitiesManager.GetComponent<Tracker>().ResolveAbilities(clientTeam, hiddenAbility, normalAbilityOne, normalAbilityTwo);

        // Call a function on the client end to spawn the relevant buttons on the client

        photonView.RPC("PRPC_SpawnButtonsOnYourScreen", PhotonTargets.All);

        photonView.RPC("PRPC_SpawnButtonsOnOpponentScreen", PhotonTargets.All, normalAbilityOne, normalAbilityTwo);

	}


	[PunRPC]

	public void PRPC_ExecuteRookBishopSwap() {

        //called only on the server/masterclient

		specialAbilitiesManager.GetComponent<RookBishopSwap>().ExecuteAbility(photonView.viewID);

	}


	[PunRPC] 

	public void PRPC_ExecuteCanonDefender() {

        //called only on the server/masterclient

        specialAbilitiesManager.GetComponent<CanonDefender>().ExecuteAbility(photonView.viewID);

	}

	[PunRPC]

	public void PRPC_ExecuteFortify() {

        //called only on the server/masterclient

        specialAbilitiesManager.GetComponent<Fortify>().ExecuteAbility(photonView.viewID);

	}


	[PunRPC] 

	public void PRPC_ExecuteDoubleMove() {

        //called only on the server/masterclient

        specialAbilitiesManager.GetComponent<DoubleMove>().ExecuteAbility(photonView.viewID);

	}


    [PunRPC]

    public void PRPC_ExecuteSacrifice()
    {

        //called only on the server/masterclient

        specialAbilitiesManager.GetComponent<Sacrifice>().ExecuteAbility(photonView.viewID);

    }


    [PunRPC] 

	public void PRPC_ExecuteStonePieces() {

        //called only on the server/masterclient

        specialAbilitiesManager.GetComponent<StonePieces>().ExecuteAbility(photonView.viewID);

	}





	//************* ABILITY VISUALS *********************


	[PunRPC] 

	private void PRPC_SpawnButtonsOnYourScreen () {

        if (photonView.isMine)
        {

            this.gameObject.transform.Find("Abilities UI").GetComponent<AbilitiesUI>().SpawnButtons();

        }
	}



	[PunRPC]

	private void PRPC_SpawnButtonsOnOpponentScreen ( int normalAbilityOneID, int normalAbilityTwoID) {

        if (!photonView.isMine)
        {

            this.gameObject.transform.Find("Abilities UI").GetComponent<AbilitiesUI>().SpawnEnemyButtons(normalAbilityOneID, normalAbilityTwoID);

        }

	}


	[PunRPC]

	public void PRPC_UpdateAbilityCooldownVisuals( int abilityTypeID, int cooldown) {

		this.GetComponentInChildren<AbilitiesUI>().UpdateAbilityCooldownsVisual((AbilityType)GlobalFunctions.ResolveIDToAbility(abilityTypeID), cooldown);

	}


    [PunRPC]

    public void PRPC_ShowAbilityReadyEffect(byte[] serializedCDArray)
    {

        if (!photonView.isMine)
        {

            return;

        }

        List<int> deserializedCDArray = GlobalFunctions.DeserializeToObject<List<int>>(serializedCDArray);

        this.GetComponentInChildren<AbilitiesUI>().AbilityReadyEffect(this.clientTeam, deserializedCDArray);

    }



	//*************** WIN CONDITION **********************

	[PunRPC]

	public void PRPC_OnWinConditionSatisfied( bool didIWin ) {

		if (photonView.isMine) {

			return;

		}

		//introducing a small delay before disconnecting from the server to give both the clients to sync to the same
		//  state. Although this is a little bit messy, havent really got another quick fix for this. 
		//Ideally, if there was an animation or special effect that played during this delay, that would be better.
		Invoke("CallDisconnectMeFromServer", 0.25f);

	}

	void CallDisconnectMeFromServer() {

		GameObject.FindObjectOfType<MyNetworkManager>().DisconnectMeFromServer();

	}



	//*********SWITCH TURN INDICATOR**************
	[PunRPC]
	public void PRPC_ShowTurnIndicator() {

		// if pc has authority on this client, it means that it is 'YOUR TURN' for this client
		// if pc does NOT have authority on this client, it means that it is 'ENEMY TURN' on this client

		if (photonView.isMine) {

			board.transform.Find("TurnChange").transform.Find("your_turn").gameObject.SetActive(true);
			board.transform.Find("TurnChange").transform.Find("turnChangeGlow_PS").gameObject.SetActive(true);

		} else {

			board.transform.Find("TurnChange").transform.Find("enemy_turn").gameObject.SetActive(true);

		}

	}



    [PunRPC]

    public void PRPC_ExecuteDisconnect()
    {
        if(photonView.isMine)
        {

            PhotonNetwork.Disconnect();
            
        }
        
    }


}
