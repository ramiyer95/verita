﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bishop : VeritaMen {

    public override bool[,] PossibleMoves()
    {

        
        int pos_x = (int)pos.x;
        int pos_y = (int)pos.y;

        //looping through the squares
        int n;

        ResetLegalMovesArray();



            for (int i = -1; i<2; i = i+2)
            {


            //positive slope

            n = 1;

            while ( IsSquareOnBoard(pos_x + (n * i), pos_y + (n * i)))
            {

                if (!board.veritaMenOnBoard[pos_x + (n * i), pos_y + (n * i)])
                {

                    legalMovesArray[pos_x + (n * i), pos_y + (n * i)] = true;

                } else
                {

                    if (board.veritaMenOnBoard[pos_x + (n * i), pos_y + (n * i)].team == this.team)
                    {

                        legalMovesArray[pos_x + (n * i), pos_y + (n * i)] = false;

                    } else
                    {

                        legalMovesArray[pos_x + (n * i), pos_y + (n * i)] = true;

                    }

                    break;

                }
                

                n++;

            }


            //negative slope
            n = 1;


            while (IsSquareOnBoard(pos_x - (i * n), pos_y + (n * i)) )
          
            {
                if (!board.veritaMenOnBoard[pos_x - (i * n), pos_y + (n * i)])
                {

                    legalMovesArray[pos_x - (i * n), pos_y + (n * i)] = true;

                } else {

                    if (board.veritaMenOnBoard[pos_x - (i * n), pos_y + (n * i)].team == this.team)
                    {

                        //square occupied by piece of same team
                        legalMovesArray[pos_x - (i * n), pos_y + (n * i)] = false;
                        
                    } else
                    {

                        //capture square
                        legalMovesArray[pos_x - (i * n), pos_y + (n * i)] = true;

                    }

                    break;
                    
                }

             n++;

            }


        }


        //Moves that are definitely not allowed

        MovesNeverLegal();


        return legalMovesArray;

    }


}
