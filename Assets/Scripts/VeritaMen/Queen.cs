﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Queen : VeritaMen {

    public override bool[,] PossibleMoves()
    {
        ResetLegalMovesArray();

        int pos_x = (int)pos.x;
        int pos_y = (int)pos.y;

        
        
        int n = 1;

        
        //Rook component

        // forward 
        while (IsSquareOnBoard(pos_x + n, pos_y))
        {

            if (!board.veritaMenOnBoard[pos_x + n, pos_y])
            {

                legalMovesArray[pos_x + n, pos_y] = true;

            }
            else
            {

                if (board.veritaMenOnBoard[pos_x + n, pos_y].team == this.team)
                {

                    legalMovesArray[pos_x + n, pos_y] = false;

                }
                else
                {

                    legalMovesArray[pos_x + n, pos_y] = true;

                }

                break;

            }

            n++;

        }



        n = 1;

        //backward
        while (IsSquareOnBoard(pos_x - n, pos_y))
        {

            if (!board.veritaMenOnBoard[pos_x - n, pos_y])
            {

                legalMovesArray[pos_x - n, pos_y] = true;

            }
            else
            {

                if (board.veritaMenOnBoard[pos_x - n, pos_y].team == this.team)
                {

                    legalMovesArray[pos_x - n, pos_y] = false;

                }
                else
                {

                    legalMovesArray[pos_x - n, pos_y] = true;

                }

                break;

            }

            n++;

        }




        n = 1;

        //upward
        while (IsSquareOnBoard(pos_x, pos_y + n))
        {

            if (!board.veritaMenOnBoard[pos_x, pos_y + n])
            {

                legalMovesArray[pos_x, pos_y + n] = true;

            }
            else
            {

                if (board.veritaMenOnBoard[pos_x, pos_y + n].team == this.team)
                {

                    legalMovesArray[pos_x, pos_y + n] = false;

                }
                else
                {

                    legalMovesArray[pos_x, pos_y + n] = true;

                }

                break;

            }

            n++;

        }



        n = 1;

        //down
        while (IsSquareOnBoard(pos_x, pos_y - n))
        {

            if (!board.veritaMenOnBoard[pos_x, pos_y - n])
            {

                legalMovesArray[pos_x, pos_y - n] = true;

            }
            else
            {

                if (board.veritaMenOnBoard[pos_x, pos_y - n].team == this.team)
                {

                    legalMovesArray[pos_x, pos_y - n] = false;

                }
                else
                {

                    legalMovesArray[pos_x, pos_y - n] = true;

                }

                break;

            }

            n++;

        }





        //Bishop Component

        for (int i = -1; i < 2; i = i + 2)
        {


            //positive slope

            n = 1;

            while (IsSquareOnBoard(pos_x + (n * i), pos_y + (n * i)))
            {

                if (!board.veritaMenOnBoard[pos_x + (n * i), pos_y + (n * i)])
                {

                    legalMovesArray[pos_x + (n * i), pos_y + (n * i)] = true;

                }
                else
                {

                    if (board.veritaMenOnBoard[pos_x + (n * i), pos_y + (n * i)].team == this.team)
                    {

                        legalMovesArray[pos_x + (n * i), pos_y + (n * i)] = false;

                    }
                    else
                    {

                        legalMovesArray[pos_x + (n * i), pos_y + (n * i)] = true;

                    }

                    break;

                }


                n++;

            }


            //negative slope
            n = 1;


            while (IsSquareOnBoard(pos_x - (i * n), pos_y + (n * i)))

            {
                if (!board.veritaMenOnBoard[pos_x - (i * n), pos_y + (n * i)])
                {

                    legalMovesArray[pos_x - (i * n), pos_y + (n * i)] = true;

                }
                else
                {

                    if (board.veritaMenOnBoard[pos_x - (i * n), pos_y + (n * i)].team == this.team)
                    {

                        //square occupied by piece of same team
                        legalMovesArray[pos_x - (i * n), pos_y + (n * i)] = false;

                    }
                    else
                    {

                        //capture square
                        legalMovesArray[pos_x - (i * n), pos_y + (n * i)] = true;

                    }

                    break;

                }

                n++;

            }


        }



        MovesNeverLegal();


        return legalMovesArray;
        
    }
}
