﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VMSync : Photon.MonoBehaviour {

    //Using this class to synchronize important VeritaMen data (for now, the position)

    // Need to code in interpolation. Read on date 29th June 2018 in the Journal for the logic. 


    List<Vector3> receivedPositionsArray = new List<Vector3>();

    List<float> receivedTimestampsArray = new List<float>();


    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

        if(stream.isWriting)
        {

            stream.SendNext(this.transform.position);
            
        } else
        {
            //the list can potentially grow very big, so need to clamp its size to a reasonable amount of data

            if (receivedPositionsArray.Count == 100)
            {

                receivedPositionsArray.RemoveAt(0);
                receivedTimestampsArray.RemoveAt(0);
                
            }

            receivedPositionsArray.Add((Vector3)stream.ReceiveNext());
            receivedTimestampsArray.Add(Time.time);

            if (receivedPositionsArray.Count == 1)
            {
                return;
            }

            float averageSpeed = 0.0f;
            int i;
            //Need to calculate the average speed to pass into the coroutine
            for (i = 0; i <= receivedPositionsArray.Count - 1; i++)
            {

                if (i != 0)
                {

                    float s = Vector3.Distance(receivedPositionsArray[i], receivedPositionsArray[i - 1]) /
                                (receivedTimestampsArray[i] - receivedTimestampsArray[i - 1]);


                    averageSpeed = ((averageSpeed * (i - 1)) + s) / i; 

                }

            }

            StopAllCoroutines();

            //Using i-1 because in the last iteration of the for loop, i would have incremented one more than the
            //  range of the array
            StartCoroutine(InterpolateThis(receivedPositionsArray[i-1], averageSpeed));

        }
        

    }



    public IEnumerator InterpolateThis(Vector3 endPos, float averageSpeed)
    {

        Debug.Log("a speed : " + averageSpeed);
        

        Vector3 startPos = this.transform.position;

        float dist = Vector3.Distance(endPos, startPos);

        float time = dist / averageSpeed;

        time = Mathf.Clamp(time, 0f, 0.125f);

        Debug.Log("time : " + time);

        float elapsedTime = 0;

        while (elapsedTime < time)
        {

            this.transform.position = Vector3.Lerp(startPos, endPos, elapsedTime / time);

            elapsedTime += 0.025f;

            yield return new WaitForSecondsRealtime(0.025f);

        }

        this.transform.position = endPos;

    }



}
