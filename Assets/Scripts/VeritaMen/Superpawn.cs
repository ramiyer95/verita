﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Superpawn : VeritaMen {

    public override bool[,] PossibleMoves()
    {

        //The legalMovesArray is reset to all false in 7x7 every time before making the calculation.
        ResetLegalMovesArray();

        int pos_x = (int)pos.x;
        int pos_y = (int)pos.y;
 


        //conditions to find the legal moves for the pawn

        if (this.team == Team.BLUE)
        {

            //MOVES FOR THE BLUE TEAM
            
            
            for (int i = pos_x-1; i<= pos_x+1; i++)
            {

                for (int j = pos_y-1; j<= pos_y+1; j++)
                {

                    if (IsSquareOnBoard(i,j))
                    {

                        if (board.veritaMenOnBoard[i,j])
                        {

                            legalMovesArray[i, j] = false;


                        } else
                        {

                            legalMovesArray[i, j] = true;

                        }


                    }


                }


            }

            
            // check the capture position (x-1, y-1)

            if (IsSquareOnBoard(pos_x-1 , pos_y-1) )
            {

                if (board.veritaMenOnBoard[pos_x-1, pos_y-1])
                {

                    if (board.veritaMenOnBoard[pos_x - 1, pos_y - 1].team == this.team)
                    {

                        legalMovesArray[pos_x - 1, pos_y - 1] = false;

                    }
                    else
                    {

                        legalMovesArray[pos_x - 1, pos_y - 1] = true;

                    }


                } 



            }
            

            

        } else
        {

            //MOVES FOR THE RED TEAM

            for (int i = pos_x - 1; i <= pos_x + 1; i++)
            {

                for (int j = pos_y - 1; j <= pos_y + 1; j++)
                {

                    if (IsSquareOnBoard(i, j))
                    {

                        if (board.veritaMenOnBoard[i, j])
                        {

                            legalMovesArray[i, j] = false;


                        }
                        else
                        {

                            legalMovesArray[i, j] = true;

                        }


                    }


                }


            }

            // capture position for the red team is (x+1, y+1)

            if (IsSquareOnBoard(pos_x + 1, pos_y + 1))
            {

                if (board.veritaMenOnBoard[pos_x + 1, pos_y + 1])
                {

                    if (board.veritaMenOnBoard[pos_x + 1, pos_y + 1].team == this.team)
                    {

                        legalMovesArray[pos_x + 1, pos_y + 1] = false;

                    }
                    else
                    {

                        legalMovesArray[pos_x + 1, pos_y + 1] = true;

                    }


                }
               



            }



        }


        MovesNeverLegal();
        

        return legalMovesArray;

    }
}
