﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class VeritaMen : MonoBehaviour {

    public Vector2 pos;

    public Team team;

    public Board board;

    public WallManager walls;

    public RespawnManager respawnManager;

    public TurnManager turnManager;

    //set in editor
    [Tooltip("The VeritaManType of this piece")]
    public VeritaMenType veritamenTypeOfThis;

	//We need a legalMovesArray to make sure that the calculation of possible moves is not done repeatedly.
    //Update the legalMovesArray whenever the position of the piece is changed. 
    public bool[,] legalMovesArray = new bool[7,7];

    public VMSync thisVMSync;

    //for permanent states like normal, corrupted, downgraded
    public VeritaManState permaState = VeritaManState.NORMAL;

    //for temp states which a piece can have multiple of at a time
    // temp states include stone, turnglow
    public List<VeritaManState> tempState = new List<VeritaManState>();
    
    //[SyncVar]
    public string beforeSacrifice;

    //[SyncVar]
    public int killThisAtTurn = -1;
    //Default is -1 because turns will never take the value of -1


    public bool addThisToRespawnList = true;
    //Only false when the unit has been sacrificed. In that case, we dont want it to be added to the respawn list just yet

    public Sprite corruptedSprite;

    public Sprite downSprite;
 


    public void Start()
    {

        this.board = GameObject.Find("Board").GetComponent<Board>();
        this.walls = GameObject.Find("Walls").GetComponent<WallManager>();

        turnManager = GameObject.Find("TurnManager").GetComponent<TurnManager>();

        respawnManager = board.gameObject.GetComponent<RespawnManager>();

        thisVMSync = this.gameObject.GetComponent<VMSync>();
                

        if (this.veritamenTypeOfThis == VeritaMenType.BULLDOZER)
        {

            if (this.team == Team.BLUE)
            {

                this.GetComponent<Bulldozer>().deathSquare1 = GameObject.Find("BD_DeathSquares/B/DeathSquarePanel1");
                this.GetComponent<Bulldozer>().deathSquare2 = GameObject.Find("BD_DeathSquares/B/DeathSquarePanel2");

            }
            else
            {

                this.GetComponent<Bulldozer>().deathSquare1 = GameObject.Find("BD_DeathSquares/R/DeathSquarePanel1");
                this.GetComponent<Bulldozer>().deathSquare2 = GameObject.Find("BD_DeathSquares/R/DeathSquarePanel2");

            }


        }



    }



         
    public bool MoveVeritaMan (Vector2 moveToSquare)
    {
                
        if (legalMovesArray[(int)moveToSquare.x, (int)moveToSquare.y])
        {

            

            if (board.veritaMenOnBoard[(int)moveToSquare.x, (int)moveToSquare.y])
            {
                // following code is here so that the "ghost" bulldozer does not go around destroying walls.
                // bulldozers were destroying walls even after being destroyed
                if (board.veritaMenOnBoard[(int)moveToSquare.x, (int)moveToSquare.y].gameObject.GetComponent<Bulldozer>())
                {
                    if (this.team == Team.BLUE)
                    {
                        walls.ResetSiegeValues(Team.RED);

                        //also need to update the wall sprites when the bulldozer dies
                        
                    } else
                    {

                        walls.ResetSiegeValues(Team.BLUE);
                        
                    }
                    

					board.veritaMenOnBoard[(int)moveToSquare.x, (int)moveToSquare.y].gameObject.GetComponent<Bulldozer>().toBeDestroyedInCurrentFrame = true;

                }


                PhotonNetwork.Destroy(board.veritaMenOnBoard[(int)moveToSquare.x, (int)moveToSquare.y].gameObject);

            }

            // ************* CODE FOR FORTIFY ABILITY **************

			// Check if this piece is the fortified piece

            Vector2 fortifiedPieaceSqaure = (Vector2)board.fortifiedSqaures[this.team];

            if (this.pos.x == fortifiedPieaceSqaure.x && this.pos.y == fortifiedPieaceSqaure.y) {

            	//if this condition is true, it means that this is the fortified square

            	//moving the piece out of this square would mean that fortification is now lost

            	foreach (GameObject shield in GameObject.FindGameObjectsWithTag("fortify_shield")) {

            		if (shield.GetComponent<FortifyShield>().shieldOfTeam == this.team) {

            			PhotonNetwork.Destroy(shield);

            		}

            	}

            	board.fortifiedSqaures[this.team] = new Vector2(-1, -1);

            }



            
            //this.transform.position = board.BoardToGameCoordinates(moveToSquare, true);

			StartCoroutine(MovePieceOverTime(this.gameObject, GlobalFunctions.GridToWorldCoordinates(moveToSquare), 0.25f));

            this.legalMovesArray = null;

            return true;

        }

        return false;
        
    }



    public virtual bool[,] PossibleMoves()
    {

        bool[,] returnArray = new bool[7, 7];

        returnArray[3, 3] = true;

        return returnArray;

    }

    private bool[,] LegalMovesToHighGround()
    {

    	//At start, both win squares are always false. Their conditions need to be checked and true needs to be set
    	//  later in this function. This is in response to a bug I was facing where the high ground appears green even
    	//  thought it should not. This is because the PossibleMoves functions is setting the win condition as true and
    	//  the WS condition is not being rechecked since piece is not on HighGround. 

    	legalMovesArray[0,0] = false;
    	legalMovesArray[6,6] = false;


        int a;

        int[][] checkSquares = new int[3][];

        // All work is being done on the legalMovesArray. Thus it is not required to pass in any parameters.
        // This class already has access to the legalMovesArray.

        if(this.team == Team.BLUE)
        {
            a = 1;

            checkSquares[0] = new int[] { 1, 0 };
            checkSquares[1] = new int[] { 1, 1 };
            checkSquares[2] = new int[] { 0, 1 };

        } else
        {
            a = 0;

            checkSquares[0] = new int[] { 5, 6 };
            checkSquares[1] = new int[] { 5, 5 };
            checkSquares[2] = new int[] { 6, 5 };

        }

        int x1 = (int)this.pos.x;
        int y1 = (int)this.pos.y;

        for (int i = 0; i < 3; i++)
        {

            int x2 = checkSquares[i][0];
            int y2 = checkSquares[i][1];

            if (legalMovesArray[x2, y2])
            {

                for (int j = 0; j < 7; j++)
                {

                    float eqOfLineResult;

                    if (x2-x1 == 0)
                    {
                    	//slope is infinity

                    	// thus we need to check now if the x of the point blocker is the same as x1 and x2. If it is, then the point blocker
                   		// satisfies the eqn.

                        eqOfLineResult = walls.pointBlocksHG[a][j].x - x1;


                    } else
                    {

                        eqOfLineResult = walls.pointBlocksHG[a][j].y - (((y2 - y1) / (x2 - x1)) * walls.pointBlocksHG[a][j].x) - (-(((y2 - y1) / (x2 - x1)) * x1) + y1);

                    }


                    if ( eqOfLineResult == 0)
                    {

                        legalMovesArray[x2, y2] = false;
                        break;

                    }
                    else
                    {

                        legalMovesArray[x2, y2] = true;


                    }

                }
            }


        }
        
        
        //need to write code for legal moves to high ground

        for (int i = 0; i<3; i++)
        {

            if ((int)this.pos.x == checkSquares[i][0] && (int)this.pos.y == checkSquares[i][1])
            {
                int x2;
                int y2;

                if (this.team == Team.BLUE)
                {

                    x2 = 0;
                    y2 = 0;
                    a = 1;

                } else
                {

                    x2 = 6;
                    y2 = 6;
                    a = 0;
                }

                x1 = (int)this.pos.x;
                y1 = (int)this.pos.y;

                float eqOfLineResult;




                for (int j = 0; j < 3; j++) {

                    if (x2 - x1 == 0)
                    {

                        eqOfLineResult = walls.pointBLocksWS[a][j].x;

                    }
                    else
                    {

                        eqOfLineResult = walls.pointBLocksWS[a][j].y - (((y2 - y1) / (x2 - x1)) * walls.pointBLocksWS[a][j].x) - (-(((y2 - y1) / (x2 - x1)) * x1) + y1);

                    }


                    if ( eqOfLineResult == 0)
                    {

                        legalMovesArray[x2, y2] = false;
                        break;


                    } else
                    {

                        legalMovesArray[x2, y2] = true;

                    }

                }

                



            }



        }


        //need to check for special case of lines going through (6,4)-(5,5)-(4,6) and (2,0)-(1,1)-(0,2)
        //pieces are able to JUMP OVER WALLS if this does not exist

        if (this.team == Team.BLUE) {

        	//if at the relevant position
			if ( ((int) this.pos.x == 2) && ((int) this.pos.y == 0) ) {

        		//if moving to the other side of the wall is being considered legal
        		if ( legalMovesArray[0,2] ) {

        			//if moving to the highground is not allowed
        			if ( !legalMovesArray[1,1] ) {

        				legalMovesArray[0,2] = false;

        			}

        		}
        	}



			//if at the relevant position
			if ( ((int) this.pos.x == 0) && ((int) this.pos.y == 2) ) {

        		//if moving to the other side of the wall is being considered legal
        		if ( legalMovesArray[2,0] ) {

        			//if moving to the highground is not allowed
        			if ( !legalMovesArray[1,1] ) {

        				legalMovesArray[2,0] = false;

        			}

        		}
        	}


        } else {


			//if at the relevant position
			if ( ((int) this.pos.x == 6) && ((int) this.pos.y == 4) ) {

        		//if moving to the other side of the wall is being considered legal
        		if ( legalMovesArray[4,6] ) {

        			//if moving to the highground is not allowed
        			if ( !legalMovesArray[5,5] ) {

        				legalMovesArray[4,6] = false;

        			}

        		}
        	}



			//if at the relevant position
			if ( ((int) this.pos.x == 4) && ((int) this.pos.y == 6) ) {

        		//if moving to the other side of the wall is being considered legal
        		if ( legalMovesArray[6,4] ) {

        			//if moving to the highground is not allowed
        			if ( !legalMovesArray[5,5] ) {

        				legalMovesArray[6,4] = false;

        			}

        		}
        	}



        }




        return legalMovesArray;

    }





	private bool[,] CheckFortifications() {

    	// Need to check for fortify first 

    	Team teamToCheck;

    	if (this.team == Team.BLUE) {

    		teamToCheck = Team.RED;

    	} else {

    		teamToCheck = Team.BLUE;

    	}

    	Vector2 fortifiedSquare = (Vector2) board.fortifiedSqaures[teamToCheck];

    	// By default the fortifiedSquares hashtable will have vector (-1,-1) to represent no square is fortified.
    	// Thus, if the returned vector is not -1, only then it is actionable. 

    	if ( ! (fortifiedSquare.x == -1) ) {

     		legalMovesArray[(int) fortifiedSquare.x, (int) fortifiedSquare.y] = false;

    	}


    	// Secondly, need to check for stone pieces

    	if ((bool) board.stonePiecesOfTeam[teamToCheck]) {

    		// if stone pieces of a team is set, then on whatever square there is a piece of that team should be false
    		// in legalMovesArray

    		// this is probably sloppy but lets just loop through all items in veritaManOnBoard and marks items

    		int i = 0;

    		int j = 0;


    		for (i=0; i<7; i++) {

    			for (j=0; j<7; j++) {

    				if (board.veritaMenOnBoard[i,j]) {

    					if (board.veritaMenOnBoard[i,j].team == teamToCheck) {

    						legalMovesArray[i,j] = false;

    					}

    				}



    			}

    		}



    	}

    	return legalMovesArray;

    }




    public bool IsSquareOnBoard( int x, int y)
    {

        if (x > 6 || x < 0 )
        {

            return false;


        }

        if (y > 6 || y < 0)
        {

            return false;
            
        }


        return true;

    }


    public bool[,] ResetLegalMovesArray()
    {

        legalMovesArray = new bool[7, 7];

        for (int i = 0; i<7; i++)
        {

            for (int j =0; j<7; j++)
            {

                legalMovesArray[i, j] = false;  

            }

        }

        return legalMovesArray;

    }


    public bool[,] MovesNeverLegal()
    {

    	CheckFortifications();

        LegalMovesToHighGround();

        if (team == Team.BLUE)
        {

            legalMovesArray[6, 6] = false;

        } else
        {

            legalMovesArray[0, 0] = false;

        }

        legalMovesArray[(int)pos.x, (int)pos.y] = false;

        return legalMovesArray;
        
    }


    [PunRPC] 

    public void PRPC_ActivateSkullEffect()
    {
        
        if(this.gameObject.transform.Find("sacrificeSkullEffect"))
        {

            this.gameObject.transform.Find("sacrificeSkullEffect").gameObject.SetActive(true);

        }
        

    }
       




    //Need a Coroutine to move the piece fluidly instantly instead of teleporting it. 

    public IEnumerator MovePieceOverTime( GameObject objectToMove, Vector3 endPos, float time) {

    	Vector3 startPos = objectToMove.transform.position;

    	float elapsedTime = 0;
        
    	while (elapsedTime < time)  {
    		
    		objectToMove.transform.position = Vector3.Lerp(startPos, endPos, elapsedTime/time);

    		elapsedTime += 0.025f;

    		yield return new WaitForSecondsRealtime(0.025f);

    	}

    	objectToMove.transform.position = endPos;
        
    }

    //Each veritaMan must inform the Respawn Manager of its respawn turn and its type

    void OnDestroy() {

        //Fortification ability
        if (PhotonNetwork.isMasterClient) {

			Vector2 fortifiedPieaceSqaure = (Vector2)board.fortifiedSqaures[this.team];

	        if (this.pos.x == fortifiedPieaceSqaure.x && this.pos.y == fortifiedPieaceSqaure.y) {

	        	//if this condition is true, it means that this is the fortified square

	        	//moving the piece out of this square would mean that fortification is now lost

	        	foreach (GameObject shield in GameObject.FindGameObjectsWithTag("fortify_shield")) {

	        		if (shield.GetComponent<FortifyShield>().shieldOfTeam == this.team) {

	        			PhotonNetwork.Destroy(shield);

	        		}

	        	}

	        	board.fortifiedSqaures[this.team] = new Vector2(-1, -1);

	     	}

     	}


        AudioManager.instance.PlaySound("shatter_audio");

        //deactivating the deathsquares
        if (this.veritamenTypeOfThis == VeritaMenType.BULLDOZER)
        {
            Debug.Log("reached");

            if (this.team == Team.BLUE)
            {

                GameObject go = GameObject.Find("BD_DeathSquares/B");

                foreach (Transform child in go.transform)
                {

                    child.gameObject.SetActive(false);

                }

            }
            else
            {

                GameObject go = GameObject.Find("BD_DeathSquares/R");

                foreach (Transform child in go.transform)
                {

                    child.gameObject.SetActive(false);

                }


            }



        }

        //Triggering the animation

        if (this.team == Team.BLUE) {

			GameObject deathSplatter = board.transform.Find("SpecialEffects").transform.Find("DeathSplatter").transform.Find("b_death").gameObject;

			deathSplatter.transform.position = this.transform.position;

			deathSplatter.SetActive(true);   	   	

     	} else {

			GameObject deathSplatter = board.transform.Find("SpecialEffects").transform.Find("DeathSplatter").transform.Find("r_death").gameObject;

			deathSplatter.transform.position = this.transform.position;

			deathSplatter.SetActive(true);

     	}

     	//need to deactivate when the effect is over
		board.InvokeDeactivateSpecialEffectsInXSeconds(2f);

     	//Code for the respawn manager

    	int currentTurn;

    	int respawnThisOnTurn;

		List<int> relevantRespawnTurnsList;


    	if (this.team == Team.BLUE) {

    		currentTurn = turnManager.blueTurn;

			relevantRespawnTurnsList = respawnManager.ReturnRespawnTurnListOfTeam(Team.BLUE);

    	} else {

    		currentTurn = turnManager.redTurn;

    		relevantRespawnTurnsList = respawnManager.ReturnRespawnTurnListOfTeam(Team.RED);

    	}


    	if (relevantRespawnTurnsList.Count == 0) {

    		respawnThisOnTurn = GlobalFunctions.GetCooldownOfVeritaMantype(veritamenTypeOfThis) + currentTurn;

    	} else {

			// 					accessing the last element of a non-empty list.							
    		respawnThisOnTurn = relevantRespawnTurnsList[relevantRespawnTurnsList.Count - 1] + GlobalFunctions.GetCooldownOfVeritaMantype(veritamenTypeOfThis);

    	}



    	if (permaState == VeritaManState.NORMAL) {

    		if(addThisToRespawnList) {

    			respawnManager.AddVeritaManToRespawnList(veritamenTypeOfThis, respawnThisOnTurn, this.team);

    		}

    		// need to check for isserver because the OnDestroy function is called on both the clients
    		/*if (isServer) {

				board.UpdateRespawnUI();

			}
            */
    		return;

    	}


    	// for both corrupted or downgraded pieces, the original piece respawns with the respawn cooldown of the dying piece

		VeritaMenType veritaManToGoIntoList = (VeritaMenType) GlobalFunctions.ResolveStringToVeritaManType(beforeSacrifice);

    	respawnManager.AddVeritaManToRespawnList(veritaManToGoIntoList, respawnThisOnTurn, this.team);

        /*if (isServer) {

				board.UpdateRespawnUI();

		}*/

        



    }

    

}
