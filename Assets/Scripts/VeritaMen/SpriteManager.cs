﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteManager : Photon.MonoBehaviour {

    public List<Sprite> normalSprites;

    public List<Sprite> corruptedSprites;

    public List<Sprite> downgradedSprites;

    // For the above lists (for sprites of each permanent state) following is the convention :

    // 0 - default sprite (no temp state)
    // 1 - stone
    // 2 - turn glow
    // 3 - stone + turn glow




    public void CallPRPCUpdateSprites()
    {
        // the purpose of this function is to look at the temporary and permanent states of this VeritaMan and 
        //  accordingly call the PRPC_UpdateSprite function in a way that resembles the states of this VeritaMan

        bool isStone = false;

        bool isTurnGlow = false;

        int stateID = GlobalFunctions.ResolveVeritaManStateToID(this.gameObject.GetComponent<VeritaMen>().permaState);

        foreach (VeritaManState theState in this.gameObject.GetComponent<VeritaMen>().tempState)
        {

            if (theState == VeritaManState.STONE)
            {

                isStone = true;

            }

            if (theState == VeritaManState.TURNGLOW)
            {

                isTurnGlow = true;

            }

        }

        this.gameObject.GetComponent<PhotonView>().RPC("PRPC_UpdateSprite", PhotonTargets.AllViaServer, stateID, isStone, isTurnGlow);

    }

    







    [PunRPC]

    public void PRPC_UpdateSprite(int stateID, bool isStone, bool isTurnGlow)
    {

        
        VeritaManState theState1 = (VeritaManState)GlobalFunctions.ResovleIDToVeritaManState(stateID);

        if (theState1 == VeritaManState.NORMAL)
        {

            // the following looks like a bad choice of sequence of condition-checking with the perspective of 
            //   readability ( '!' is difficult to process mentally).
            // however the first condition has the highest probability of occurence and hence it is a good idea to 
            //   do it like this. 

            if (!isStone && !isTurnGlow)
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = normalSprites[0];

            }
            else if (!isStone && isTurnGlow)
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = normalSprites[2];

            }
            else if (isStone && !isTurnGlow)
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = normalSprites[1];

            }
            else
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = normalSprites[3];

            }
            

        }
        else if (theState1 == VeritaManState.DOWNGRADED)
        {

            if (!isStone && !isTurnGlow)
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = downgradedSprites[0];

            }
            else if (!isStone && isTurnGlow)
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = downgradedSprites[2];

            }
            else if (isStone && !isTurnGlow)
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = downgradedSprites[1];

            }
            else
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = downgradedSprites[3];

            }
        }       
        else if (theState1 == VeritaManState.CORRUPTED)
        {

            if (!isStone && !isTurnGlow)
            {
                Debug.Log(this.name);
                this.gameObject.GetComponent<SpriteRenderer>().sprite = corruptedSprites[0];

            }
            else if (!isStone && isTurnGlow)
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = corruptedSprites[2];

            }
            else if (isStone && !isTurnGlow)
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = corruptedSprites[1];

            }
            else
            {

                this.gameObject.GetComponent<SpriteRenderer>().sprite = corruptedSprites[3];

            }


        }



    }


}
