﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulldozer : VeritaMen {


    public GameObject deathSquare1;
    public GameObject deathSquare2;

    public int[][] movePath;

    
    //currentPos is the current position of the bulldozer out of the 12 possible positions
    //keeping track of the bulldozer is difficult without a dedicated variable
    public int currentPos;


    // Need this bool because Destroy(gameObject) actually executes at the end of the frame. So the bulldozer is free
    //   to execute its code in the current frame even though it is actually destroyed.
    // This bool is set when a VeritaMen calls the destroy method to kill the Bulldozer.
    public bool toBeDestroyedInCurrentFrame = false;


    public void AssignMovePathAndDeathSquares ()
    {

        // this function basically assigns the correct movePath at the start of the game depending on the team
        //  to which this piece belongs


        if (this.team == Team.BLUE)
        {

            int[] p0 = new int[2] { 4,6 };
            int[] p1 = new int[2] { 3,6 };
            int[] p2 = new int[2] { 2,6 };
            int[] p3 = new int[2] { 1,6 };
            int[] p4 = new int[2] { 0,6 };
            int[] p5 = new int[2] { 0,5 };
            int[] p6 = new int[2] { 0,4 };
            int[] p7 = new int[2] { 0,3 };
            int[] p8 = new int[2] { 0,2 };
            int[] p9 = new int[2] { 0,1 };
            int[] p10 = new int[2] { 1,1 };
            int[] p11 = new int[2] { 1,0 };

            movePath = new int[][] { p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 };

        }
        else
        {

            int[] p0 = new int[2] { 2, 0 };
            int[] p1 = new int[2] { 3, 0 };
            int[] p2 = new int[2] { 4, 0 };
            int[] p3 = new int[2] { 5, 0 };
            int[] p4= new int[2] { 6, 0 };
            int[] p5 = new int[2] { 6, 1 };
            int[] p6 = new int[2] { 6, 2 };
            int[] p7 = new int[2] { 6, 3 };
            int[] p8 = new int[2] { 6, 4 };
            int[] p9 = new int[2] { 6, 5 };
            int[] p10 = new int[2] { 5, 5 };
            int[] p11 = new int[2] { 5, 6 };

            movePath = new int[][] { p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 };


        }



    }




    public void SpecialMoveBulldozer()
    {
        
        int a = 0;

        Vector2 moveToSquare = new Vector2(0, 0);

        int stepsToMove = 0;

        //depending on whether this bulldozer is Red's or Blue's, the corresponding walls will be checked.
        //Although the good thing is that the symmertry helps out a lot. 

        if (this.team == Team.BLUE)
        {

            //index for red's walls in wallArray
            a = 1;

            walls.DestroyWallOf(Team.RED);

        }
        else
        {

            //index for blue's walls in the wallArray
            a = 0;

            walls.DestroyWallOf(Team.BLUE);
            
        }

        //currentPos is an int ranging from 0-11 (total 12) containing the current position of the bulldozer
        //if the currentPos is 6 or lower, basic logic is enough to suffice.

        if (currentPos < 7)
        {
            //movePath[currentPos + 1][0] => x point, movePath[currentPos + 1][1] => y point
            if (board.veritaMenOnBoard[movePath[currentPos + 1][0], movePath[currentPos + 1][1]])
            {

                stepsToMove = 1;
                

            } else
            {

                stepsToMove = 2;

            }

            if (currentPos + stepsToMove == 8)
            {

                walls.SiegeWallOf(a, 4);

            }
            

        }
        else if (currentPos == 7)
        {


            if (walls.wallsArray[a, 3] || board.veritaMenOnBoard[movePath[currentPos+1][0], movePath[currentPos+1][1]])
            {

                //landing on p8

                stepsToMove = 1;

                //If the bulldozer survives till the start of the next turn, the wall w4 must be destroyed.
                walls.SiegeWallOf(a, 4);


            }
            else
            {

                // landing on p9    

                stepsToMove = 2;
 
                if (walls.wallsArray[a, 5])
                {
                    // SiegeWallOf(a=1,..) means Blue is being sieged, a=0 means Red is being sieged.
                    walls.SiegeWallOf(a, 6);

                }


            }






        }
        else if (currentPos == 8)
        {


            if (walls.wallsArray[a, 5] || board.veritaMenOnBoard[movePath[currentPos+1][0], movePath[currentPos+1][1]])
            {
                //landing on p9

                stepsToMove = 1;

                walls.SiegeWallOf(a, 6);

            }
            else
            {
                //landing on p10

                stepsToMove = 2;

                //************IMPORTANT***************
                //there is a way to get confused over here since there is a '2' in the wallsArray and a '3' in the
                // SiegeWall funtion. 
                // WallsArray starts from 0, hence it 2 in the walls array refers to wallID 3
                // SiegeWall takes the wallID directly. 
                if (walls.wallsArray[a, 2])
                {

                    walls.SiegeWallOf(a, 3);

                }
                else if (walls.wallsArray[a, 1])
                {

                    walls.SiegeWallOf(a, 2);

                }


            }

        }
        else if (currentPos == 9)
        {

            if (walls.wallsArray[a, 2] || walls.wallsArray[a, 1] || board.veritaMenOnBoard[movePath[currentPos+1][0], movePath[currentPos+1][1]])
            {
                //landing on p10

                stepsToMove = 1;


                //read explanation in currentPos == 8 if confused with the following wall numbers.
                if (walls.wallsArray[a, 2])
                {

                    walls.SiegeWallOf(a, 3);

                }
                else
                {

                    walls.SiegeWallOf(a, 2);

                }


            }
            else
            {

                //landing on p11

                stepsToMove = 2;

                if (walls.wallsArray[a, 0])
                {

                    walls.SiegeWallOf(a, 1);

                }
                else if (walls.wallsArray[a, 4])
                {

                    walls.SiegeWallOf(a, 5);

                }


            }



        }
        else if (currentPos == 10)
        {
            //bd can only threaten p11 because that is the last sqaure the bd can go to.

            if (walls.wallsArray[a, 1])
            {

                //staying on p10

                stepsToMove = 0;

                //since stepsToMove is 0, the bd does not threaten p11 yet.

                walls.SiegeWallOf(a, 2);

            }
            else
            {
                //landing on p11

                stepsToMove = 1;

                if (walls.wallsArray[a, 0])
                {

                    walls.SiegeWallOf(a, 1);

                }
                else if (walls.wallsArray[a, 4])
                {

                    walls.SiegeWallOf(a, 5);

                }
                               

            }
        }
        else if (currentPos == 11)
        {

            //stay on p11

            if (walls.wallsArray[a, 4])
            {

                walls.SiegeWallOf(a, 5);

            }
            else
            {

                PhotonNetwork.Destroy(this.gameObject);

            }


        }


        moveToSquare = new Vector2(movePath[currentPos + stepsToMove][0], movePath[currentPos + stepsToMove][1]);

        if ( board.veritaMenOnBoard[(int)moveToSquare.x, (int)moveToSquare.y] )
        {

            if (board.veritaMenOnBoard[(int)moveToSquare.x, (int)moveToSquare.y] != this && !toBeDestroyedInCurrentFrame)
            {

				Debug.Log("destroying " + board.veritaMenOnBoard[(int)moveToSquare.x, (int)moveToSquare.y].name);

                PhotonNetwork.Destroy(board.veritaMenOnBoard[(int)moveToSquare.x, (int)moveToSquare.y].gameObject);

            }
        }

        if (!toBeDestroyedInCurrentFrame) {

            board.GetComponent<AudioNetworking>().CallPRPCPlaySound("bulldozer_audio");

	        //this.transform.position = board.BoardToGameCoordinates(moveToSquare, true);
			StartCoroutine(MovePieceOverTime(this.gameObject, GlobalFunctions.GridToWorldCoordinates(moveToSquare), 0.25f));
            
	        //**** UPDATE THE veritaMenOnBoard ARRAY ALWAYS AFTER MOVEMENT ****
	        board.veritaMenOnBoard[movePath[currentPos][0], movePath[currentPos][1]] = null;

	        board.veritaMenOnBoard[movePath[currentPos + stepsToMove][0], movePath[currentPos + stepsToMove][1]] = this;

	        currentPos += stepsToMove;

	        this.pos = moveToSquare;

            UpdateDeathSquares(a);
            
        }
    }






    void UpdateDeathSquares(int a)
    {
        //a is the team differentiator, same as above.

        //Check the currentPos and see if any pieces are threatened by the Bulldozer's possible moves 
        // in the coming turn. 

        Vector2 expectedNextPos1 = new Vector2(-1, -1);
        Vector2 expectedNextPos2 = new Vector2(-1, -1);

        //currentPos is an int ranging from 0-11 (total 12) containing the current position of the bulldozer
        //if the currentPos is 6 or lower, basic logic is enough to suffice.

        if (currentPos < 7)
        {

            expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

            expectedNextPos2 = new Vector2(movePath[currentPos + 2][0], movePath[currentPos + 2][1]);


        }
        else if (currentPos == 7)
        {


            if (walls.wallsArray[a, 3] || board.veritaMenOnBoard[movePath[currentPos + 1][0], movePath[currentPos + 1][1]])
            {

                //The above condition checks for either wall blocking or piece blocking the bd.
                // If wall blocks the bd, the second square is definitely safe because the wall protects it.
                // But if a piece blocks the bd, the second square is threatened because the piece may be moved.
                if (walls.wallsArray[a, 3])
                {

                    expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

                }
                else
                {

                    expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

                    expectedNextPos2 = new Vector2(movePath[currentPos + 2][0], movePath[currentPos + 2][1]);

                }

            }
            else
            {
                // landing on p9    

                expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

                expectedNextPos2 = new Vector2(movePath[currentPos + 2][0], movePath[currentPos + 2][1]);
                

            }
            
        }
        else if (currentPos == 8)
        {


            if (walls.wallsArray[a, 5] || board.veritaMenOnBoard[movePath[currentPos + 1][0], movePath[currentPos + 1][1]])
            {

                if (walls.wallsArray[a, 5])
                {

                    expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

                }
                else
                {

                    expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

                    expectedNextPos2 = new Vector2(movePath[currentPos + 2][0], movePath[currentPos + 2][1]);

                }

            }
            else
            {

                expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

                expectedNextPos2 = new Vector2(movePath[currentPos + 2][0], movePath[currentPos + 2][1]);
                

            }

        }
        else if (currentPos == 9)
        {

            if (walls.wallsArray[a, 2] || walls.wallsArray[a, 1] || board.veritaMenOnBoard[movePath[currentPos + 1][0], movePath[currentPos + 1][1]])
            {


                if (walls.wallsArray[a, 2] || walls.wallsArray[a, 1])
                {
                    //comes here only if a piece is blocking the bd which means the 2nd square is threatened.
                    expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

                }
                else
                {

                    expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

                    expectedNextPos2 = new Vector2(movePath[currentPos + 2][0], movePath[currentPos + 2][1]);

                }



            }
            else
            {

                expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

                expectedNextPos2 = new Vector2(movePath[currentPos + 2][0], movePath[currentPos + 2][1]);


            }



        }
        else if (currentPos == 10)
        {
            //bd can only threaten p11 because that is the last sqaure the bd can go to.

            //Only if w3 does not exist does the bd threaten 11. 
            //existence of w2 is irrelevant
            if (!walls.wallsArray[a,2])
            {
                
                expectedNextPos1 = new Vector2(movePath[currentPos + 1][0], movePath[currentPos + 1][1]);

            }
        }


        this.gameObject.GetComponent<PhotonView>().RPC("PRPC_SetDeathSquares", PhotonTargets.All, expectedNextPos1, expectedNextPos2);
        

    }


    [PunRPC]

    public void PRPC_SetDeathSquares(Vector2 expectedNextPos1, Vector2 expectedNextPos2)
    {
        if (expectedNextPos1.x != -1)
        {

            deathSquare1.SetActive(true);

            deathSquare1.transform.position = GlobalFunctions.GridToWorldCoordinates(expectedNextPos1);

        }
        else
        {

            deathSquare1.SetActive(false);

        }

        if (expectedNextPos2.x != -1)
        {

            deathSquare2.SetActive(true);

            deathSquare2.transform.position = GlobalFunctions.GridToWorldCoordinates(expectedNextPos2);

        }
        else
        {

            deathSquare2.SetActive(false);

        }
        
    }


}
