﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rook : VeritaMen {

    public override bool[,] PossibleMoves()
    {

        ResetLegalMovesArray();

        
        int pos_x = (int)pos.x;
        int pos_y = (int)pos.y;

        int n = 1;


        // right
        while(IsSquareOnBoard(pos_x+n, pos_y))
        {

            if (!board.veritaMenOnBoard[pos_x+n, pos_y])
            {

                legalMovesArray[pos_x + n, pos_y] = true;

            } else
            {

                if (board.veritaMenOnBoard[pos_x+n, pos_y].team == this.team)
                {

                    legalMovesArray[pos_x + n, pos_y] = false;

                } else
                {

                    legalMovesArray[pos_x + n, pos_y] = true;

                }

                break;

            }

            n++;

        }



        n = 1;

        // left
        while (IsSquareOnBoard(pos_x - n, pos_y))
        {

            if (!board.veritaMenOnBoard[pos_x - n, pos_y])
            {

                legalMovesArray[pos_x - n, pos_y] = true;

            }
            else
            {

                if (board.veritaMenOnBoard[pos_x - n, pos_y].team == this.team)
                {

                    legalMovesArray[pos_x - n, pos_y] = false;

                }
                else
                {

                    legalMovesArray[pos_x - n, pos_y] = true;

                }

                break;

            }

            n++;

        }




        n = 1;

        //upward
        while (IsSquareOnBoard(pos_x, pos_y + n))
        {

            if (!board.veritaMenOnBoard[pos_x, pos_y + n])
            {

                legalMovesArray[pos_x, pos_y + n] = true;

            }
            else
            {

                if (board.veritaMenOnBoard[pos_x, pos_y + n].team == this.team)
                {

                    legalMovesArray[pos_x, pos_y + n] = false;

                }
                else
                {

                    legalMovesArray[pos_x, pos_y + n] = true;

                }

                break;

            }

            n++;

        }



        n = 1;

        //down
        while (IsSquareOnBoard(pos_x, pos_y - n))
        {

            if (!board.veritaMenOnBoard[pos_x, pos_y - n])
            {

                legalMovesArray[pos_x, pos_y - n] = true;

            }
            else
            {

                if (board.veritaMenOnBoard[pos_x, pos_y - n].team == this.team)
                {

                    legalMovesArray[pos_x, pos_y - n] = false;

                }
                else
                {

                    legalMovesArray[pos_x, pos_y - n] = true;

                }

                break;

            }

            n++;

        }




        MovesNeverLegal();

        return legalMovesArray;

    }
}
